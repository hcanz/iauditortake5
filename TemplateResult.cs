﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_iAuditor
{
    public class TemplateResult
    {
        public int count { get; set; }
        public int total { get; set; }
        public List<Audits> audits { get; set; }
    }
}
