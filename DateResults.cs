﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_iAuditor
{
    public class DateResults : IEnumerator,IEnumerable
    {
        private Audits[] auditlist;
        int position = -1;

      //Create internal array in constructor.
      public DateResults()
      {
         auditlist= new Audits[1000];
      
      }

      //IEnumerator and IEnumerable require these methods.
      public IEnumerator GetEnumerator()
      {
         return (IEnumerator)this;
      }

      //IEnumerator
      public bool MoveNext()
      {
         position++;
         return (position < auditlist.Length);
      }

      //IEnumerable
      public void Reset()
      {position = 0;}

      //IEnumerable
      public object Current
      {
         get { return auditlist[position];}
      }
         

    }
}
