﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_iAuditor
{
    public class Assets
    {
        public string barcode { get; set; }
        public int cost { get; set; }
        public List<CustomFields> custom_fields { get; set; }
        public int depreciation { get; set; }
        public int depth { get; set; }
        public string description { get; set; }
        public int height { get; set; }
        public string id { get; set; }
        public string identifier { get; set; }
        public string make { get; set; }
        public List<Image> media { get; set; }
        public string model { get; set; }
        public string serial_number { get; set; }
        public string time_stamp { get; set; }
        public string title { get; set; }
        public int weight { get; set; }
        public int width { get; set; }
        public int year_of_manufacture { get; set; }
    }
}
