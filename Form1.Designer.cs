﻿using System.Windows.Forms;
namespace API_iAuditor
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        public string CustomFormat { get; set; }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {            
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.Download = new System.Windows.Forms.Button();
            this.Clear = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.audit_text = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.listBox_template = new System.Windows.Forms.ListBox();
            this.label6 = new System.Windows.Forms.Label();
            this.dateTimePicker_from = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker_to = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.auditdataBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.jsonResultBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.auditdataBindingSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.jsonResultBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "siemens.gif");
            // 
            // Download
            // 
            this.Download.Location = new System.Drawing.Point(657, 22);
            this.Download.Name = "Download";
            this.Download.Size = new System.Drawing.Size(98, 23);
            this.Download.TabIndex = 0;
            this.Download.Text = "Download API";
            this.Download.UseVisualStyleBackColor = true;
            this.Download.Click += new System.EventHandler(this.Download_Click);
            // 
            // Clear
            // 
            this.Clear.Location = new System.Drawing.Point(657, 51);
            this.Clear.Name = "Clear";
            this.Clear.Size = new System.Drawing.Size(98, 23);
            this.Clear.TabIndex = 1;
            this.Clear.Text = "Clear";
            this.Clear.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 128);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Audit ID";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Audit ID";
            // 
            // audit_text
            // 
            this.audit_text.Location = new System.Drawing.Point(84, 15);
            this.audit_text.Name = "audit_text";
            this.audit_text.Size = new System.Drawing.Size(231, 20);
            this.audit_text.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 51);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Template ID";
            // 
            // listBox_template
            // 
            this.listBox_template.FormattingEnabled = true;
            this.listBox_template.Items.AddRange(new object[] {
            "",
            "Take 5",
            "PMS CT Emotion 2003 Checklist",
            "PMS CT Emotion 2007 Checklist",
            "PMS CT DEF/AS/FLASH/FORCE/EDGE CHECKLIST",
            "PMS CT Scope Checklist",
            "PMS CT S10/16/40/64 Checklist",
            "PMS CT BAL/EMO/ESP CHECKLIST",
            "PMS CT Perspective Checklist ",
            " "});
            this.listBox_template.Location = new System.Drawing.Point(84, 45);
            this.listBox_template.Name = "listBox_template";
            this.listBox_template.Size = new System.Drawing.Size(231, 17);
            this.listBox_template.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(356, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(83, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Audit Date From";
            // 
            // dateTimePicker_from
            // 
            //this.dateTimePicker_from.CustomFormat = "yyyy\'-\'MM\'-\'dd 00\':\'00\':\'ss\'Z\'";
            this.dateTimePicker_from.CustomFormat = "yyyy\'-\'MM\'-\'dd 20\':\'46\'%2B1000\'";
           // this.dateTimePicker_from.CustomFormat = "yyyy\'-\'MM\'-\'dd 00\':\'00\'%2B1000\'";
            this.dateTimePicker_from.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker_from.Location = new System.Drawing.Point(445, 15);
            this.dateTimePicker_from.Name = "dateTimePicker_from";
            this.dateTimePicker_from.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker_from.TabIndex = 13;
            // 
            // dateTimePicker_to
            // 
            //this.dateTimePicker_to.CustomFormat = "yyyy\'-\'MM\'-\'dd 23\':\'59\':\'ss\'Z\'";
            this.dateTimePicker_to.CustomFormat = "yyyy\'-\'MM\'-\'dd 20\':\'45\'%2B1000\'";
            //this.dateTimePicker_to.CustomFormat = "yyyy\'-\'MM\'-\'dd 23\':\'59\'%2B1000\'";
            this.dateTimePicker_to.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker_to.Location = new System.Drawing.Point(445, 45);
            this.dateTimePicker_to.Name = "dateTimePicker_to";
            this.dateTimePicker_to.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker_to.TabIndex = 14;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(356, 49);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Audit Date To";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(18, 145);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(737, 122);
            this.dataGridView1.TabIndex = 16;
            // 
            // auditdataBindingSource2
            // 
            this.auditdataBindingSource2.DataMember = "audit_data";
            this.auditdataBindingSource2.DataSource = this.jsonResultBindingSource;
            // 
            // jsonResultBindingSource
            // 
            this.jsonResultBindingSource.DataSource = typeof(API_iAuditor.JsonResult);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(787, 586);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.dateTimePicker_to);
            this.Controls.Add(this.dateTimePicker_from);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.listBox_template);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.audit_text);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Clear);
            this.Controls.Add(this.Download);
            this.Name = "Form1";
            this.Text = "iAuditor API Downloader";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.auditdataBindingSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.jsonResultBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Button Download;
        private System.Windows.Forms.Button Clear;
        private System.Windows.Forms.Label label1;
        
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox audit_text;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ListBox listBox_template;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker dateTimePicker_from;
        private System.Windows.Forms.DateTimePicker dateTimePicker_to;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingSource jsonResultBindingSource;

        private System.Windows.Forms.BindingSource auditdataBindingSource2;
        
       
       
        
        
    }
}

