﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace API_iAuditor
{
    public class Option
    {
        public string weighting { get; set; }
        public string enable_date { get; set; }
        public string enable_time { get; set; }
        public string is_mandatory { get; set; }
    }
}
