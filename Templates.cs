﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_iAuditor
{
    public class Templates
    {
        //Take 5 Templates
        //template_c4def6f80b1711e4acf6001b1118ce11
        //template_1874139c447111e495fb001b1118ce11

        ////PMS Templates
        ////Ysio 
        //template_2174EA4822394581A9517EA6266D8501 
        //template_1FAF3DBEB1204545A57CD3EDECE8DCC0
        ////PMS CT Emotion 2003
        //template_AEA47BB8A31C460AB73D5790F32D1B97
        ////PMS CT DEF/AS/FLASH/FORCE/EDGE CHECKLIST
        //template_A0EB765227E946728487C41BACD62B68
        ////PMS CT S10/16/40/64 Checklist
        //template_64903D9B1222444EBFA0985DB76164EC
        ////PMS CT Perspective Checklist 
        //template_E67B3FD7C74747189C558DF26326758D
        ////PMS CT Scope Checklist
        //template_791E3E0B3AC24FCEBDA89DBC8B4C3631
        ////CT S10/16/40/64 PMS Checklist
        //template_64903D9B1222444EBFA0985DB76164EC
        ////PMS CT BAL/EMO/ESP CHECKLIST
        //template_A166ACDEE9024EDB85E4A576A8A934AA
        ////Flash PMS
        //template_DB11A0AEC8E2472A850505D01F182D94
        ////Artis Zee Biplane PMS
        //template_0CA47969939140E2AD463CC3DCEDF77A
        ////Sysmex CS-2100i / CS-2000i ver 2 PMS
        //template_89B0C445829B405EACF2EDA5D3B1A82E
        ////Biograph Advanced Workflow
        //template_30B4C8BAE7154BC4895E3146BDDEA4E9
        ////Syngo MI Workstation PMS
        //template_AC059B292EF943FDB72C618A9C0CDD7F
        ////Polymobil III/Plus
        //template_F95BC837A17F4A32B90F41CC9FFD8700
        ////Symbia T PMS
        //template_8855CC0482DE45A4B959CDDF7A2B64C8
        ////Symbia Intevo PMS
        //template_DD6233BBE5D1450B988C8381B1CCEAD8
        ////Symbia S PMS 
        //template_0C9D18C248924695B2795CC3703B6ECF
        ////e.cam PMS
        //template_4C964EEEAADA40C09E5493DEDE7922C6
        ////Symbia E PMS
        //template_9BCF964457AB43209EB51FAB399774D8
        //// Bio 6 PMS 
        //template_2DD77037D9764EA287E16724C4F15546
        ////Bio 6 TP PMS
        //template_A12E99EFE5B54C23A6D273A143848406
        ////Bio 16 PMS
        //template_C49CB33526174E91A96315873295E6B3
        ////Bio 16 TP PMS
        //template_7A44286DD97447B2A1417C72C7319DBE
        ////Bio Classic PMS
        //template_C2F884E29AA34CB9B2E94728D789E5F6
        ////Bio Duo PMS
        //template_702FD3A0C583471FBCEF36993FD960BD
        ////Bio mCT PMS 
        //template_76CA6CF4B1E944B8B4DE3842A0145E40
        ////Bio mCT Flow PMS
        //template_5C25D339C0BE4DF795B0B0D2873473D5
        ////Bio 40-64 TP PMS
        //template_FF0C545F5B0A4AE3BEE7B42657CEA637
        ////Advia Labcell PMS
        //template_74C62BC3FD874A34B1F5B4F0BA1EFAA4
        ////Centaur Classic & XP PMS
        //template_BB3890C0340C44C987AE0982B83C3FEA
        ////Angiostar PMS
        //template_41D65C245B9A44A29F4A247328354E97
        ////Artis One PMS
        //template_78FC554DF50140E19CBDF1CAE6A59CB3
        ////Artis Q Biplane PMS
        //template_65FC0CD746034CE99C91F724970E956A
        ////Artis Q Ceiling PMS
        //template_C0B269E6EBD147519E5A635F13244865
        ////Artis Q Floor PMS
        //template_742AFE8109E34A039B0212D6C1A1491E
        ////Artis Q Zeego PMS
        //template_27835CF19E174ACCA86B83008628EC2E
        ////Artis Zee Ceiling PMS
        //template_43D20DBCE47A48F8847E5D219D1D8407
        ////Artis Zee Floor PMS
        //template_CCD72D4DBB234E25B315994E30CB994F
        ////Artis Zee MP PMS
        //template_3A9616FC706B4C029E2B278B08D1E67F
        ////Artis Zeego PMS
        //template_4F134F52BF16489ABED8C788C12DD22B
        ////Axiom Artis BA/BC/dBA/dBC PMS
        //template_9BA3CAE65DAE4E16B333420194164996
        ////Axiom Artis FA/FC/dFA/dFC PMS
        //template_3D4635D3F21E42ED90B2B5CD420ECE50
        ////Axiom Artis MP/dMP
        //template_1898439F509D4FB897096EB44473EF95
        ////Axiom Artis TA/TC/dTA/dTC PMS
        //template_CCFB24EE50EE4EE8824AE0821A8B249C
        ////Axiom Artis U PMS
        //template_6C786621F471416B81D656ED6DA44540
        ////BCT PMS
        //template_E759BB953D2145C4A5207DCB5BFAF519
        ////Perspective PMS 
        //template_2DE69DCF7F454B948096F9560804C3A6
        ////Siremobil Compact_L from 10000 PMS 1
        //template_F9F20636898F4936AAED178D9D2118DD
        ////Siremobil Compact_L PMS
        //template_41048E1FD24545C4B617477C7041DB77
        ////Siremobil Compact PMS
        //template_5051221A403F4B4894CA1C55053763E1
        ////Multix Select DR PMS 1
        //template_AFEEA59A74A94F588774A8BEE2E997F3
        ////PMS CT Emotion 2007 Checklist test
        //template_0F74D3237DD44009AC8091BF2D0C28CF
        ////Axiom Sensis PMS
        //template_41EECDA3CC8942C28FBB480326D311E8
        ////Sensis Server PMS
        //template_5578F3A19B964145AFB3EFE8303DEAFF
        ////Coroskop PMS
        //template_A13A2BC9B9BE4A03A60390BBD415469A
        ////Leonardo PMS
        //template_8FE8D4B2463B4D0CAE5BDCD7966C4808
////



        
    }
}
