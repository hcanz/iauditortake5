﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_iAuditor
{
    public class Coordinates
    {
        public decimal lat { get; set; }
        public decimal lng { get; set; }
    }
}
