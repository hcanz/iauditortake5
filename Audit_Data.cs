﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_iAuditor
{
    public class Audit_Data
    {        
        public double score { get; set; }
        public double total_score { get; set; }
        public double score_percentage { get; set; }
        public string name { get; set; }
        public double duration { get; set; }
        public Authorship authorship { get; set; }
        public string date_completed { get; set; }
        public string date_modified { get; set; }
        public string date_started { get; set; }
        
    }
}
