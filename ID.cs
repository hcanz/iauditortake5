﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_iAuditor
{
    public class ID
    {        
        public string id { get; set; }
        public string colour { get; set; }
        public string label { get; set; }
        public double score { get; set; }
        public string short_label { get; set; }
        public string type { get; set; }        
        public bool enable_score { get; set; }
        public string image { get; set; }
       
    }
}
