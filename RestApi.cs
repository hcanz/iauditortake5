﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Web.Script.Serialization;
using System.IO;

namespace API_iAuditor
{
    public class RestApi : IDisposable
    {
        private readonly WebClient wc;
        private JavaScriptSerializer js = new JavaScriptSerializer();
        
        //No network credentials constructor
        public RestApi()
        {
            wc = new WebClient();
            wc.Headers[HttpRequestHeader.Accept] = "application/json";
        }

        //With network credentials constructor
        public RestApi(NetworkCredential credentials)
        {
            wc = new WebClient();
            wc.Headers[HttpRequestHeader.Accept] = "application/json";
            if (credentials == null) return;
            wc.Credentials = credentials;
           
            ////kat
            //wc.Headers[HttpRequestHeader.Authorization] = "Bearer dea8db3b0728fd306f25f913edb2152d34f7b3ebb1cff6ae9a1d9c9672f574a5"; 
            ////mark
           // wc.Headers[HttpRequestHeader.Authorization] = "Bearer a86cc9bd92d0105ab4507c95a5128353febab1ec71d6ee8118e3509d69c6624c";
            //sanjay
            wc.Headers[HttpRequestHeader.Authorization] = "Bearer 8861c40cbfcd82b19f6d38b84fae54fb16566bad5b1d5834c2e572678bcd3f57";

        }

        public string GetRawString(string url)
        {
            return wc.DownloadString(url);
        }

        public DateResult Get<DateResult>(string url)
        {
            wc.Headers[HttpRequestHeader.ContentType] = "application/json";
            var stringValue = wc.DownloadString(url);
            //T returnObj = js.Deserialize<T>(stringValue);
            dynamic returnObj = js.Deserialize<DateResult>(stringValue);
             
            return returnObj;

            
        }

        public T Gets<T>(string url)
        {
            wc.Headers[HttpRequestHeader.ContentType] = "application/json";
            var stringValue = wc.DownloadString(url);
            //T returnObj = js.Deserialize<T>(stringValue);
            

           dynamic returnObj = js.Deserialize<T>(stringValue); 
            
            return returnObj;


        }
        public T Post<T>(string url, object body)
        {
            wc.Headers[HttpRequestHeader.ContentType] = "application/json";
            var stringValue = wc.UploadString(url, js.Serialize(body));
            T returnObj = js.Deserialize<T>(stringValue);
            return returnObj;
        }

        public void DownloadFile(string url, string destination)
        {
            wc.DownloadFile(url, destination);
        }

        #region IDisposable

        // Flag: Has Dispose already been called? 
        bool disposed = false;

        // Public implementation of Dispose pattern callable by consumers. 
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // Protected implementation of Dispose pattern. 
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                // Free any other managed objects here. 
                //
            }

            // Free any unmanaged objects here. 
            //
            disposed = true;
        }

        ~RestApi()
        {
            Dispose(false);
        }
        #endregion
    }
}
