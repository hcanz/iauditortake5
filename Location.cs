﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_iAuditor
{
    public class Location
    {
        public string administrative_area { get; set; }
        public string country { get; set; }
        public Array formatted_address { get; set; }
        public Geometry geometry { get; set; }
        public string iso_country_code { get; set; }
        public string locality { get; set; }
        public string name { get; set; }
        public string postal_code { get; set; }
        public string sub_administrative_area { get; set; }
        public string sub_locality { get; set; }
        public string sub_thoroughfare { get; set; }
        public string thoroughfare { get; set; }
    }
}
