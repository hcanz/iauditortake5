﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_iAuditor
{
    public class Authorship
    {
        public string device_id { get; set; }
        public string owner { get; set; }
        public string author { get; set; }
    }
}
