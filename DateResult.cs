﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_iAuditor
{
    public class DateResult 

    {
        public int count { get; set; }
        public int total { get; set; }
        public List<AuditsDate> audits { get; set; }     
 
    }
}
