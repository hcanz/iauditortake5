﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_iAuditor
{
    public class Scoring
    {
        public double score { get; set; }
        public double max_score { get; set; }
        public double score_percentage { get; set; }
        public double combined_score { get; set; }
        public double combined_max_score { get; set; }
        public double combined_score_percentage { get; set; }
    }
}
