﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_iAuditor
{
    public class Item
    {
        public string item_id { get; set; }
        public string parent_id { get; set; }
        public string label { get; set; }
        public string type { get; set; }
        //public string[] children { get; set; }
        public List<string> children { get; set; }
        public Scoring scoring { get; set; }
        public Options options { get; set; }
        public Responses responses { get; set; }
        public bool evaluation { get; set; }
        public bool inactive { get; set; }        
        
        
    }

    public class ItemsSaved
    {
        public string Label { get; set; }
        public string Response_Label { get; set; }
    }
}
