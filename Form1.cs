﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Web;
using System.IO;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Globalization;
using System.Collections;
using System.Reflection;
using System.Configuration;
using System.Net.Mail;


namespace API_iAuditor
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
           
        }

        //protected override void OnShown(EventArgs e)
        //{
        //    base.OnShown(e);
        //    this.Download_Click(null, null);
        //}
                     
        private void Download_Click(object sender, EventArgs e)
        {        
            string auditid = audit_text.Text;
            string templateid = String.Empty;
            //DateTime dayto = DateTime.Now;
            //DateTime dayfrom = dayto.Subtract(new TimeSpan(0,5,0));
            //DateTime dayfrom = subtract;
            DateTime day = DateTime.Today;
            //DateTime dayfrom = DateTime.Today.AddDays(-1);
            DateTime dayfrom = DateTime.Today.AddDays(-3);
            //string dateto = day.ToString("yyyy-MM-dd 23:59'%'2B1000");
            //string datefrom = dayfrom.ToString("yyyy-MM-dd 00:00'%'2B1000");
            //dateTimePicker_from.Text = DateTime.Parse(dateTimePicker_from.Text).AddDays(-1).ToString();
            //string datetot = dateTimePicker_to.Text;
            //string datefromt = dateTimePicker_from.Text;
            //string datefrom = dayfrom.ToString("yyyy-MM-dd hh:mm'%'2B1000");
            //string dateto = dayto.ToString("yyyy-MM-dd hh:mm'%'2B1000");

            string dateto = day.ToString("yyyy-MM-ddT23:59:59.000Z");
            string datefrom = dayfrom.ToString("yyyy-MM-ddT12:00:01.000Z");
                
            try
            {
                #region //specific audit
                if (audit_text.Text != "")
                {
                    string url = @"https://api.safetyculture.io/audits/" + auditid;
                    //var credential = new NetworkCredential("kathrine.pagsisihan@siemens.com", "YumiZack68");
                    var credential = new NetworkCredential("sanjay.sharma@siemens-healthineers.com", "SANEDrf9");
                    var getAudit = new RestApi(credential);
                    var auditresult = getAudit.Gets<JsonResult>(url);
                    #region //declaration of variables
                    //declaration of variables 
                    var audit_id = auditresult.audit_id;
                    var template_id = auditresult.template_id;
                    var doc_type = String.Empty;

                    if (template_id == "template_c4def6f80b1711e4acf6001b1118ce11" || template_id == "template_1874139c447111e495fb001b1118ce11" || template_id == "template_4c2d3d81edf0470e9c5ee6df0acc3267")
                        doc_type = "Take 5";
                    else if (template_id == "template_1320A5F3871D4054AC50A1F1EA55956B")
                        doc_type = "Follow-up Visit Report";
                    else if (template_id == "template_E1FE842EA792412DAD6264B20AA2E048" || template_id == "template_2CE608F21AF2465AA263917CDC25AF41" || template_id == "template_E1FE842EA792412DAD6264B20AA2E048" || template_id == "template_A26A4F72F439498A8E191FE3520C5E56")
                        doc_type = "Other Visit Report";
                    else if (template_id == "template_83CB10205F9A40A0BAE340791E16686C")
                        doc_type = "Image Quality Visit Report";
                    else if (template_id == "template_BBD428F41D4F4F109A0CB3C982A697B2" || template_id == "template_2DC83E30C22A47E89E03CD03D3149879" || template_id == "template_3DD5AD077F8A4DAA8993F379E170CB9B" || template_id == "template_121c8a3537d711e49e40001b1118ce11")
                        doc_type = "Handover Visit Report";
                    else if (template_id == "template_f738f1ee379311e49278001b1118ce11")
                        doc_type = "Parts Pick Up Request";
                    else if (template_id == "template_D913EC39AEFB44CE9C95CBF8F69784E7")
                        doc_type = "Service Report";
                    else if (template_id == "template_9bd9ac0c0e1a11e487c0001b1118ce11")
                        doc_type = "Declaration of Consent";
                    else if (template_id == "template_13c0a0002e4511e49a85001b1118ce11")
                        doc_type = "MR Safety Checklist";
                    else if (template_id == "template_63bbac00394711e493f4001b1118ce11" || template_id == "template_7DFC9ACF50994E44AA85839D2D568D9B")
                        doc_type = "Field Call Clarification";
                    else if (template_id == "template_885b7fb0580811e4a009001b1118ce11")
                        doc_type = "Attendance Record";
                    else if (template_id == "template_e4ec0cbd43af11e4ab23001b1118ce11")
                        doc_type = "Wonderware Logger";
                    else if (template_id == "template_62c6f2b440034da8a845a14f7267990f")
                        doc_type = "Travel Request";
                    else if (template_id == "template_9144007C618A41D89FBCB693C15CA274")
                        doc_type = "Training Checklist";
                    else
                        doc_type = "PMS Checklist";

                    //if (template_id == "template_c4def6f80b1711e4acf6001b1118ce11" || template_id == "template_1874139c447111e495fb001b1118ce11")
                    //{
                    //    continue;
                    //}

                    var created_at = auditresult.created_at;
                    var modified_at = auditresult.modified_at;
                    var score = auditresult.audit_data.score;
                    var totalscore = auditresult.audit_data.total_score;
                    var scorepercentage = auditresult.audit_data.score_percentage;
                    var name = auditresult.audit_data.name;
                    var duration = auditresult.audit_data.duration;
                    var datecompleted = auditresult.audit_data.date_completed;
                    var datemodified = auditresult.audit_data.date_modified;
                    var datestarted = auditresult.audit_data.date_started;
                    var deviceid = auditresult.audit_data.authorship.device_id;
                    var owner = auditresult.audit_data.authorship.owner;
                    var author = auditresult.audit_data.authorship.author;
                    var metname = auditresult.template_data.metadata.name;
                    var metdesc = auditresult.template_data.metadata.description;
                    var metimage = String.Empty;

                    if (auditresult == null || auditresult.template_data == null || auditresult.template_data.metadata == null || auditresult.template_data.metadata.image == null || auditresult.template_data.metadata.image.media_id == null || auditresult.template_data.metadata.image.media_id == String.Empty)
                        metimage = null;
                    else
                        metimage = auditresult.template_data.metadata.image.media_id;

                    var responseid = auditresult.template_data.response_sets;

                    ////change the format of dates
                    TimeSpan serverOffset = TimeZoneInfo.Local.GetUtcOffset(DateTimeOffset.Now);
                    DateTimeOffset createdat = DateTimeOffset.Parse(created_at, System.Globalization.CultureInfo.GetCultureInfo("en-AU"));
                    DateTimeOffset creatat = TimeZoneInfo.ConvertTime(createdat, TimeZoneInfo.Local);

                    DateTimeOffset modifiedat = DateTimeOffset.Parse(modified_at, System.Globalization.CultureInfo.GetCultureInfo("en-AU"));
                    DateTimeOffset modat = TimeZoneInfo.ConvertTime(modifiedat, TimeZoneInfo.Local);

                    DateTimeOffset datec = DateTimeOffset.Parse(datecompleted, System.Globalization.CultureInfo.GetCultureInfo("en-AU"));
                    DateTimeOffset dateco = TimeZoneInfo.ConvertTime(datec, TimeZoneInfo.Local);

                    DateTimeOffset datem = DateTimeOffset.Parse(datemodified, System.Globalization.CultureInfo.GetCultureInfo("en-AU"));
                    DateTimeOffset datemo = TimeZoneInfo.ConvertTime(datem, TimeZoneInfo.Local);

                    DateTimeOffset dates = DateTimeOffset.Parse(datestarted, System.Globalization.CultureInfo.GetCultureInfo("en-AU"));
                    DateTimeOffset datest = TimeZoneInfo.ConvertTime(dates, TimeZoneInfo.Local);
                    #endregion
                    //connect to the database
                    using (SqlConnection connection = new SqlConnection(@"Server=pd1g9d7qth63qbk.c2ikttwxhgln.ap-northeast-1.rds.amazonaws.com;Database=iAuditor; User Id=admin; password=?hXfSXYco4J;max pool size=1;MultipleActiveResultSets=True;"))
                    //using (SqlConnection connection = new SqlConnection(@"Data Source=AAEAUMEL27872L\API;Initial Catalog=iAuditor;Integrated Security=True;max pool size=1"))
                    {
                        connection.Open();
                        //to check if the audit id is already existing in DB                          
                        var dataAudit = "SELECT Audit_ID FROM api_Audit WHERE (Audit_ID = @AuditID)";
                        var Dataaudit = new SqlCommand(dataAudit, connection);
                        var checkparam = Dataaudit.Parameters.AddWithValue("@AuditID", audit_id);
                        if (audit_id == null) checkparam.Value = DBNull.Value;

                        Dataaudit.ExecuteNonQuery();
                        var sResult = (String)Dataaudit.ExecuteScalar();

                        if (sResult != null)
                        {
                            var dataHeader = "SELECT Audit_ID, Label, Text FROM api_header_items WHERE (Audit_ID = @AuditID and Label = 'System ID')";
                            var DataHeader = new SqlCommand(dataHeader, connection);
                            var checkheader = DataHeader.Parameters.AddWithValue("@AuditID", audit_id);
                            if (audit_id == null) checkheader.Value = DBNull.Value;

                            DataHeader.ExecuteNonQuery();
                            var sResultHeader = (String)DataHeader.ExecuteScalar();

                            if (sResultHeader != null)
                            {
                                foreach (var header in auditresult.header_items)
                                {
                                    var systemid = String.Empty;
                                    if (header.label == "System ID")
                                    {
                                        if (header == null || header.responses == null || header.responses.text == null || header.responses.text == String.Empty)
                                            systemid = null;
                                        else
                                            systemid = header.responses.text;

                                        //update header systemID
                                        string headeritems = "UPDATE api_Header_Items_OTHER SET Text = @Text WHERE Audit_ID = @AuditID and Label = 'System ID'";
                                        SqlCommand myHeaderItems = new SqlCommand(headeritems, connection);
                                        SqlParameter auditParam = myHeaderItems.Parameters.AddWithValue("@AuditID", audit_id);
                                        if (audit_id == null) auditParam.Value = DBNull.Value;
                                        SqlParameter textParam = myHeaderItems.Parameters.AddWithValue("@Text", systemid);
                                        if (systemid == null) textParam.Value = DBNull.Value;
                                        myHeaderItems.ExecuteNonQuery();
                                    }
                                }
                            }                            
                        }
                        #region //audit
                        //insert into Audit Table
                        string audittable = "INSERT INTO api_Audit (Audit_id, Template_id, Created_at, Modified_at, Doc_Type)";
                        audittable += " VALUES (@Audit_id, @Template_id, @Created_at, @Modified_at, @Doc_Type)";

                        SqlCommand myCommand = new SqlCommand(audittable, connection);

                        myCommand.Parameters.AddWithValue("@Audit_id", audit_id);
                        myCommand.Parameters.AddWithValue("@Template_id", template_id);
                        myCommand.Parameters.AddWithValue("@Created_at", creatat);
                        myCommand.Parameters.AddWithValue("@Modified_at", modat);
                        myCommand.Parameters.AddWithValue("@Doc_Type", doc_type);
                        myCommand.ExecuteNonQuery();
                        #endregion //audit
                        #region //auditdata
                        //insert into Audit Data Table
                        string auditdata = "INSERT INTO api_AuditData (Audit_ID, Score, Total_Score, Score_Percentage, Name, Duration, Device_ID, Owner, Author, Date_Completed, Date_Modified, Date_Started)";
                        auditdata += "VALUES (@Audit_ID, @Score, @Total_Score, @Score_Percentage, @Name, @Duration, @Device_ID, @Owner, @Author, @Date_Completed, @Date_Modified, @Date_Started)";

                        SqlCommand myAuditData = new SqlCommand(auditdata, connection);
                        SqlParameter auditidParam = myAuditData.Parameters.AddWithValue("@Audit_ID", audit_id);
                        if (audit_id == null) auditidParam.Value = DBNull.Value;
                        SqlParameter scoreParam = myAuditData.Parameters.AddWithValue("@Score", score);
                        if (score == 0.00) scoreParam.Value = DBNull.Value;
                        SqlParameter totalParam = myAuditData.Parameters.AddWithValue("@Total_Score", totalscore);
                        if (totalscore == 0.00) totalParam.Value = DBNull.Value;
                        SqlParameter percentParam = myAuditData.Parameters.AddWithValue("@Score_Percentage", scorepercentage);
                        if (scorepercentage == 0.00) percentParam.Value = DBNull.Value;
                        SqlParameter nameParam = myAuditData.Parameters.AddWithValue("@Name", name);
                        if (name == null) nameParam.Value = DBNull.Value;
                        SqlParameter durationParam = myAuditData.Parameters.AddWithValue("@Duration", duration);
                        if (duration == 0.00) durationParam.Value = DBNull.Value;
                        SqlParameter deviceidParam = myAuditData.Parameters.AddWithValue("@Device_ID", deviceid);
                        if (deviceid == null) deviceidParam.Value = DBNull.Value;
                        SqlParameter ownerParam = myAuditData.Parameters.AddWithValue("@Owner", owner);
                        if (owner == null) ownerParam.Value = DBNull.Value;
                        SqlParameter authorParam = myAuditData.Parameters.AddWithValue("@Author", author);
                        if (author == null) authorParam.Value = DBNull.Value;
                        SqlParameter datecParam = myAuditData.Parameters.AddWithValue("@Date_Completed", dateco);
                        if (datecompleted == null) datecParam.Value = DBNull.Value;
                        SqlParameter datemParam = myAuditData.Parameters.AddWithValue("@Date_Modified", datemo);
                        if (datemodified == null) datemParam.Value = DBNull.Value;
                        SqlParameter datesParam = myAuditData.Parameters.AddWithValue("@Date_Started", datest);
                        if (datestarted == null) datesParam.Value = DBNull.Value;
                        myAuditData.ExecuteNonQuery();
                        #endregion auditdata
                        #region //templatedata
                        //insert into Template Data
                        string templatedata = "INSERT INTO api_Template_Data (Audit_ID, Device_ID, Owner, Author, Name, Description, Media_ID)";
                        templatedata += "VALUES (@Audit_ID, @Device_ID, @Owner, @Author, @Name, @Description, @Media_ID)";

                        SqlCommand myTemplateData = new SqlCommand(templatedata, connection);
                        SqlParameter auditnoParam = myTemplateData.Parameters.AddWithValue("@Audit_ID", audit_id);
                        if (audit_id == null) auditnoParam.Value = DBNull.Value;
                        SqlParameter deviceParam = myTemplateData.Parameters.AddWithValue("@Device_ID", deviceid);
                        if (deviceid == null) deviceParam.Value = DBNull.Value;
                        SqlParameter owner1Param = myTemplateData.Parameters.AddWithValue("@Owner", owner);
                        if (owner == null) owner1Param.Value = DBNull.Value;
                        SqlParameter author1Param = myTemplateData.Parameters.AddWithValue("@Author", author);
                        if (author == null) author1Param.Value = DBNull.Value;
                        SqlParameter name1Param = myTemplateData.Parameters.AddWithValue("@Name", metname);
                        if (metname == null) name1Param.Value = DBNull.Value;
                        SqlParameter descriptionParam = myTemplateData.Parameters.AddWithValue("@Description", metdesc);
                        if (metdesc == null) descriptionParam.Value = DBNull.Value;
                        SqlParameter mediaParam = myTemplateData.Parameters.AddWithValue("@Media_ID", metimage);
                        if (metimage == null) mediaParam.Value = DBNull.Value;

                        myTemplateData.ExecuteNonQuery();
                        #endregion //template data
                        #region //header_items
                        //loop for header items
                        foreach (var header in auditresult.header_items)
                        {
                            var audit_no = auditresult.audit_id;

                            if (header.label == "System ID")
                            {
                                //initialize values
                                var parentid = String.Empty;
                                var itemid = String.Empty;
                                var label = String.Empty;
                                var section = String.Empty;
                                var systemid = String.Empty;
                                //check if null
                                if (header == null || header.parent_id == null || header.parent_id == String.Empty)
                                    parentid = null;
                                else
                                    parentid = header.parent_id;
                                if (header == null || header.item_id == null || header.item_id == String.Empty)
                                    itemid = null;
                                else
                                    itemid = header.item_id;
                                if (header == null || header.label == null || header.label == String.Empty)
                                    label = null;
                                else
                                    label = header.label;
                                if (header == null || header.section == null || header.section == String.Empty)
                                    section = null;
                                else
                                    section = header.section;
                                if (header == null || header.responses == null || header.responses.text == null || header.responses.text == String.Empty)
                                    systemid = null;
                                else
                                    systemid = header.responses.text;

                                //insert into Header Items
                                string headeritems = "INSERT INTO api_Header_Items (Audit_ID, Parent_ID, Item_ID, Label, Section, Text )";
                                headeritems += " VALUES (@Audit_ID, @Parent_ID, @Item_ID, @Label, @Section, @Text)";

                                SqlCommand myHeaderItems = new SqlCommand(headeritems, connection);
                                SqlParameter auditParam = myHeaderItems.Parameters.AddWithValue("@Audit_ID", audit_no);
                                if (audit_no == null) auditParam.Value = DBNull.Value;
                                SqlParameter parentParam = myHeaderItems.Parameters.AddWithValue("@Parent_ID", parentid);
                                if (parentid == null) parentParam.Value = DBNull.Value;
                                SqlParameter itemParam = myHeaderItems.Parameters.AddWithValue("@Item_ID", itemid);
                                if (itemid == null) itemParam.Value = DBNull.Value;
                                SqlParameter labelParam = myHeaderItems.Parameters.AddWithValue("@Label", label);
                                if (label == null) labelParam.Value = DBNull.Value;
                                SqlParameter sectionParam = myHeaderItems.Parameters.AddWithValue("@Section", section);
                                if (section == null) sectionParam.Value = DBNull.Value;
                                SqlParameter textParam = myHeaderItems.Parameters.AddWithValue("@Text", systemid);
                                if (systemid == null) textParam.Value = DBNull.Value;
                                myHeaderItems.ExecuteNonQuery();

                                //to get the region of SYSTEM ID and correct the dates
                                //SqlConnection connectIBASE = new SqlConnection(@"Data Source=AAEAUMEL27872L\API;Initial Catalog=HCCS;Integrated Security=True;max pool size=1");
                                SqlConnection connectIBASE = new SqlConnection(@"Server=pd1g9d7qth63qbk.c2ikttwxhgln.ap-northeast-1.rds.amazonaws.com;Database=HCCloudservices; User Id=admin; password=?hXfSXYco4J;max pool size=1;MultipleActiveResultSets=True;");
                                connectIBASE.Open();

                                var ibase = "SELECT Region FROM PRD.IBASES WHERE (FLNumber = @FLNumber)";
                                var ibases = new SqlCommand(ibase, connectIBASE);
                                var sysid = "800-" + systemid;
                                var checkid = ibases.Parameters.AddWithValue("@FLNumber", sysid);
                                if (sysid == null) checkid.Value = DBNull.Value;
                                ibases.ExecuteNonQuery();
                                var sIBASE = (String)ibases.ExecuteScalar();
                                #region Region is not null
                                if (sIBASE != null) // correct system id
                                {
                                    ////for timezone settings
                                    DateTime date;
                                    TimeZoneInfo tst;
                                    TimeZoneInfo dst;
                                    bool isDaylight;
                                    
                                    createdat = DateTimeOffset.Parse(created_at, System.Globalization.CultureInfo.GetCultureInfo("en-AU"));
                                    modifiedat = DateTimeOffset.Parse(modified_at, System.Globalization.CultureInfo.GetCultureInfo("en-AU"));
                                    datec = DateTimeOffset.Parse(datecompleted, System.Globalization.CultureInfo.GetCultureInfo("en-AU"));
                                    datem = DateTimeOffset.Parse(datemodified, System.Globalization.CultureInfo.GetCultureInfo("en-AU"));
                                    dates = DateTimeOffset.Parse(datestarted, System.Globalization.CultureInfo.GetCultureInfo("en-AU"));

                                    #region ACT, VIC, NSW
                                    if (sIBASE == "ACT" || sIBASE == "VIC" || sIBASE == "NSW")
                                    {
                                        tst = TimeZoneInfo.FindSystemTimeZoneById("AUS Eastern Standard Time");
                                        creatat = TimeZoneInfo.ConvertTime(createdat, tst);
                                        isDaylight = tst.IsDaylightSavingTime(createdat);
                                        if (isDaylight == true)
                                        {         
                                            //dst = TimeZoneInfo.FindSystemTimeZoneById("AUS Eastern Daylight Time");
                                            creatat = TimeZoneInfo.ConvertTime(createdat, tst);
                                            modat = TimeZoneInfo.ConvertTime(modifiedat, tst);
                                            dateco = TimeZoneInfo.ConvertTime(datec, tst);
                                            datemo = TimeZoneInfo.ConvertTime(datem, tst);
                                            datest = TimeZoneInfo.ConvertTime(dates, tst);
                                            //update audit table
                                            string auditupdate = "UPDATE api_Audit SET Created_at = @Created_at, Modified_at = @Modified_at Where Audit_id = @Audit_ID";

                                            SqlCommand cmd = new SqlCommand(auditupdate, connection);
                                            cmd.Parameters.AddWithValue("@Audit_ID", audit_no);
                                            cmd.Parameters.AddWithValue("@Created_at", creatat);
                                            cmd.Parameters.AddWithValue("@Modified_at", modat);
                                            cmd.ExecuteNonQuery();

                                            string auditdataupdate = "UPDATE api_AuditData SET Date_Completed = @Date_Completed, Date_Modified = @Date_Modified, Date_Started = @Date_Started WHERE Audit_id = @Audit_ID";

                                            SqlCommand cmdauditdata = new SqlCommand(auditdataupdate, connection);
                                            cmdauditdata.Parameters.AddWithValue("@Audit_ID", audit_no);
                                            cmdauditdata.Parameters.AddWithValue("@Date_Completed", dateco);
                                            cmdauditdata.Parameters.AddWithValue("@Date_Modified", datemo);
                                            cmdauditdata.Parameters.AddWithValue("@Date_Started", datest);
                                            cmdauditdata.ExecuteNonQuery();
                                        }
                                        else
                                        {
                                            modat = TimeZoneInfo.ConvertTime(modifiedat, tst);
                                            dateco = TimeZoneInfo.ConvertTime(datec, tst);
                                            datemo = TimeZoneInfo.ConvertTime(datem, tst);
                                            datest = TimeZoneInfo.ConvertTime(dates, tst);
                                            //update audit table
                                            string auditupdate = "UPDATE api_Audit SET Created_at = @Created_at, Modified_at = @Modified_at Where Audit_id = @Audit_ID";

                                            SqlCommand cmd = new SqlCommand(auditupdate, connection);
                                            cmd.Parameters.AddWithValue("@Audit_ID", audit_no);
                                            cmd.Parameters.AddWithValue("@Created_at", creatat);
                                            cmd.Parameters.AddWithValue("@Modified_at", modat);
                                            cmd.ExecuteNonQuery();

                                            //update auditdata
                                            string auditdataupdate = "UPDATE api_AuditData SET Date_Completed = @Date_Completed, Date_Modified = @Date_Modified, Date_Started = @Date_Started WHERE Audit_id = @Audit_ID";
                                            SqlCommand cmdauditdata = new SqlCommand(auditdataupdate, connection);
                                            cmdauditdata.Parameters.AddWithValue("@Audit_ID", audit_no);
                                            cmdauditdata.Parameters.AddWithValue("@Date_Completed", dateco);
                                            cmdauditdata.Parameters.AddWithValue("@Date_Modified", datemo);
                                            cmdauditdata.Parameters.AddWithValue("@Date_Started", datest);
                                            cmdauditdata.ExecuteNonQuery();
                                        }
                                    }
                                    #endregion
                                    #region QLD
                                    else if (sIBASE == "QLD")
                                    {
                                        tst = TimeZoneInfo.FindSystemTimeZoneById("E. Australia Standard Time");
                                        creatat = TimeZoneInfo.ConvertTime(createdat, tst);
                                        modat = TimeZoneInfo.ConvertTime(modifiedat, tst);
                                        dateco = TimeZoneInfo.ConvertTime(datec, tst);
                                        datemo = TimeZoneInfo.ConvertTime(datem, tst);
                                        datest = TimeZoneInfo.ConvertTime(dates, tst);
                                        //update audit table
                                        string auditupdate = "UPDATE api_Audit SET Created_at = @Created_at, Modified_at = @Modified_at Where Audit_id = @Audit_ID";

                                        SqlCommand cmd = new SqlCommand(auditupdate, connection);
                                        cmd.Parameters.AddWithValue("@Audit_ID", audit_no);
                                        cmd.Parameters.AddWithValue("@Created_at", creatat);
                                        cmd.Parameters.AddWithValue("@Modified_at", modat);
                                        cmd.ExecuteNonQuery();

                                        //update auditdata
                                        string auditdataupdate = "UPDATE api_AuditData SET Date_Completed = @Date_Completed, Date_Modified = @Date_Modified, Date_Started = @Date_Started WHERE Audit_id = @Audit_ID";
                                        SqlCommand cmdauditdata = new SqlCommand(auditdataupdate, connection);
                                        cmdauditdata.Parameters.AddWithValue("@Audit_ID", audit_no);
                                        cmdauditdata.Parameters.AddWithValue("@Date_Completed", dateco);
                                        cmdauditdata.Parameters.AddWithValue("@Date_Modified", datemo);
                                        cmdauditdata.Parameters.AddWithValue("@Date_Started", datest);
                                        cmdauditdata.ExecuteNonQuery();
                                    }
                                    #endregion
                                    #region SA
                                    else if (sIBASE == "SA")
                                    {
                                        tst = TimeZoneInfo.FindSystemTimeZoneById("AUS Central Standard Time");
                                        creatat = TimeZoneInfo.ConvertTime(createdat, tst);
                                        isDaylight = tst.IsDaylightSavingTime(createdat);
                                        if (isDaylight == true)
                                        {
                                            //dst = TimeZoneInfo.FindSystemTimeZoneById("AUS Central Daylight Time");
                                            creatat = TimeZoneInfo.ConvertTime(createdat, tst);
                                            modat = TimeZoneInfo.ConvertTime(modifiedat, tst);
                                            dateco = TimeZoneInfo.ConvertTime(datec, tst);
                                            datemo = TimeZoneInfo.ConvertTime(datem, tst);
                                            datest = TimeZoneInfo.ConvertTime(dates, tst);
                                            //update audit table
                                            string auditupdate = "UPDATE api_Audit SET Created_at = @Created_at, Modified_at = @Modified_at Where Audit_id = @Audit_ID";

                                            SqlCommand cmd = new SqlCommand(auditupdate, connection);
                                            cmd.Parameters.AddWithValue("@Audit_ID", audit_no);
                                            cmd.Parameters.AddWithValue("@Created_at", creatat);
                                            cmd.Parameters.AddWithValue("@Modified_at", modat);
                                            cmd.ExecuteNonQuery();

                                            string auditdataupdate = "UPDATE api_AuditData SET Date_Completed = @Date_Completed, Date_Modified = @Date_Modified, Date_Started = @Date_Started WHERE Audit_id = @Audit_ID";

                                            SqlCommand cmdauditdata = new SqlCommand(auditdataupdate, connection);
                                            cmdauditdata.Parameters.AddWithValue("@Audit_ID", audit_no);
                                            cmdauditdata.Parameters.AddWithValue("@Date_Completed", dateco);
                                            cmdauditdata.Parameters.AddWithValue("@Date_Modified", datemo);
                                            cmdauditdata.Parameters.AddWithValue("@Date_Started", datest);
                                            cmdauditdata.ExecuteNonQuery();
                                        }
                                        else
                                        {
                                            modat = TimeZoneInfo.ConvertTime(modifiedat, tst);
                                            dateco = TimeZoneInfo.ConvertTime(datec, tst);
                                            datemo = TimeZoneInfo.ConvertTime(datem, tst);
                                            datest = TimeZoneInfo.ConvertTime(dates, tst);
                                            //update audit table
                                            string auditupdate = "UPDATE api_Audit SET Created_at = @Created_at, Modified_at = @Modified_at Where Audit_id = @Audit_ID";

                                            SqlCommand cmd = new SqlCommand(auditupdate, connection);
                                            cmd.Parameters.AddWithValue("@Audit_ID", audit_no);
                                            cmd.Parameters.AddWithValue("@Created_at", creatat);
                                            cmd.Parameters.AddWithValue("@Modified_at", modat);
                                            cmd.ExecuteNonQuery();

                                            //update auditdata
                                            string auditdataupdate = "UPDATE api_AuditData SET Date_Completed = @Date_Completed, Date_Modified = @Date_Modified, Date_Started = @Date_Started WHERE Audit_id = @Audit_ID";
                                            SqlCommand cmdauditdata = new SqlCommand(auditdataupdate, connection);
                                            cmdauditdata.Parameters.AddWithValue("@Audit_ID", audit_no);
                                            cmdauditdata.Parameters.AddWithValue("@Date_Completed", dateco);
                                            cmdauditdata.Parameters.AddWithValue("@Date_Modified", datemo);
                                            cmdauditdata.Parameters.AddWithValue("@Date_Started", datest);
                                            cmdauditdata.ExecuteNonQuery();
                                        }
                                    }
                                    #endregion
                                    #region NT
                                    else if (sIBASE == "NT")
                                    {
                                        tst = TimeZoneInfo.FindSystemTimeZoneById("AUS Central Standard Time");
                                        creatat = TimeZoneInfo.ConvertTime(createdat, tst);
                                        modat = TimeZoneInfo.ConvertTime(modifiedat, tst);
                                        dateco = TimeZoneInfo.ConvertTime(datec, tst);
                                        datemo = TimeZoneInfo.ConvertTime(datem, tst);
                                        datest = TimeZoneInfo.ConvertTime(dates, tst);
                                        //update audit table
                                        string auditupdate = "UPDATE api_Audit SET Created_at = @Created_at, Modified_at = @Modified_at Where Audit_id = @Audit_ID";

                                        SqlCommand cmd = new SqlCommand(auditupdate, connection);
                                        cmd.Parameters.AddWithValue("@Audit_ID", audit_no);
                                        cmd.Parameters.AddWithValue("@Created_at", creatat);
                                        cmd.Parameters.AddWithValue("@Modified_at", modat);
                                        cmd.ExecuteNonQuery();

                                        //update auditdata
                                        string auditdataupdate = "UPDATE api_AuditData SET Date_Completed = @Date_Completed, Date_Modified = @Date_Modified, Date_Started = @Date_Started WHERE Audit_id = @Audit_ID";
                                        SqlCommand cmdauditdata = new SqlCommand(auditdataupdate, connection);
                                        cmdauditdata.Parameters.AddWithValue("@Audit_ID", audit_no);
                                        cmdauditdata.Parameters.AddWithValue("@Date_Completed", dateco);
                                        cmdauditdata.Parameters.AddWithValue("@Date_Modified", datemo);
                                        cmdauditdata.Parameters.AddWithValue("@Date_Started", datest);
                                        cmdauditdata.ExecuteNonQuery();
                                    }
                                    #endregion
                                    #region WA
                                    else if (sIBASE == "WA")
                                    {
                                        tst = TimeZoneInfo.FindSystemTimeZoneById("W. Australia Standard Time");
                                        creatat = TimeZoneInfo.ConvertTime(createdat, tst);
                                        modat = TimeZoneInfo.ConvertTime(modifiedat, tst);
                                        dateco = TimeZoneInfo.ConvertTime(datec, tst);
                                        datemo = TimeZoneInfo.ConvertTime(datem, tst);
                                        datest = TimeZoneInfo.ConvertTime(dates, tst);
                                        //update audit table
                                        string auditupdate = "UPDATE api_Audit SET Created_at = @Created_at, Modified_at = @Modified_at Where Audit_id = @Audit_ID";

                                        SqlCommand cmd = new SqlCommand(auditupdate, connection);
                                        cmd.Parameters.AddWithValue("@Audit_ID", audit_no);
                                        cmd.Parameters.AddWithValue("@Created_at", creatat);
                                        cmd.Parameters.AddWithValue("@Modified_at", modat);
                                        cmd.ExecuteNonQuery();

                                        //update auditdata
                                        string auditdataupdate = "UPDATE api_AuditData SET Date_Completed = @Date_Completed, Date_Modified = @Date_Modified, Date_Started = @Date_Started WHERE Audit_id = @Audit_ID";
                                        SqlCommand cmdauditdata = new SqlCommand(auditdataupdate, connection);
                                        cmdauditdata.Parameters.AddWithValue("@Audit_ID", audit_no);
                                        cmdauditdata.Parameters.AddWithValue("@Date_Completed", dateco);
                                        cmdauditdata.Parameters.AddWithValue("@Date_Modified", datemo);
                                        cmdauditdata.Parameters.AddWithValue("@Date_Started", datest);
                                        cmdauditdata.ExecuteNonQuery();
                                    }
                                    #endregion
                                    #region NZ
                                    else if (sIBASE == "AKL" || sIBASE == "AUK" || sIBASE == "BOP" || sIBASE == "CAN" || sIBASE == "GIS" || sIBASE == "HKB" || sIBASE == "MBH" || sIBASE == "MWT" || sIBASE == "NSN" || sIBASE == "NTL" || sIBASE == "OTA" || sIBASE == "STL" || sIBASE == "TKI" || sIBASE == "WGN" || sIBASE == "WKO" || sIBASE == "WTC")
                                    {
                                        createdat = DateTimeOffset.Parse(created_at, System.Globalization.CultureInfo.GetCultureInfo("en-NZ"));
                                        modifiedat = DateTimeOffset.Parse(modified_at, System.Globalization.CultureInfo.GetCultureInfo("en-NZ"));
                                        datec = DateTimeOffset.Parse(datecompleted, System.Globalization.CultureInfo.GetCultureInfo("en-NZ"));
                                        datem = DateTimeOffset.Parse(datemodified, System.Globalization.CultureInfo.GetCultureInfo("en-NZ"));
                                        dates = DateTimeOffset.Parse(datestarted, System.Globalization.CultureInfo.GetCultureInfo("en-NZ"));

                                        tst = TimeZoneInfo.FindSystemTimeZoneById("New Zealand Standard Time");
                                        creatat = TimeZoneInfo.ConvertTime(createdat, tst);
                                        isDaylight = tst.IsDaylightSavingTime(createdat);
                                        if (isDaylight == true)
                                        {
                                            //dst = TimeZoneInfo.FindSystemTimeZoneById("New Zealand Daylight Time");
                                            creatat = TimeZoneInfo.ConvertTime(createdat, tst);
                                            modat = TimeZoneInfo.ConvertTime(modifiedat, tst);
                                            dateco = TimeZoneInfo.ConvertTime(datec, tst);
                                            datemo = TimeZoneInfo.ConvertTime(datem, tst);
                                            datest = TimeZoneInfo.ConvertTime(dates, tst);
                                            //update audit table
                                            string auditupdate = "UPDATE api_Audit SET Created_at = @Created_at, Modified_at = @Modified_at Where Audit_id = @Audit_ID";

                                            SqlCommand cmd = new SqlCommand(auditupdate, connection);
                                            cmd.Parameters.AddWithValue("@Audit_ID", audit_no);
                                            cmd.Parameters.AddWithValue("@Created_at", creatat);
                                            cmd.Parameters.AddWithValue("@Modified_at", modat);
                                            cmd.ExecuteNonQuery();

                                            string auditdataupdate = "UPDATE api_AuditData SET Date_Completed = @Date_Completed, Date_Modified = @Date_Modified, Date_Started = @Date_Started WHERE Audit_id = @Audit_ID";

                                            SqlCommand cmdauditdata = new SqlCommand(auditdataupdate, connection);
                                            cmdauditdata.Parameters.AddWithValue("@Audit_ID", audit_no);
                                            cmdauditdata.Parameters.AddWithValue("@Date_Completed", dateco);
                                            cmdauditdata.Parameters.AddWithValue("@Date_Modified", datemo);
                                            cmdauditdata.Parameters.AddWithValue("@Date_Started", datest);
                                            cmdauditdata.ExecuteNonQuery();
                                        }
                                        else
                                        {
                                            modat = TimeZoneInfo.ConvertTime(modifiedat, tst);
                                            dateco = TimeZoneInfo.ConvertTime(datec, tst);
                                            datemo = TimeZoneInfo.ConvertTime(datem, tst);
                                            datest = TimeZoneInfo.ConvertTime(dates, tst);
                                            //update audit table
                                            string auditupdate = "UPDATE api_Audit SET Created_at = @Created_at, Modified_at = @Modified_at Where Audit_id = @Audit_ID";

                                            SqlCommand cmd = new SqlCommand(auditupdate, connection);
                                            cmd.Parameters.AddWithValue("@Audit_ID", audit_no);
                                            cmd.Parameters.AddWithValue("@Created_at", creatat);
                                            cmd.Parameters.AddWithValue("@Modified_at", modat);
                                            cmd.ExecuteNonQuery();

                                            //update auditdata
                                            string auditdataupdate = "UPDATE api_AuditData SET Date_Completed = @Date_Completed, Date_Modified = @Date_Modified, Date_Started = @Date_Started WHERE Audit_id = @Audit_ID";
                                            SqlCommand cmdauditdata = new SqlCommand(auditdataupdate, connection);
                                            cmdauditdata.Parameters.AddWithValue("@Audit_ID", audit_no);
                                            cmdauditdata.Parameters.AddWithValue("@Date_Completed", dateco);
                                            cmdauditdata.Parameters.AddWithValue("@Date_Modified", datemo);
                                            cmdauditdata.Parameters.AddWithValue("@Date_Started", datest);
                                            cmdauditdata.ExecuteNonQuery();
                                        }
                                    }
                                    #endregion
                                    #region TAS
                                    else if (sIBASE == "TAS")
                                    {
                                        //get the country
                                        var ibasecountry = "SELECT Country FROM PRD.IBASES WHERE (FLNumber = @FLNumber and Region = @Region)";
                                        var ibasecountries = new SqlCommand(ibasecountry, connectIBASE);
                                        var systid = "800-" + systemid;
                                        var checksid = ibasecountries.Parameters.AddWithValue("@FLNumber", sysid);
                                        if (systid == null) checksid.Value = DBNull.Value;
                                        var checksreg = ibasecountries.Parameters.AddWithValue("@Region", "TAS");
                                        ibasecountries.ExecuteNonQuery();
                                        var sIBASECountry = (String)ibasecountries.ExecuteScalar();
                                        if (sIBASECountry != null)
                                        {
                                            #region AU
                                            if (sIBASECountry == "AU")
                                            {
                                                tst = TimeZoneInfo.FindSystemTimeZoneById("AUS Eastern Standard Time");
                                                creatat = TimeZoneInfo.ConvertTime(createdat, tst);
                                                isDaylight = tst.IsDaylightSavingTime(createdat);
                                                if (isDaylight == true)
                                                {
                                                    //dst = TimeZoneInfo.FindSystemTimeZoneById("AUS Eastern Daylight Time");
                                                    creatat = TimeZoneInfo.ConvertTime(createdat, tst);
                                                    modat = TimeZoneInfo.ConvertTime(modifiedat, tst);
                                                    dateco = TimeZoneInfo.ConvertTime(datec, tst);
                                                    datemo = TimeZoneInfo.ConvertTime(datem, tst);
                                                    datest = TimeZoneInfo.ConvertTime(dates, tst);
                                                    //update audit table
                                                    string auditupdate = "UPDATE api_Audit SET Created_at = @Created_at, Modified_at = @Modified_at Where Audit_id = @Audit_ID";

                                                    SqlCommand cmd = new SqlCommand(auditupdate, connection);
                                                    cmd.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                    cmd.Parameters.AddWithValue("@Created_at", creatat);
                                                    cmd.Parameters.AddWithValue("@Modified_at", modat);
                                                    cmd.ExecuteNonQuery();

                                                    string auditdataupdate = "UPDATE api_AuditData SET Date_Completed = @Date_Completed, Date_Modified = @Date_Modified, Date_Started = @Date_Started WHERE Audit_id = @Audit_ID";

                                                    SqlCommand cmdauditdata = new SqlCommand(auditdataupdate, connection);
                                                    cmdauditdata.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                    cmdauditdata.Parameters.AddWithValue("@Date_Completed", dateco);
                                                    cmdauditdata.Parameters.AddWithValue("@Date_Modified", datemo);
                                                    cmdauditdata.Parameters.AddWithValue("@Date_Started", datest);
                                                    cmdauditdata.ExecuteNonQuery();
                                                }
                                                else
                                                {
                                                    modat = TimeZoneInfo.ConvertTime(modifiedat, tst);
                                                    dateco = TimeZoneInfo.ConvertTime(datec, tst);
                                                    datemo = TimeZoneInfo.ConvertTime(datem, tst);
                                                    datest = TimeZoneInfo.ConvertTime(dates, tst);
                                                    //update audit table
                                                    string auditupdate = "UPDATE api_Audit SET Created_at = @Created_at, Modified_at = @Modified_at Where Audit_id = @Audit_ID";

                                                    SqlCommand cmd = new SqlCommand(auditupdate, connection);
                                                    cmd.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                    cmd.Parameters.AddWithValue("@Created_at", creatat);
                                                    cmd.Parameters.AddWithValue("@Modified_at", modat);
                                                    cmd.ExecuteNonQuery();

                                                    //update auditdata
                                                    string auditdataupdate = "UPDATE api_AuditData SET Date_Completed = @Date_Completed, Date_Modified = @Date_Modified, Date_Started = @Date_Started WHERE Audit_id = @Audit_ID";
                                                    SqlCommand cmdauditdata = new SqlCommand(auditdataupdate, connection);
                                                    cmdauditdata.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                    cmdauditdata.Parameters.AddWithValue("@Date_Completed", dateco);
                                                    cmdauditdata.Parameters.AddWithValue("@Date_Modified", datemo);
                                                    cmdauditdata.Parameters.AddWithValue("@Date_Started", datest);
                                                    cmdauditdata.ExecuteNonQuery();
                                                }
                                            }
                                            #endregion
                                            #region NZ
                                            else if (sIBASECountry == "NZ")
                                            {
                                                createdat = DateTimeOffset.Parse(created_at, System.Globalization.CultureInfo.GetCultureInfo("en-NZ"));
                                                modifiedat = DateTimeOffset.Parse(modified_at, System.Globalization.CultureInfo.GetCultureInfo("en-NZ"));
                                                datec = DateTimeOffset.Parse(datecompleted, System.Globalization.CultureInfo.GetCultureInfo("en-NZ"));
                                                datem = DateTimeOffset.Parse(datemodified, System.Globalization.CultureInfo.GetCultureInfo("en-NZ"));
                                                dates = DateTimeOffset.Parse(datestarted, System.Globalization.CultureInfo.GetCultureInfo("en-NZ"));

                                                tst = TimeZoneInfo.FindSystemTimeZoneById("New Zealand Standard Time");
                                                creatat = TimeZoneInfo.ConvertTime(createdat, tst);
                                                isDaylight = tst.IsDaylightSavingTime(createdat);
                                                if (isDaylight == true)
                                                {
                                                    //dst = TimeZoneInfo.FindSystemTimeZoneById("New Zealand Daylight Time");
                                                    creatat = TimeZoneInfo.ConvertTime(createdat, tst);
                                                    modat = TimeZoneInfo.ConvertTime(modifiedat, tst);
                                                    dateco = TimeZoneInfo.ConvertTime(datec, tst);
                                                    datemo = TimeZoneInfo.ConvertTime(datem, tst);
                                                    datest = TimeZoneInfo.ConvertTime(dates, tst);
                                                    //update audit table
                                                    string auditupdate = "UPDATE api_Audit SET Created_at = @Created_at, Modified_at = @Modified_at Where Audit_id = @Audit_ID";

                                                    SqlCommand cmd = new SqlCommand(auditupdate, connection);
                                                    cmd.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                    cmd.Parameters.AddWithValue("@Created_at", creatat);
                                                    cmd.Parameters.AddWithValue("@Modified_at", modat);
                                                    cmd.ExecuteNonQuery();

                                                    string auditdataupdate = "UPDATE api_AuditData SET Date_Completed = @Date_Completed, Date_Modified = @Date_Modified, Date_Started = @Date_Started WHERE Audit_id = @Audit_ID";

                                                    SqlCommand cmdauditdata = new SqlCommand(auditdataupdate, connection);
                                                    cmdauditdata.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                    cmdauditdata.Parameters.AddWithValue("@Date_Completed", dateco);
                                                    cmdauditdata.Parameters.AddWithValue("@Date_Modified", datemo);
                                                    cmdauditdata.Parameters.AddWithValue("@Date_Started", datest);
                                                    cmdauditdata.ExecuteNonQuery();
                                                }
                                                else
                                                {
                                                    modat = TimeZoneInfo.ConvertTime(modifiedat, tst);
                                                    dateco = TimeZoneInfo.ConvertTime(datec, tst);
                                                    datemo = TimeZoneInfo.ConvertTime(datem, tst);
                                                    datest = TimeZoneInfo.ConvertTime(dates, tst);
                                                    //update audit table
                                                    string auditupdate = "UPDATE api_Audit SET Created_at = @Created_at, Modified_at = @Modified_at Where Audit_id = @Audit_ID";

                                                    SqlCommand cmd = new SqlCommand(auditupdate, connection);
                                                    cmd.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                    cmd.Parameters.AddWithValue("@Created_at", creatat);
                                                    cmd.Parameters.AddWithValue("@Modified_at", modat);
                                                    cmd.ExecuteNonQuery();

                                                    //update auditdata
                                                    string auditdataupdate = "UPDATE api_AuditData SET Date_Completed = @Date_Completed, Date_Modified = @Date_Modified, Date_Started = @Date_Started WHERE Audit_id = @Audit_ID";
                                                    SqlCommand cmdauditdata = new SqlCommand(auditdataupdate, connection);
                                                    cmdauditdata.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                    cmdauditdata.Parameters.AddWithValue("@Date_Completed", dateco);
                                                    cmdauditdata.Parameters.AddWithValue("@Date_Modified", datemo);
                                                    cmdauditdata.Parameters.AddWithValue("@Date_Started", datest);
                                                    cmdauditdata.ExecuteNonQuery();
                                                }
                                            }
                                            #endregion
                                        }
                                    }
                                    #endregion

                                }
                                #endregion
                                #region if System ID is not valid then get the Region of Employee
                                else
                                {
                                    string[] names = owner.ToString().Trim().Split(new char[] { ' ' }, 2);
                                    string fname = "";
                                    string lname = "";

                                    if (names.Length == 1)
                                    {
                                        fname = "";
                                        lname = names[0];
                                    }
                                    else
                                    {
                                        fname = names[0];
                                        lname = names[1];
                                    }

                                    var empReg = "SELECT Region FROM PRD.Employees WHERE (GivenName = @GivenName and LastName = @LastName and len(id) = 8 and (country <> '#N/A' and country <> ''))";
                                    var emps = new SqlCommand(empReg, connectIBASE);
                                    var checkf = emps.Parameters.AddWithValue("@GivenName", fname);
                                    if (fname == null) checkf.Value = DBNull.Value;
                                    var checkl = emps.Parameters.AddWithValue("@LastName", lname);
                                    if (lname == null) checkl.Value = DBNull.Value;
                                    emps.ExecuteNonQuery();
                                    var sEMP = (String)emps.ExecuteScalar();

                                    if (sEMP != null)
                                    {
                                        ////for timezone settings
                                        DateTime date;
                                        TimeZoneInfo tst;
                                        TimeZoneInfo dst;
                                        bool isDaylight;

                                        createdat = DateTimeOffset.Parse(created_at, System.Globalization.CultureInfo.GetCultureInfo("en-AU"));
                                        modifiedat = DateTimeOffset.Parse(modified_at, System.Globalization.CultureInfo.GetCultureInfo("en-AU"));
                                        datec = DateTimeOffset.Parse(datecompleted, System.Globalization.CultureInfo.GetCultureInfo("en-AU"));
                                        datem = DateTimeOffset.Parse(datemodified, System.Globalization.CultureInfo.GetCultureInfo("en-AU"));
                                        dates = DateTimeOffset.Parse(datestarted, System.Globalization.CultureInfo.GetCultureInfo("en-AU"));

                                        #region ACT, VIC, NSW
                                        if (sEMP == "ACT" || sEMP == "VIC" || sEMP == "NSW")
                                        {
                                            tst = TimeZoneInfo.FindSystemTimeZoneById("AUS Eastern Standard Time");
                                            creatat = TimeZoneInfo.ConvertTime(createdat, tst);
                                            isDaylight = tst.IsDaylightSavingTime(createdat);
                                            if (isDaylight == true)
                                            {
                                                //dst = TimeZoneInfo.FindSystemTimeZoneById("AUS Eastern Daylight Time");
                                                creatat = TimeZoneInfo.ConvertTime(createdat, tst);
                                                modat = TimeZoneInfo.ConvertTime(modifiedat, tst);
                                                dateco = TimeZoneInfo.ConvertTime(datec, tst);
                                                datemo = TimeZoneInfo.ConvertTime(datem, tst);
                                                datest = TimeZoneInfo.ConvertTime(dates, tst);
                                                //update audit table
                                                string auditupdate = "UPDATE api_Audit SET Created_at = @Created_at, Modified_at = @Modified_at Where Audit_id = @Audit_ID";

                                                SqlCommand cmd = new SqlCommand(auditupdate, connection);
                                                cmd.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                cmd.Parameters.AddWithValue("@Created_at", creatat);
                                                cmd.Parameters.AddWithValue("@Modified_at", modat);
                                                cmd.ExecuteNonQuery();

                                                string auditdataupdate = "UPDATE api_AuditData SET Date_Completed = @Date_Completed, Date_Modified = @Date_Modified, Date_Started = @Date_Started WHERE Audit_id = @Audit_ID";

                                                SqlCommand cmdauditdata = new SqlCommand(auditdataupdate, connection);
                                                cmdauditdata.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                cmdauditdata.Parameters.AddWithValue("@Date_Completed", dateco);
                                                cmdauditdata.Parameters.AddWithValue("@Date_Modified", datemo);
                                                cmdauditdata.Parameters.AddWithValue("@Date_Started", datest);
                                                cmdauditdata.ExecuteNonQuery();
                                            }
                                            else
                                            {
                                                modat = TimeZoneInfo.ConvertTime(modifiedat, tst);
                                                dateco = TimeZoneInfo.ConvertTime(datec, tst);
                                                datemo = TimeZoneInfo.ConvertTime(datem, tst);
                                                datest = TimeZoneInfo.ConvertTime(dates, tst);
                                                //update audit table
                                                string auditupdate = "UPDATE api_Audit SET Created_at = @Created_at, Modified_at = @Modified_at Where Audit_id = @Audit_ID";

                                                SqlCommand cmd = new SqlCommand(auditupdate, connection);
                                                cmd.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                cmd.Parameters.AddWithValue("@Created_at", creatat);
                                                cmd.Parameters.AddWithValue("@Modified_at", modat);
                                                cmd.ExecuteNonQuery();

                                                //update auditdata
                                                string auditdataupdate = "UPDATE api_AuditData SET Date_Completed = @Date_Completed, Date_Modified = @Date_Modified, Date_Started = @Date_Started WHERE Audit_id = @Audit_ID";
                                                SqlCommand cmdauditdata = new SqlCommand(auditdataupdate, connection);
                                                cmdauditdata.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                cmdauditdata.Parameters.AddWithValue("@Date_Completed", dateco);
                                                cmdauditdata.Parameters.AddWithValue("@Date_Modified", datemo);
                                                cmdauditdata.Parameters.AddWithValue("@Date_Started", datest);
                                                cmdauditdata.ExecuteNonQuery();
                                            }
                                        }
                                        #endregion
                                        #region QLD
                                        else if (sEMP == "QLD")
                                        {
                                            tst = TimeZoneInfo.FindSystemTimeZoneById("E. Australia Standard Time");
                                            creatat = TimeZoneInfo.ConvertTime(createdat, tst);
                                            modat = TimeZoneInfo.ConvertTime(modifiedat, tst);
                                            dateco = TimeZoneInfo.ConvertTime(datec, tst);
                                            datemo = TimeZoneInfo.ConvertTime(datem, tst);
                                            datest = TimeZoneInfo.ConvertTime(dates, tst);
                                            //update audit table
                                            string auditupdate = "UPDATE api_Audit SET Created_at = @Created_at, Modified_at = @Modified_at Where Audit_id = @Audit_ID";

                                            SqlCommand cmd = new SqlCommand(auditupdate, connection);
                                            cmd.Parameters.AddWithValue("@Audit_ID", audit_no);
                                            cmd.Parameters.AddWithValue("@Created_at", creatat);
                                            cmd.Parameters.AddWithValue("@Modified_at", modat);
                                            cmd.ExecuteNonQuery();

                                            //update auditdata
                                            string auditdataupdate = "UPDATE api_AuditData SET Date_Completed = @Date_Completed, Date_Modified = @Date_Modified, Date_Started = @Date_Started WHERE Audit_id = @Audit_ID";
                                            SqlCommand cmdauditdata = new SqlCommand(auditdataupdate, connection);
                                            cmdauditdata.Parameters.AddWithValue("@Audit_ID", audit_no);
                                            cmdauditdata.Parameters.AddWithValue("@Date_Completed", dateco);
                                            cmdauditdata.Parameters.AddWithValue("@Date_Modified", datemo);
                                            cmdauditdata.Parameters.AddWithValue("@Date_Started", datest);
                                            cmdauditdata.ExecuteNonQuery();
                                        }
                                        #endregion
                                        #region SA
                                        else if (sEMP == "SA")
                                        {
                                            tst = TimeZoneInfo.FindSystemTimeZoneById("AUS Central Standard Time");
                                            creatat = TimeZoneInfo.ConvertTime(createdat, tst);
                                            isDaylight = tst.IsDaylightSavingTime(createdat);
                                            if (isDaylight == true)
                                            {
                                                //dst = TimeZoneInfo.FindSystemTimeZoneById("AUS Central Daylight Time");
                                                creatat = TimeZoneInfo.ConvertTime(createdat, tst);
                                                modat = TimeZoneInfo.ConvertTime(modifiedat, tst);
                                                dateco = TimeZoneInfo.ConvertTime(datec, tst);
                                                datemo = TimeZoneInfo.ConvertTime(datem, tst);
                                                datest = TimeZoneInfo.ConvertTime(dates, tst);
                                                //update audit table
                                                string auditupdate = "UPDATE api_Audit SET Created_at = @Created_at, Modified_at = @Modified_at Where Audit_id = @Audit_ID";

                                                SqlCommand cmd = new SqlCommand(auditupdate, connection);
                                                cmd.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                cmd.Parameters.AddWithValue("@Created_at", creatat);
                                                cmd.Parameters.AddWithValue("@Modified_at", modat);
                                                cmd.ExecuteNonQuery();

                                                string auditdataupdate = "UPDATE api_AuditData SET Date_Completed = @Date_Completed, Date_Modified = @Date_Modified, Date_Started = @Date_Started WHERE Audit_id = @Audit_ID";

                                                SqlCommand cmdauditdata = new SqlCommand(auditdataupdate, connection);
                                                cmdauditdata.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                cmdauditdata.Parameters.AddWithValue("@Date_Completed", dateco);
                                                cmdauditdata.Parameters.AddWithValue("@Date_Modified", datemo);
                                                cmdauditdata.Parameters.AddWithValue("@Date_Started", datest);
                                                cmdauditdata.ExecuteNonQuery();
                                            }
                                            else
                                            {
                                                modat = TimeZoneInfo.ConvertTime(modifiedat, tst);
                                                dateco = TimeZoneInfo.ConvertTime(datec, tst);
                                                datemo = TimeZoneInfo.ConvertTime(datem, tst);
                                                datest = TimeZoneInfo.ConvertTime(dates, tst);
                                                //update audit table
                                                string auditupdate = "UPDATE api_Audit SET Created_at = @Created_at, Modified_at = @Modified_at Where Audit_id = @Audit_ID";

                                                SqlCommand cmd = new SqlCommand(auditupdate, connection);
                                                cmd.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                cmd.Parameters.AddWithValue("@Created_at", creatat);
                                                cmd.Parameters.AddWithValue("@Modified_at", modat);
                                                cmd.ExecuteNonQuery();

                                                //update auditdata
                                                string auditdataupdate = "UPDATE api_AuditData SET Date_Completed = @Date_Completed, Date_Modified = @Date_Modified, Date_Started = @Date_Started WHERE Audit_id = @Audit_ID";
                                                SqlCommand cmdauditdata = new SqlCommand(auditdataupdate, connection);
                                                cmdauditdata.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                cmdauditdata.Parameters.AddWithValue("@Date_Completed", dateco);
                                                cmdauditdata.Parameters.AddWithValue("@Date_Modified", datemo);
                                                cmdauditdata.Parameters.AddWithValue("@Date_Started", datest);
                                                cmdauditdata.ExecuteNonQuery();
                                            }
                                        }
                                        #endregion
                                        #region NT
                                        else if (sEMP == "NT")
                                        {
                                            tst = TimeZoneInfo.FindSystemTimeZoneById("AUS Central Standard Time");
                                            creatat = TimeZoneInfo.ConvertTime(createdat, tst);
                                            modat = TimeZoneInfo.ConvertTime(modifiedat, tst);
                                            dateco = TimeZoneInfo.ConvertTime(datec, tst);
                                            datemo = TimeZoneInfo.ConvertTime(datem, tst);
                                            datest = TimeZoneInfo.ConvertTime(dates, tst);
                                            //update audit table
                                            string auditupdate = "UPDATE api_Audit SET Created_at = @Created_at, Modified_at = @Modified_at Where Audit_id = @Audit_ID";

                                            SqlCommand cmd = new SqlCommand(auditupdate, connection);
                                            cmd.Parameters.AddWithValue("@Audit_ID", audit_no);
                                            cmd.Parameters.AddWithValue("@Created_at", creatat);
                                            cmd.Parameters.AddWithValue("@Modified_at", modat);
                                            cmd.ExecuteNonQuery();

                                            //update auditdata
                                            string auditdataupdate = "UPDATE api_AuditData SET Date_Completed = @Date_Completed, Date_Modified = @Date_Modified, Date_Started = @Date_Started WHERE Audit_id = @Audit_ID";
                                            SqlCommand cmdauditdata = new SqlCommand(auditdataupdate, connection);
                                            cmdauditdata.Parameters.AddWithValue("@Audit_ID", audit_no);
                                            cmdauditdata.Parameters.AddWithValue("@Date_Completed", dateco);
                                            cmdauditdata.Parameters.AddWithValue("@Date_Modified", datemo);
                                            cmdauditdata.Parameters.AddWithValue("@Date_Started", datest);
                                            cmdauditdata.ExecuteNonQuery();
                                        }
                                        #endregion
                                        #region WA
                                        else if (sEMP == "WA")
                                        {
                                            tst = TimeZoneInfo.FindSystemTimeZoneById("W. Australia Standard Time");
                                            creatat = TimeZoneInfo.ConvertTime(createdat, tst);
                                            modat = TimeZoneInfo.ConvertTime(modifiedat, tst);
                                            dateco = TimeZoneInfo.ConvertTime(datec, tst);
                                            datemo = TimeZoneInfo.ConvertTime(datem, tst);
                                            datest = TimeZoneInfo.ConvertTime(dates, tst);
                                            //update audit table
                                            string auditupdate = "UPDATE api_Audit SET Created_at = @Created_at, Modified_at = @Modified_at Where Audit_id = @Audit_ID";

                                            SqlCommand cmd = new SqlCommand(auditupdate, connection);
                                            cmd.Parameters.AddWithValue("@Audit_ID", audit_no);
                                            cmd.Parameters.AddWithValue("@Created_at", creatat);
                                            cmd.Parameters.AddWithValue("@Modified_at", modat);
                                            cmd.ExecuteNonQuery();

                                            //update auditdata
                                            string auditdataupdate = "UPDATE api_AuditData SET Date_Completed = @Date_Completed, Date_Modified = @Date_Modified, Date_Started = @Date_Started WHERE Audit_id = @Audit_ID";
                                            SqlCommand cmdauditdata = new SqlCommand(auditdataupdate, connection);
                                            cmdauditdata.Parameters.AddWithValue("@Audit_ID", audit_no);
                                            cmdauditdata.Parameters.AddWithValue("@Date_Completed", dateco);
                                            cmdauditdata.Parameters.AddWithValue("@Date_Modified", datemo);
                                            cmdauditdata.Parameters.AddWithValue("@Date_Started", datest);
                                            cmdauditdata.ExecuteNonQuery();
                                        }
                                        #endregion
                                        #region NZ
                                        else if (sEMP == "AKL" || sEMP == "AUK" || sEMP == "BOP" || sEMP == "CAN" || sEMP == "GIS" || sEMP == "HKB" || sEMP == "MBH" || sEMP == "MWT" || sEMP == "NSN" || sEMP == "NTL" || sEMP == "OTA" || sEMP == "STL" || sEMP == "TKI" || sEMP == "WGN" || sEMP == "WKO" || sEMP == "WTC")
                                        {
                                            createdat = DateTimeOffset.Parse(created_at, System.Globalization.CultureInfo.GetCultureInfo("en-NZ"));
                                            modifiedat = DateTimeOffset.Parse(modified_at, System.Globalization.CultureInfo.GetCultureInfo("en-NZ"));
                                            datec = DateTimeOffset.Parse(datecompleted, System.Globalization.CultureInfo.GetCultureInfo("en-NZ"));
                                            datem = DateTimeOffset.Parse(datemodified, System.Globalization.CultureInfo.GetCultureInfo("en-NZ"));
                                            dates = DateTimeOffset.Parse(datestarted, System.Globalization.CultureInfo.GetCultureInfo("en-NZ"));

                                            tst = TimeZoneInfo.FindSystemTimeZoneById("New Zealand Standard Time");
                                            creatat = TimeZoneInfo.ConvertTime(createdat, tst);
                                            isDaylight = tst.IsDaylightSavingTime(createdat);
                                            if (isDaylight == true)
                                            {
                                                //dst = TimeZoneInfo.FindSystemTimeZoneById("New Zealand Daylight Time");
                                                creatat = TimeZoneInfo.ConvertTime(createdat, tst);
                                                modat = TimeZoneInfo.ConvertTime(modifiedat, tst);
                                                dateco = TimeZoneInfo.ConvertTime(datec, tst);
                                                datemo = TimeZoneInfo.ConvertTime(datem, tst);
                                                datest = TimeZoneInfo.ConvertTime(dates, tst);
                                                //update audit table
                                                string auditupdate = "UPDATE api_Audit SET Created_at = @Created_at, Modified_at = @Modified_at Where Audit_id = @Audit_ID";

                                                SqlCommand cmd = new SqlCommand(auditupdate, connection);
                                                cmd.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                cmd.Parameters.AddWithValue("@Created_at", creatat);
                                                cmd.Parameters.AddWithValue("@Modified_at", modat);
                                                cmd.ExecuteNonQuery();

                                                string auditdataupdate = "UPDATE api_AuditData SET Date_Completed = @Date_Completed, Date_Modified = @Date_Modified, Date_Started = @Date_Started WHERE Audit_id = @Audit_ID";

                                                SqlCommand cmdauditdata = new SqlCommand(auditdataupdate, connection);
                                                cmdauditdata.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                cmdauditdata.Parameters.AddWithValue("@Date_Completed", dateco);
                                                cmdauditdata.Parameters.AddWithValue("@Date_Modified", datemo);
                                                cmdauditdata.Parameters.AddWithValue("@Date_Started", datest);
                                                cmdauditdata.ExecuteNonQuery();
                                            }
                                            else
                                            {
                                                modat = TimeZoneInfo.ConvertTime(modifiedat, tst);
                                                dateco = TimeZoneInfo.ConvertTime(datec, tst);
                                                datemo = TimeZoneInfo.ConvertTime(datem, tst);
                                                datest = TimeZoneInfo.ConvertTime(dates, tst);
                                                //update audit table
                                                string auditupdate = "UPDATE api_Audit SET Created_at = @Created_at, Modified_at = @Modified_at Where Audit_id = @Audit_ID";

                                                SqlCommand cmd = new SqlCommand(auditupdate, connection);
                                                cmd.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                cmd.Parameters.AddWithValue("@Created_at", creatat);
                                                cmd.Parameters.AddWithValue("@Modified_at", modat);
                                                cmd.ExecuteNonQuery();

                                                //update auditdata
                                                string auditdataupdate = "UPDATE api_AuditData SET Date_Completed = @Date_Completed, Date_Modified = @Date_Modified, Date_Started = @Date_Started WHERE Audit_id = @Audit_ID";
                                                SqlCommand cmdauditdata = new SqlCommand(auditdataupdate, connection);
                                                cmdauditdata.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                cmdauditdata.Parameters.AddWithValue("@Date_Completed", dateco);
                                                cmdauditdata.Parameters.AddWithValue("@Date_Modified", datemo);
                                                cmdauditdata.Parameters.AddWithValue("@Date_Started", datest);
                                                cmdauditdata.ExecuteNonQuery();
                                            }
                                        }
                                        #endregion
                                        #region TAS
                                        else if (sEMP == "TAS")
                                        {
                                            //get the country
                                            var ibasecountry = "SELECT Country FROM PRD.IBASES WHERE (FLNumber = @FLNumber and Region = @Region)";
                                            var ibasecountries = new SqlCommand(ibasecountry, connectIBASE);
                                            var systid = "800-" + systemid;
                                            var checksid = ibasecountries.Parameters.AddWithValue("@FLNumber", sysid);
                                            if (systid == null) checksid.Value = DBNull.Value;
                                            var checksreg = ibasecountries.Parameters.AddWithValue("@Region", "TAS");
                                            ibasecountries.ExecuteNonQuery();
                                            var sIBASECountry = (String)ibasecountries.ExecuteScalar();
                                            if (sIBASECountry != null)
                                            {
                                                #region AU
                                                if (sIBASECountry == "AU")
                                                {
                                                    tst = TimeZoneInfo.FindSystemTimeZoneById("AUS Eastern Standard Time");
                                                    creatat = TimeZoneInfo.ConvertTime(createdat, tst);
                                                    isDaylight = tst.IsDaylightSavingTime(createdat);
                                                    if (isDaylight == true)
                                                    {
                                                        //dst = TimeZoneInfo.FindSystemTimeZoneById("AUS Eastern Daylight Time");
                                                        creatat = TimeZoneInfo.ConvertTime(createdat, tst);
                                                        modat = TimeZoneInfo.ConvertTime(modifiedat, tst);
                                                        dateco = TimeZoneInfo.ConvertTime(datec, tst);
                                                        datemo = TimeZoneInfo.ConvertTime(datem, tst);
                                                        datest = TimeZoneInfo.ConvertTime(dates, tst);
                                                        //update audit table
                                                        string auditupdate = "UPDATE api_Audit SET Created_at = @Created_at, Modified_at = @Modified_at Where Audit_id = @Audit_ID";

                                                        SqlCommand cmd = new SqlCommand(auditupdate, connection);
                                                        cmd.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                        cmd.Parameters.AddWithValue("@Created_at", creatat);
                                                        cmd.Parameters.AddWithValue("@Modified_at", modat);
                                                        cmd.ExecuteNonQuery();

                                                        string auditdataupdate = "UPDATE api_AuditData SET Date_Completed = @Date_Completed, Date_Modified = @Date_Modified, Date_Started = @Date_Started WHERE Audit_id = @Audit_ID";

                                                        SqlCommand cmdauditdata = new SqlCommand(auditdataupdate, connection);
                                                        cmdauditdata.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                        cmdauditdata.Parameters.AddWithValue("@Date_Completed", dateco);
                                                        cmdauditdata.Parameters.AddWithValue("@Date_Modified", datemo);
                                                        cmdauditdata.Parameters.AddWithValue("@Date_Started", datest);
                                                        cmdauditdata.ExecuteNonQuery();
                                                    }
                                                    else
                                                    {
                                                        modat = TimeZoneInfo.ConvertTime(modifiedat, tst);
                                                        dateco = TimeZoneInfo.ConvertTime(datec, tst);
                                                        datemo = TimeZoneInfo.ConvertTime(datem, tst);
                                                        datest = TimeZoneInfo.ConvertTime(dates, tst);
                                                        //update audit table
                                                        string auditupdate = "UPDATE api_Audit SET Created_at = @Created_at, Modified_at = @Modified_at Where Audit_id = @Audit_ID";

                                                        SqlCommand cmd = new SqlCommand(auditupdate, connection);
                                                        cmd.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                        cmd.Parameters.AddWithValue("@Created_at", creatat);
                                                        cmd.Parameters.AddWithValue("@Modified_at", modat);
                                                        cmd.ExecuteNonQuery();

                                                        //update auditdata
                                                        string auditdataupdate = "UPDATE api_AuditData SET Date_Completed = @Date_Completed, Date_Modified = @Date_Modified, Date_Started = @Date_Started WHERE Audit_id = @Audit_ID";
                                                        SqlCommand cmdauditdata = new SqlCommand(auditdataupdate, connection);
                                                        cmdauditdata.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                        cmdauditdata.Parameters.AddWithValue("@Date_Completed", dateco);
                                                        cmdauditdata.Parameters.AddWithValue("@Date_Modified", datemo);
                                                        cmdauditdata.Parameters.AddWithValue("@Date_Started", datest);
                                                        cmdauditdata.ExecuteNonQuery();
                                                    }
                                                }
                                                #endregion
                                                #region NZ
                                                else if (sIBASECountry == "NZ")
                                                {
                                                    createdat = DateTimeOffset.Parse(created_at, System.Globalization.CultureInfo.GetCultureInfo("en-NZ"));
                                                    modifiedat = DateTimeOffset.Parse(modified_at, System.Globalization.CultureInfo.GetCultureInfo("en-NZ"));
                                                    datec = DateTimeOffset.Parse(datecompleted, System.Globalization.CultureInfo.GetCultureInfo("en-NZ"));
                                                    datem = DateTimeOffset.Parse(datemodified, System.Globalization.CultureInfo.GetCultureInfo("en-NZ"));
                                                    dates = DateTimeOffset.Parse(datestarted, System.Globalization.CultureInfo.GetCultureInfo("en-NZ"));

                                                    tst = TimeZoneInfo.FindSystemTimeZoneById("New Zealand Standard Time");
                                                    creatat = TimeZoneInfo.ConvertTime(createdat, tst);
                                                    isDaylight = tst.IsDaylightSavingTime(createdat);
                                                    if (isDaylight == true)
                                                    {
                                                        //dst = TimeZoneInfo.FindSystemTimeZoneById("New Zealand Daylight Time");
                                                        creatat = TimeZoneInfo.ConvertTime(createdat, tst);
                                                        modat = TimeZoneInfo.ConvertTime(modifiedat, tst);
                                                        dateco = TimeZoneInfo.ConvertTime(datec, tst);
                                                        datemo = TimeZoneInfo.ConvertTime(datem, tst);
                                                        datest = TimeZoneInfo.ConvertTime(dates, tst);
                                                        //update audit table
                                                        string auditupdate = "UPDATE api_Audit SET Created_at = @Created_at, Modified_at = @Modified_at Where Audit_id = @Audit_ID";

                                                        SqlCommand cmd = new SqlCommand(auditupdate, connection);
                                                        cmd.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                        cmd.Parameters.AddWithValue("@Created_at", creatat);
                                                        cmd.Parameters.AddWithValue("@Modified_at", modat);
                                                        cmd.ExecuteNonQuery();

                                                        string auditdataupdate = "UPDATE api_AuditData SET Date_Completed = @Date_Completed, Date_Modified = @Date_Modified, Date_Started = @Date_Started WHERE Audit_id = @Audit_ID";

                                                        SqlCommand cmdauditdata = new SqlCommand(auditdataupdate, connection);
                                                        cmdauditdata.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                        cmdauditdata.Parameters.AddWithValue("@Date_Completed", dateco);
                                                        cmdauditdata.Parameters.AddWithValue("@Date_Modified", datemo);
                                                        cmdauditdata.Parameters.AddWithValue("@Date_Started", datest);
                                                        cmdauditdata.ExecuteNonQuery();
                                                    }
                                                    else
                                                    {
                                                        modat = TimeZoneInfo.ConvertTime(modifiedat, tst);
                                                        dateco = TimeZoneInfo.ConvertTime(datec, tst);
                                                        datemo = TimeZoneInfo.ConvertTime(datem, tst);
                                                        datest = TimeZoneInfo.ConvertTime(dates, tst);
                                                        //update audit table
                                                        string auditupdate = "UPDATE api_Audit SET Created_at = @Created_at, Modified_at = @Modified_at Where Audit_id = @Audit_ID";

                                                        SqlCommand cmd = new SqlCommand(auditupdate, connection);
                                                        cmd.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                        cmd.Parameters.AddWithValue("@Created_at", creatat);
                                                        cmd.Parameters.AddWithValue("@Modified_at", modat);
                                                        cmd.ExecuteNonQuery();

                                                        //update auditdata
                                                        string auditdataupdate = "UPDATE api_AuditData SET Date_Completed = @Date_Completed, Date_Modified = @Date_Modified, Date_Started = @Date_Started WHERE Audit_id = @Audit_ID";
                                                        SqlCommand cmdauditdata = new SqlCommand(auditdataupdate, connection);
                                                        cmdauditdata.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                        cmdauditdata.Parameters.AddWithValue("@Date_Completed", dateco);
                                                        cmdauditdata.Parameters.AddWithValue("@Date_Modified", datemo);
                                                        cmdauditdata.Parameters.AddWithValue("@Date_Started", datest);
                                                        cmdauditdata.ExecuteNonQuery();
                                                    }
                                                }
                                                #endregion
                                            }
                                        }
                                        #endregion
                                    }
                                }
                                #endregion

                                connectIBASE.Close();
                            }
                            else if (header.label == "Job #")
                            {
                                //initialize values
                                var parentid = String.Empty;
                                var itemid = String.Empty;
                                var label = String.Empty;
                                var section = String.Empty;
                                var jobno = String.Empty;
                                //check if null
                                if (header == null || header.parent_id == null || header.parent_id == String.Empty)
                                    parentid = null;
                                else
                                    parentid = header.parent_id;
                                if (header == null || header.item_id == null || header.item_id == String.Empty)
                                    itemid = null;
                                else
                                    itemid = header.item_id;
                                if (header == null || header.label == null || header.label == String.Empty)
                                    label = null;
                                else
                                    label = header.label;
                                if (header == null || header.section == null || header.section == String.Empty)
                                    section = null;
                                else
                                    section = header.section;
                                if (header == null || header.responses == null || header.responses.text == null || header.responses.text == String.Empty)
                                    jobno = null;
                                else
                                    jobno = header.responses.text;

                                //insert into Header Items
                                string headeritems = "INSERT INTO api_Header_Items (Audit_ID, Parent_ID, Item_ID, Label, Section,Text)";
                                headeritems += "VALUES (@Audit_ID, @Parent_ID, @Item_ID, @Label, @Section, @Text)";

                                SqlCommand myHeaderItems = new SqlCommand(headeritems, connection);
                                SqlParameter auditParam = myHeaderItems.Parameters.AddWithValue("@Audit_ID", audit_no);
                                if (audit_no == null) auditParam.Value = DBNull.Value;
                                SqlParameter parentParam = myHeaderItems.Parameters.AddWithValue("@Parent_ID", parentid);
                                if (parentid == null) parentParam.Value = DBNull.Value;
                                SqlParameter itemParam = myHeaderItems.Parameters.AddWithValue("@Item_ID", itemid);
                                if (itemid == null) itemParam.Value = DBNull.Value;
                                SqlParameter labelParam = myHeaderItems.Parameters.AddWithValue("@Label", label);
                                if (label == null) labelParam.Value = DBNull.Value;
                                SqlParameter sectionParam = myHeaderItems.Parameters.AddWithValue("@Section", section);
                                if (section == null) sectionParam.Value = DBNull.Value;
                                SqlParameter textParam = myHeaderItems.Parameters.AddWithValue("@Text", jobno);
                                if (jobno == null) textParam.Value = DBNull.Value;

                                myHeaderItems.ExecuteNonQuery();
                            }
                            else if (header.label == "Prepared by")
                            {
                                //initialize values
                                var parentid = String.Empty;
                                var itemid = String.Empty;
                                var label = String.Empty;
                                var section = String.Empty;
                                var preparedby = String.Empty;
                                //check if null
                                if (header == null || header.parent_id == null || header.parent_id == String.Empty)
                                    parentid = null;
                                else
                                    parentid = header.parent_id;
                                if (header == null || header.item_id == null || header.item_id == String.Empty)
                                    itemid = null;
                                else
                                    itemid = header.item_id;
                                if (header == null || header.label == null || header.label == String.Empty)
                                    label = null;
                                else
                                    label = header.label;
                                if (header == null || header.section == null || header.section == String.Empty)
                                    section = null;
                                else
                                    section = header.section;
                                if (header == null || header.responses == null || header.responses.text == null || header.responses.text == String.Empty)
                                    preparedby = null;
                                else
                                    preparedby = header.responses.text;

                                //insert into Header Items
                                string headeritems = "INSERT INTO api_Header_Items (Audit_ID, Parent_ID, Item_ID, Label, Section,Text)";
                                headeritems += "VALUES (@Audit_ID, @Parent_ID, @Item_ID, @Label, @Section, @Text)";

                                SqlCommand myHeaderItems = new SqlCommand(headeritems, connection);
                                SqlParameter auditParam = myHeaderItems.Parameters.AddWithValue("@Audit_ID", audit_no);
                                if (audit_no == null) auditParam.Value = DBNull.Value;
                                SqlParameter parentParam = myHeaderItems.Parameters.AddWithValue("@Parent_ID", parentid);
                                if (parentid == null) parentParam.Value = DBNull.Value;
                                SqlParameter itemParam = myHeaderItems.Parameters.AddWithValue("@Item_ID", itemid);
                                if (itemid == null) itemParam.Value = DBNull.Value;
                                SqlParameter labelParam = myHeaderItems.Parameters.AddWithValue("@Label", label);
                                if (label == null) labelParam.Value = DBNull.Value;
                                SqlParameter sectionParam = myHeaderItems.Parameters.AddWithValue("@Section", section);
                                if (section == null) sectionParam.Value = DBNull.Value;
                                SqlParameter textParam = myHeaderItems.Parameters.AddWithValue("@Text", preparedby);
                                if (preparedby == null) textParam.Value = DBNull.Value;

                                myHeaderItems.ExecuteNonQuery();
                            }

                            else if (header.label == "Conducted on")
                            {
                                //initialize values
                                var parentid = String.Empty;
                                var itemid = String.Empty;
                                var label = String.Empty;
                                var section = String.Empty;
                                var weight = String.Empty;
                                var conductedon = String.Empty;
                                var enable_date = String.Empty;
                                var enable_time = String.Empty;
                                DateTimeOffset con = new DateTimeOffset();

                                //check the values if null
                                if (header == null || header.parent_id == null || header.parent_id == String.Empty)
                                    parentid = null;
                                else
                                    parentid = header.parent_id;
                                if (header == null || header.item_id == null || header.item_id == String.Empty)
                                    itemid = null;
                                else
                                    itemid = header.item_id;
                                if (header == null || header.label == null || header.label == String.Empty)
                                    label = null;
                                else
                                    label = header.label;
                                if (header == null || header.section == null || header.section == String.Empty)
                                    section = null;
                                else
                                    section = header.section;
                                if (header == null || header.options == null || header.options.weighting == null || header.options.weighting == String.Empty)
                                    weight = null;
                                else
                                    weight = header.options.weighting;
                                if (header == null || header.options == null || header.options.enable_date == null || header.options.enable_date == String.Empty)
                                    enable_date = null;
                                else
                                    enable_date = header.options.enable_date;
                                if (header == null || header.options == null || header.options.enable_time == null || header.options.enable_time == String.Empty)
                                    enable_time = null;
                                else
                                    enable_time = header.options.enable_time;
                                if (header == null || header.responses == null || header.responses.datetime == null || header.responses.datetime == String.Empty)
                                    conductedon = null;
                                else
                                    conductedon = header.responses.datetime;
                                //check if conducted on is null
                                if (conductedon == null)
                                    con = new DateTimeOffset();
                                else
                                {
                                    //convert fomat of dates
                                    DateTimeOffset cdate = DateTimeOffset.Parse(conductedon, System.Globalization.CultureInfo.GetCultureInfo("en-AU"));
                                    DateTimeOffset codate = TimeZoneInfo.ConvertTime(cdate, TimeZoneInfo.Local);
                                    con = codate;
                                }

                                //insert data to database
                                string headeritems = "INSERT INTO api_Header_Items (Audit_ID, Parent_ID, Item_ID, Label, Section, Weighting, Enable_Date, Enable_Time, Datetime)";
                                headeritems += "VALUES (@Audit_ID, @Parent_ID, @Item_ID, @Label, @Section, @Weighting, @Enable_Date, @Enable_Time,  @Datetime)";

                                SqlCommand myHeaderItems = new SqlCommand(headeritems, connection);
                                SqlParameter auditParam = myHeaderItems.Parameters.AddWithValue("@Audit_ID", audit_no);
                                if (audit_no == null) auditParam.Value = DBNull.Value;
                                SqlParameter parentParam = myHeaderItems.Parameters.AddWithValue("@Parent_ID", parentid);
                                if (parentid == null) parentParam.Value = DBNull.Value;
                                SqlParameter itemParam = myHeaderItems.Parameters.AddWithValue("@Item_ID", itemid);
                                if (itemid == null) itemParam.Value = DBNull.Value;
                                SqlParameter labelParam = myHeaderItems.Parameters.AddWithValue("@Label", label);
                                if (label == null) labelParam.Value = DBNull.Value;
                                SqlParameter sectionParam = myHeaderItems.Parameters.AddWithValue("@Section", section);
                                if (section == null) sectionParam.Value = DBNull.Value;
                                SqlParameter weightParam = myHeaderItems.Parameters.AddWithValue("@Weighting", weight);
                                if (weight == null) weightParam.Value = DBNull.Value;
                                SqlParameter dateParam = myHeaderItems.Parameters.AddWithValue("@Enable_Date", enable_date);
                                if (enable_date == null) dateParam.Value = DBNull.Value;
                                SqlParameter timeParam = myHeaderItems.Parameters.AddWithValue("@Enable_Time", enable_time);
                                if (enable_time == null) timeParam.Value = DBNull.Value;
                                SqlParameter textParam = myHeaderItems.Parameters.AddWithValue("@DateTime", con);
                                if (con == null) textParam.Value = DBNull.Value;
                                myHeaderItems.ExecuteNonQuery();
                            }
                        }
                        #endregion //header_items
                        #region //items                        
                        int line_nos = 0;
                        foreach (var items in auditresult.items)
                        {
                            #region initialize values
                            //initialize values
                            
                            var itemid = String.Empty;
                            var label = String.Empty;
                            var type_items = String.Empty;
                            var combined_score = 0.00;
                            var combined_max_score = 0.00;
                            var combined_score_percentage = 0.00;
                            var parentid = items.parent_id;
                            var weighting = String.Empty;
                            List<string> values = new List<string>();
                            var condition = String.Empty;
                            var type_options = String.Empty;
                            var visible_in_audit = false;
                            var visible_in_report = false;
                            var response_set = String.Empty;
                            var select = String.Empty;
                            var evaluation = false;
                            var inactive = false;

                            //check if null
                            if (items == null || items.item_id == null)
                                itemid = null;
                            else
                                itemid = items.item_id;
                            if (items == null || items.label == null)
                                label = null;
                            else
                                label = items.label;
                            if (items == null || items.type == null)
                                type_items = null;
                            else
                                type_items = items.type;
                            if (items == null || items.scoring == null || items.scoring.combined_score == 0.00)
                                combined_score = 0.00;
                            else
                                combined_score = items.scoring.combined_score;
                            if (items == null || items.scoring == null || items.scoring.combined_max_score == 0.00)
                                combined_max_score = 0.00;
                            else
                                combined_max_score = items.scoring.combined_max_score;
                            if (items == null || items.scoring == null || items.scoring.combined_score_percentage == 0.00)
                                combined_score_percentage = 0.00;
                            else
                                combined_score_percentage = items.scoring.combined_score_percentage;
                            if (items == null || items.options == null || items.options.weighting == null)
                                weighting = null;
                            else
                                weighting = items.options.weighting;
                            if (items == null || items.options == null || items.options.values == null)
                                values = null;
                            else
                                values = items.options.values;
                            if (items == null || items.options == null || items.options.condition == null || items.options.condition == String.Empty)
                                condition = null;
                            else
                                condition = items.options.condition;
                            if (items == null || items.options == null || items.options.type == null || items.options.type == String.Empty)
                                type_options = null;
                            else
                                type_options = items.options.type;
                            if (items == null || items.options == null || items.options.visible_in_audit == false)
                                visible_in_audit = false;
                            else
                                visible_in_audit = items.options.visible_in_audit;
                            if (items == null || items.options == null || items.options.visible_in_report == false)
                                visible_in_report = false;
                            else
                                visible_in_report = items.options.visible_in_report;
                            if (items == null || items.options == null || items.options.response_set == null || items.options.response_set == String.Empty)
                                response_set = null;
                            else
                                response_set = items.options.response_set;
                            if (items == null || items.evaluation == false)
                                evaluation = false;
                            else
                                evaluation = items.evaluation;
                            if (items == null || items.inactive == false)
                                inactive = false;
                            else
                                inactive = items.inactive;
                            #endregion
                            #region insert into Items Data
                            //insert data to Items_Data      
                            line_nos++;                            
                            
                            string itemsdata = "INSERT INTO Items (Audit_ID, Item_ID, Parent_ID, Label, Type, Evaluation, Inactive, Weighting, Response_Set, Options_Values, Condition, Options_Type, Visible_in_Audit, Visible_in_Report,Combined_Score, Combined_Max_Score, Combined_Score_Percentage, Line_No )";
                            itemsdata += "VALUES (@Audit_ID, @Item_ID, @Parent_ID, @Label, @Type, @Evaluation, @Inactive, @Weighting, @Response_Set, @Option_Values, @Condition, @Options_Type, @Visible_in_Audit, @Visible_in_Report, @Combined_Score, @Combined_Max_Score, @Combined_Score_Percentage, @Line_Num)";
                            #region passing of parameters
                            SqlCommand myItemData = new SqlCommand(itemsdata, connection);
                            SqlParameter auditno1Param = myItemData.Parameters.AddWithValue("@Audit_ID", audit_id);
                            if (audit_id == null) auditno1Param.Value = DBNull.Value;
                            SqlParameter itemParam = myItemData.Parameters.AddWithValue("@Item_ID", itemid);
                            if (itemid == null) itemParam.Value = DBNull.Value;
                            SqlParameter parentParam = myItemData.Parameters.AddWithValue("@Parent_ID", parentid);
                            if (parentid == null) parentParam.Value = DBNull.Value;
                            SqlParameter LabelParam = myItemData.Parameters.AddWithValue("@Label", label);
                            if (label == null) LabelParam.Value = DBNull.Value;
                            SqlParameter typeParam = myItemData.Parameters.AddWithValue("@Type", type_items);
                            if (type_items == null) typeParam.Value = DBNull.Value;
                            SqlParameter evalParam = myItemData.Parameters.AddWithValue("@Evaluation", evaluation);
                            if (evaluation == false) evalParam.Value = DBNull.Value;
                            SqlParameter inactiveParam = myItemData.Parameters.AddWithValue("@Inactive", inactive);
                            if (inactive == false) inactiveParam.Value = DBNull.Value;
                            SqlParameter weightParam = myItemData.Parameters.AddWithValue("@Weighting", weighting);
                            if (weighting == null) weightParam.Value = DBNull.Value;
                            SqlParameter setParam = myItemData.Parameters.AddWithValue("@Response_Set", response_set);
                            if (response_set == null) setParam.Value = DBNull.Value;
                            SqlParameter valuesParam = myItemData.Parameters.Add("@Option_Values", SqlDbType.VarChar);
                            if (values == null || values.Count == 0)
                                myItemData.Parameters["@Option_Values"].Value = values;
                            else
                            {
                                foreach (var val in values)
                                {
                                    myItemData.Parameters["@Option_Values"].Value = val;
                                }
                            }
                            if (values == null || values.Count == 0) valuesParam.Value = DBNull.Value;
                            SqlParameter conditionParam = myItemData.Parameters.AddWithValue("@Condition", condition);
                            if (condition == null) conditionParam.Value = DBNull.Value;
                            SqlParameter otypeParam = myItemData.Parameters.AddWithValue("@Options_Type", type_options);
                            if (type_options == null) otypeParam.Value = DBNull.Value;
                            SqlParameter visibleaParam = myItemData.Parameters.AddWithValue("@Visible_in_Audit", visible_in_audit);
                            if (visible_in_audit == false) visibleaParam.Value = DBNull.Value;
                            SqlParameter visiblerParam = myItemData.Parameters.AddWithValue("@Visible_in_Report", visible_in_report);
                            if (visible_in_report == false) visiblerParam.Value = DBNull.Value;
                            SqlParameter cscoreParam = myItemData.Parameters.AddWithValue("@Combined_Score", combined_score);
                            if (combined_score == 0.00) cscoreParam.Value = DBNull.Value;
                            SqlParameter cmscoreParam = myItemData.Parameters.AddWithValue("@Combined_Max_Score", combined_max_score);
                            if (combined_max_score == 0.00) cmscoreParam.Value = DBNull.Value;
                            SqlParameter cscorepParam = myItemData.Parameters.AddWithValue("@Combined_Score_Percentage", combined_score_percentage);
                            if (combined_score_percentage == 0.00) cscorepParam.Value = DBNull.Value;
                            SqlParameter line_nom = myItemData.Parameters.AddWithValue("@Line_Num", line_nos);
                            if (line_nos == 0) line_nom.Value = DBNull.Value;
                            #endregion
                            myItemData.ExecuteNonQuery();
                            #endregion
                            #region update Items data
                            if (items == null || items.responses == null)
                                select = null;
                            else
                            {
                                if (items.responses.selected == null || items.responses.selected.Count == 0)
                                    continue;
                                else
                                {
                                    if (items.responses.selected[0] is string)
                                    {
                                        var stringValue = (string)items.responses.selected[0];
                                        if (stringValue == null)
                                            continue;
                                        else
                                        {
                                            var response_id = String.Empty;
                                            response_id = stringValue;

                                            String strResult = String.Empty;
                                            string data = String.Empty;
                                            string itemdata = String.Empty;
                                            SqlCommand itemData = new SqlCommand();
                                            SqlCommand Data = new SqlCommand();
                                            SqlParameter auditrParam = new SqlParameter();
                                            SqlParameter parentrParam = new SqlParameter();

                                            //check data if already inserted to Items
                                            data = "SELECT Audit_ID, Parent_ID FROM Items WHERE (Audit_ID = @AuditID and Parent_ID = @ParentID)";
                                            Data = new SqlCommand(data, connection);
                                            auditrParam = Data.Parameters.AddWithValue("@AuditID", audit_id);
                                            if (audit_id == null) auditrParam.Value = DBNull.Value;
                                            parentrParam = Data.Parameters.AddWithValue("@ParentID", parentid);
                                            if (parentid == null) parentrParam.Value = DBNull.Value;
                                            Data.ExecuteNonQuery();
                                            strResult = (String)Data.ExecuteScalar();
                                            if (strResult == null)
                                            {
                                                itemdata = "UPDATE Items SET ID = @ID WHERE (Audit_ID = @Audit_ID and Parent_ID = @ParentID)";
                                                itemData = new SqlCommand(itemdata, connection);

                                                auditrParam = itemData.Parameters.AddWithValue("@Audit_ID", audit_id);
                                                if (audit_id == null) auditrParam.Value = DBNull.Value;
                                                parentrParam = itemData.Parameters.AddWithValue("@Item_ID", itemid);
                                                if (itemid == null) parentrParam.Value = DBNull.Value;
                                                SqlParameter idParam = itemData.Parameters.AddWithValue("@ID", response_id);
                                                if (response_id == null) idParam.Value = DBNull.Value;
                                                itemData.ExecuteNonQuery();
                                            }
                                        }
                                    }
                                    else
                                    {
                                        var sele = (items.responses.selected[0].GetType().GetGenericTypeDefinition().IsAssignableFrom(typeof(Dictionary<string, object>)));

                                        var dictionaryValue = items.responses.selected[0] as Dictionary<string, object>;

                                        if (dictionaryValue == null)
                                            return;
                                        else
                                        {
                                            foreach (var sel in dictionaryValue)
                                            {
                                                String strResult = String.Empty;
                                                string data = String.Empty;
                                                string itemdata = String.Empty;
                                                SqlCommand itemData = new SqlCommand();
                                                SqlCommand Data = new SqlCommand();
                                                SqlParameter auditrParam = new SqlParameter();
                                                SqlParameter itemrParam = new SqlParameter();

                                                //initialize values
                                                object response_id = String.Empty;
                                                object colour = String.Empty;
                                                object label_response = String.Empty;
                                                object label_score = 0.00;
                                                object short_label = String.Empty;
                                                object response_type = String.Empty;
                                                object enable_score = false;
                                                object image = String.Empty;

                                                //get the values
                                                if (sel.Key == ("id"))
                                                {
                                                    response_id = sel.Value;

                                                    //Check data if already exists in Items
                                                    data = "SELECT Audit_ID, Item_ID FROM Items WHERE (Audit_ID = @AuditID and Item_ID = @ItemID)";
                                                    Data = new SqlCommand(data, connection);
                                                    auditrParam = Data.Parameters.AddWithValue("@AuditID", audit_id);
                                                    if (audit_id == null) auditrParam.Value = DBNull.Value;

                                                    itemrParam = Data.Parameters.AddWithValue("@ItemID", itemid);
                                                    if (itemid == null) itemrParam.Value = DBNull.Value;

                                                    Data.ExecuteNonQuery();
                                                    strResult = (String)Data.ExecuteScalar();

                                                    if (strResult != null)
                                                    {
                                                        itemdata = "UPDATE Items SET ID = @ID WHERE (Audit_ID = @Audit_ID and Item_ID = @Item_ID)";
                                                        itemData = new SqlCommand(itemdata, connection);

                                                        auditrParam = itemData.Parameters.AddWithValue("@Audit_ID", audit_id);
                                                        if (audit_id == null) auditrParam.Value = DBNull.Value;

                                                        itemrParam = itemData.Parameters.AddWithValue("@Item_ID", itemid);
                                                        if (itemid == null) itemrParam.Value = DBNull.Value;

                                                        SqlParameter idParam = itemData.Parameters.AddWithValue("@ID", response_id);
                                                        if (response_id == null) idParam.Value = DBNull.Value;

                                                        itemData.ExecuteNonQuery();
                                                    }
                                                }
                                                else if (sel.Key == ("colour"))
                                                {
                                                    colour = sel.Value;

                                                    //update data to Items_Responses
                                                    data = "SELECT Audit_ID, Item_ID FROM Items WHERE (Audit_ID = @AuditID and Item_ID = @ItemID)";
                                                    Data = new SqlCommand(data, connection);
                                                    auditrParam = Data.Parameters.AddWithValue("@AuditID", audit_id);
                                                    if (audit_id == null) auditrParam.Value = DBNull.Value;

                                                    itemrParam = Data.Parameters.AddWithValue("@ItemID", itemid);
                                                    if (itemid == null) itemrParam.Value = DBNull.Value;

                                                    Data.ExecuteNonQuery();
                                                    strResult = (String)Data.ExecuteScalar();

                                                    if (strResult != null)
                                                    {
                                                        itemdata = "UPDATE Items SET Colour = @Colour WHERE (Audit_ID = @Audit_ID and Item_ID = @Item_ID)";
                                                        itemData = new SqlCommand(itemdata, connection);
                                                        auditrParam = itemData.Parameters.AddWithValue("@Audit_ID", audit_id);
                                                        if (audit_id == null) auditrParam.Value = DBNull.Value;

                                                        itemrParam = itemData.Parameters.AddWithValue("@Item_ID", itemid);
                                                        if (itemid == null) itemrParam.Value = DBNull.Value;

                                                        SqlParameter colourParam = itemData.Parameters.AddWithValue("@Colour", colour);
                                                        if (colour == null) colourParam.Value = DBNull.Value;

                                                        itemData.ExecuteNonQuery();
                                                    }
                                                }
                                                else if (sel.Key == ("label"))
                                                {
                                                    label_response = sel.Value;

                                                    //update data to Items_Responses
                                                    data = "SELECT Audit_ID, Item_ID FROM Items WHERE (Audit_ID = @AuditID and Item_ID = @ItemID)";
                                                    Data = new SqlCommand(data, connection);
                                                    auditrParam = Data.Parameters.AddWithValue("@AuditID", audit_id);
                                                    if (audit_id == null) auditrParam.Value = DBNull.Value;

                                                    itemrParam = Data.Parameters.AddWithValue("@ItemID", itemid);
                                                    if (itemid == null) itemrParam.Value = DBNull.Value;

                                                    Data.ExecuteNonQuery();
                                                    strResult = (String)Data.ExecuteScalar();

                                                    if (strResult != null)
                                                    {
                                                        itemdata = "UPDATE Items SET Response_Label = @Label WHERE (Audit_ID = @Audit_ID and Item_ID = @Item_ID)";
                                                        itemData = new SqlCommand(itemdata, connection);
                                                        auditrParam = itemData.Parameters.AddWithValue("@Audit_ID", audit_id);
                                                        if (audit_id == null) auditrParam.Value = DBNull.Value;

                                                        itemrParam = itemData.Parameters.AddWithValue("@Item_ID", itemid);
                                                        if (itemid == null) itemrParam.Value = DBNull.Value;

                                                        SqlParameter labelParam = itemData.Parameters.AddWithValue("@Label", label_response);
                                                        if (label_response == null) labelParam.Value = DBNull.Value;

                                                        itemData.ExecuteNonQuery();
                                                    }
                                                }
                                                else if (sel.Key == "score")
                                                {
                                                    label_score = sel.Value;

                                                    //update data to Items_Responses
                                                    data = "SELECT Audit_ID, Item_ID FROM Items WHERE (Audit_ID = @AuditID and Item_ID = @ItemID)";
                                                    Data = new SqlCommand(data, connection);
                                                    auditrParam = Data.Parameters.AddWithValue("@AuditID", audit_id);
                                                    if (audit_id == null) auditrParam.Value = DBNull.Value;

                                                    itemrParam = Data.Parameters.AddWithValue("@ItemID", itemid);
                                                    if (itemid == null) itemrParam.Value = DBNull.Value;

                                                    Data.ExecuteNonQuery();
                                                    strResult = (String)Data.ExecuteScalar();

                                                    if (strResult != null)
                                                    {
                                                        itemdata = "UPDATE Items SET Score = @Score WHERE (Audit_ID = @Audit_ID and Item_ID = @Item_ID)";
                                                        itemData = new SqlCommand(itemdata, connection);
                                                        auditrParam = itemData.Parameters.AddWithValue("@Audit_ID", audit_id);
                                                        if (audit_id == null) auditrParam.Value = DBNull.Value;

                                                        itemrParam = itemData.Parameters.AddWithValue("@Item_ID", itemid);
                                                        if (itemid == null) itemrParam.Value = DBNull.Value;

                                                        SqlParameter score1Param = itemData.Parameters.AddWithValue("@Score", label_score);
                                                        if (label_score == null) score1Param.Value = DBNull.Value;

                                                        itemData.ExecuteNonQuery();
                                                    }
                                                }
                                                else if (sel.Key == ("short_label"))
                                                {
                                                    short_label = sel.Value;

                                                    //update data to Items_Responses
                                                    data = "SELECT Audit_ID, Item_ID FROM Items WHERE (Audit_ID = @AuditID and Item_ID = @ItemID)";
                                                    Data = new SqlCommand(data, connection);
                                                    auditrParam = Data.Parameters.AddWithValue("@AuditID", audit_id);
                                                    if (audit_id == null) auditrParam.Value = DBNull.Value;

                                                    itemrParam = Data.Parameters.AddWithValue("@ItemID", itemid);
                                                    if (itemid == null) itemrParam.Value = DBNull.Value;

                                                    Data.ExecuteNonQuery();
                                                    strResult = (String)Data.ExecuteScalar();

                                                    if (strResult != null)
                                                    {
                                                        itemdata = "UPDATE Items SET Short_Label = @Short_Label WHERE (Audit_ID = @Audit_ID and Item_ID = @Item_ID)";
                                                        itemData = new SqlCommand(itemdata, connection);
                                                        auditrParam = itemData.Parameters.AddWithValue("@Audit_ID", audit_id);
                                                        if (audit_id == null) auditrParam.Value = DBNull.Value;

                                                        itemrParam = itemData.Parameters.AddWithValue("@Item_ID", itemid);
                                                        if (itemid == null) itemrParam.Value = DBNull.Value;

                                                        SqlParameter shortParam = itemData.Parameters.AddWithValue("@Short_Label", short_label);
                                                        if (short_label == null) shortParam.Value = DBNull.Value;

                                                        itemData.ExecuteNonQuery();
                                                    }
                                                }
                                                else if (sel.Key == ("type"))
                                                {
                                                    response_type = sel.Value;

                                                    //update data to Items_Responses
                                                    data = "SELECT Audit_ID, Item_ID FROM Items WHERE (Audit_ID = @AuditID and Item_ID = @ItemID)";
                                                    Data = new SqlCommand(data, connection);
                                                    auditrParam = Data.Parameters.AddWithValue("@AuditID", audit_id);
                                                    if (audit_id == null) auditrParam.Value = DBNull.Value;

                                                    itemrParam = Data.Parameters.AddWithValue("@ItemID", itemid);
                                                    if (itemid == null) itemrParam.Value = DBNull.Value;

                                                    Data.ExecuteNonQuery();
                                                    strResult = (String)Data.ExecuteScalar();

                                                    if (strResult != null)
                                                    {

                                                        itemdata = "UPDATE Items SET Responses_Type = @Response_Type WHERE (Audit_ID = @Audit_ID and Item_ID = @Item_ID)";
                                                        itemData = new SqlCommand(itemdata, connection);
                                                        auditrParam = itemData.Parameters.AddWithValue("@Audit_ID", audit_id);
                                                        if (audit_id == null) auditrParam.Value = DBNull.Value;

                                                        itemrParam = itemData.Parameters.AddWithValue("@Item_ID", itemid);
                                                        if (itemid == null) itemrParam.Value = DBNull.Value;

                                                        SqlParameter rtypeParam = itemData.Parameters.AddWithValue("@Response_Type", response_type);
                                                        if (response_type == null) rtypeParam.Value = DBNull.Value;

                                                        itemData.ExecuteNonQuery();
                                                    }
                                                }

                                                else if (sel.Key == ("enable_score"))
                                                {
                                                    enable_score = sel.Value;

                                                    //update data to Items_Responses
                                                    data = "SELECT Audit_ID, Item_ID FROM Items WHERE (Audit_ID = @AuditID and Item_ID = @ItemID)";
                                                    Data = new SqlCommand(data, connection);
                                                    auditrParam = Data.Parameters.AddWithValue("@AuditID", audit_id);
                                                    if (audit_id == null) auditrParam.Value = DBNull.Value;

                                                    itemrParam = Data.Parameters.AddWithValue("@ItemID", itemid);
                                                    if (itemid == null) itemrParam.Value = DBNull.Value;

                                                    Data.ExecuteNonQuery();
                                                    strResult = (String)Data.ExecuteScalar();

                                                    if (strResult != null)
                                                    {
                                                        itemdata = "UPDATE Items SET Enable_Score = @Enable_Score WHERE (Audit_ID = @Audit_ID and Item_ID = @Item_ID)";
                                                        itemData = new SqlCommand(itemdata, connection);
                                                        auditrParam = itemData.Parameters.AddWithValue("@Audit_ID", audit_id);
                                                        if (audit_id == null) auditrParam.Value = DBNull.Value;

                                                        itemrParam = itemData.Parameters.AddWithValue("@Item_ID", itemid);
                                                        if (itemid == null) itemrParam.Value = DBNull.Value;

                                                        SqlParameter escoreParam = itemData.Parameters.AddWithValue("@Enable_Score", enable_score);
                                                        if (enable_score == null) escoreParam.Value = DBNull.Value;

                                                        itemData.ExecuteNonQuery();
                                                    }
                                                }
                                                else if (sel.Key == ("image"))
                                                {
                                                    image = sel.Value;

                                                    //update data to Items_Responses
                                                    data = "SELECT Audit_ID, Item_ID FROM Items WHERE (Audit_ID = @AuditID and Item_ID = @ItemID)";
                                                    Data = new SqlCommand(data, connection);
                                                    auditrParam = Data.Parameters.AddWithValue("@AuditID", audit_id);
                                                    if (audit_id == null) auditrParam.Value = DBNull.Value;

                                                    itemrParam = Data.Parameters.AddWithValue("@ItemID", itemid);
                                                    if (itemid == null) itemrParam.Value = DBNull.Value;

                                                    Data.ExecuteNonQuery();
                                                    strResult = (String)Data.ExecuteScalar();

                                                    if (strResult != null)
                                                    {
                                                        itemdata = "UPDATE Items SET Image = @Image WHERE (Audit_ID = @Audit_ID and Item_ID = @Item_ID)";
                                                        itemData = new SqlCommand(itemdata, connection);
                                                        auditrParam = itemData.Parameters.AddWithValue("@Audit_ID", audit_id);
                                                        if (audit_id == null) auditrParam.Value = DBNull.Value;

                                                        itemrParam = itemData.Parameters.AddWithValue("@Item_ID", itemid);
                                                        if (itemid == null) itemrParam.Value = DBNull.Value;

                                                        SqlParameter imagerParam = itemData.Parameters.AddWithValue("@Image", image);
                                                        if (image == null) imagerParam.Value = DBNull.Value;

                                                        itemData.ExecuteNonQuery();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion
                        }
                        #endregion //items
                        dataGridView1.DataSource = new List<JsonResult> { auditresult };
                        connection.Close();
                    }
                }
                #endregion
                else
                #region //all
                {
                    #region parameters
                    //no parameter just date
                    //string url = @"https://api.safetyculture.io/audits/search?field=audit_id&field=modified_at&field=template_id&completed=true&modified_after=" + datefrom + "&modified_before=" + dateto;

                    ////Take 5
                    string url = @"https://api.safetyculture.io/audits/search?field=audit_id&field=modified_at&field=template_id&completed=true&order=desc&template=template_c4def6f80b1711e4acf6001b1118ce11&template=template_1874139c447111e495fb001b1118ce11&modified_after=" + datefrom + "&modified_before=" + dateto;
                    // string url = @"https://api.safetyculture.io/audits/search?field=audit_id&field=modified_at&field=template_id&completed=true&template=template_1ec3b72ff30c478ebcede5556a89b567&modified_after=" + datefrom + "&modified_before=" + dateto;
                    
                    //just take the last 150 and ordered by desc
                    //string url = @"https://api.safetyculture.io/audits/search?field=audit_id&field=modified_at&field=template_id&completed=true&order=desc&template=template_c4def6f80b1711e4acf6001b1118ce11&template=template_1874139c447111e495fb001b1118ce11&limit=150";

                    var credentials = new NetworkCredential("sanjay.sharma@siemens-healthineers.com", "SANEDrf9");
                    //var credentials = new NetworkCredential("mark.mcnamara@siemens.com", "1Auditor");
                    //var credentials = new NetworkCredential("kathrine.pagsisihan@siemens.com", "YumiZack68");
                    var webApi = new RestApi(credentials);
                    var result = webApi.Get<DateResult>(url);
                    #endregion
                    foreach (var audit in result.audits)
                    {
                        var auditno = audit.audit_id;

                        string urls = @"https://api.safetyculture.io/audits/" + audit.audit_id;

                        var credential = new NetworkCredential("kathrine.pagsisihan@siemens.com", "YumiZack68");
                        var getAudit = new RestApi(credential);
                        var auditresult = getAudit.Gets<JsonResult>(urls);

                        #region //declaration of variables
                        //declaration of variables 
                        var audit_id = auditresult.audit_id;
                        var template_id = auditresult.template_id;
                        var doc_type = String.Empty;
                        #region doc types
                        if (template_id == "template_c4def6f80b1711e4acf6001b1118ce11" || template_id == "template_1874139c447111e495fb001b1118ce11" || template_id == "template_4c2d3d81edf0470e9c5ee6df0acc3267")
                            doc_type = "Take 5";
                        else if (template_id == "template_1320A5F3871D4054AC50A1F1EA55956B")
                            doc_type = "Follow-up Visit Report";
                        else if (template_id == "template_E1FE842EA792412DAD6264B20AA2E048" || template_id == "template_2CE608F21AF2465AA263917CDC25AF41" || template_id == "template_E1FE842EA792412DAD6264B20AA2E048" || template_id == "template_A26A4F72F439498A8E191FE3520C5E56")
                            doc_type = "Other Visit Report";
                        else if (template_id == "template_83CB10205F9A40A0BAE340791E16686C")
                            doc_type = "Image Quality Visit Report";
                        else if (template_id == "template_BBD428F41D4F4F109A0CB3C982A697B2" || template_id == "template_2DC83E30C22A47E89E03CD03D3149879" || template_id == "template_3DD5AD077F8A4DAA8993F379E170CB9B" || template_id == "template_121c8a3537d711e49e40001b1118ce11")
                            doc_type = "Handover Visit Report";
                        else if (template_id == "template_f738f1ee379311e49278001b1118ce11")
                            doc_type = "Parts Pick Up Request";
                        else if (template_id == "template_D913EC39AEFB44CE9C95CBF8F69784E7")
                            doc_type = "Service Report";
                        else if (template_id == "template_9bd9ac0c0e1a11e487c0001b1118ce11")
                            doc_type = "Declaration of Consent";
                        else if (template_id == "template_13c0a0002e4511e49a85001b1118ce11")
                            doc_type = "MR Safety Checklist";
                        else if (template_id == "template_63bbac00394711e493f4001b1118ce11" || template_id == "template_7DFC9ACF50994E44AA85839D2D568D9B")
                            doc_type = "Field Call Clarification";
                        else if (template_id == "template_885b7fb0580811e4a009001b1118ce11")
                            doc_type = "Attendance Record";
                        else if (template_id == "template_e4ec0cbd43af11e4ab23001b1118ce11")
                            doc_type = "Wonderware Logger";
                        else if (template_id == "template_62c6f2b440034da8a845a14f7267990f")
                            doc_type = "Travel Request";
                        else if (template_id == "template_9144007C618A41D89FBCB693C15CA274")
                            doc_type = "Training Checklist";
                        else
                            doc_type = "PMS Checklist";
                        #endregion
                        //if (template_id == "template_c4def6f80b1711e4acf6001b1118ce11" || template_id == "template_1874139c447111e495fb001b1118ce11")
                        //{
                        //    continue;
                        //}

                        var created_at = auditresult.created_at;
                        var modified_at = auditresult.modified_at;
                        var score = auditresult.audit_data.score;
                        var totalscore = auditresult.audit_data.total_score;
                        var scorepercentage = auditresult.audit_data.score_percentage;
                        var name = auditresult.audit_data.name;
                        var duration = auditresult.audit_data.duration;
                        var datecompleted = auditresult.audit_data.date_completed;
                        var datemodified = auditresult.audit_data.date_modified;
                        var datestarted = auditresult.audit_data.date_started;
                        var deviceid = auditresult.audit_data.authorship.device_id;
                        var owner = auditresult.audit_data.authorship.owner;
                        var author = auditresult.audit_data.authorship.author;
                        var metname = auditresult.template_data.metadata.name;
                        var metdesc = auditresult.template_data.metadata.description;
                        var metimage = String.Empty;
                        
                        if (auditresult == null || auditresult.template_data == null || auditresult.template_data.metadata == null || auditresult.template_data.metadata.image == null || auditresult.template_data.metadata.image.media_id == null || auditresult.template_data.metadata.image.media_id == String.Empty)
                            metimage = null;
                        else
                            metimage = auditresult.template_data.metadata.image.media_id;

                        var responseid = auditresult.template_data.response_sets;
                                               
                        ////change the format of dates
                        TimeSpan serverOffset = TimeZoneInfo.Local.GetUtcOffset(DateTimeOffset.Now);
                        DateTimeOffset createdat = DateTimeOffset.Parse(created_at, System.Globalization.CultureInfo.GetCultureInfo("en-AU"));
                        DateTimeOffset creatat = TimeZoneInfo.ConvertTime(createdat, TimeZoneInfo.Local);

                        DateTimeOffset modifiedat = DateTimeOffset.Parse(modified_at, System.Globalization.CultureInfo.GetCultureInfo("en-AU"));
                        DateTimeOffset modat = TimeZoneInfo.ConvertTime(modifiedat, TimeZoneInfo.Local);

                        DateTimeOffset datec = DateTimeOffset.Parse(datecompleted, System.Globalization.CultureInfo.GetCultureInfo("en-AU"));
                        DateTimeOffset dateco = TimeZoneInfo.ConvertTime(datec, TimeZoneInfo.Local);

                        DateTimeOffset datem = DateTimeOffset.Parse(datemodified, System.Globalization.CultureInfo.GetCultureInfo("en-AU"));
                        DateTimeOffset datemo = TimeZoneInfo.ConvertTime(datem, TimeZoneInfo.Local);

                        DateTimeOffset dates = DateTimeOffset.Parse(datestarted, System.Globalization.CultureInfo.GetCultureInfo("en-AU"));
                        DateTimeOffset datest = TimeZoneInfo.ConvertTime(dates, TimeZoneInfo.Local);
                        #endregion //declaration of variables
                                               
                        //using (SqlConnection connection = new SqlConnection(@"Server=mssql-snx.csaa-prod.hccloud.de,14330; Database=iauditor; User Id=admin; password=?hXfSXYco4J;max pool size=1"))
                        using (SqlConnection connection = new SqlConnection(@"Server=pd1g9d7qth63qbk.c2ikttwxhgln.ap-northeast-1.rds.amazonaws.com;Database=iAuditor; User Id=admin; password=?hXfSXYco4J;max pool size=1;MultipleActiveResultSets=True;"))
                        {
                            connection.Open();

                            //to check if the audit id is already existing in DB                          
                            var dataAudit = "SELECT Audit_ID FROM api_Audit WHERE (Audit_ID = @AuditID)";
                            var Dataaudit = new SqlCommand(dataAudit, connection);
                            var checkparam = Dataaudit.Parameters.AddWithValue("@AuditID", audit_id);
                            if (audit_id == null) checkparam.Value = DBNull.Value;

                            Dataaudit.ExecuteNonQuery();
                            var sResult = (String)Dataaudit.ExecuteScalar();

                            if (sResult != null)
                            {
                                var dataHeader = "SELECT Audit_ID, Label, Text FROM api_header_items WHERE (Audit_ID = @AuditID and Label = 'System ID')";
                                var DataHeader = new SqlCommand(dataHeader, connection);
                                var checkheader = DataHeader.Parameters.AddWithValue("@AuditID", audit_id);
                                if (audit_id == null) checkheader.Value = DBNull.Value;

                                DataHeader.ExecuteNonQuery();
                                var sResultHeader = (String)DataHeader.ExecuteScalar();

                                if (sResultHeader != null)
                                {
                                    foreach (var header in auditresult.header_items)
                                    {
                                        var systemid = String.Empty;
                                        if (header.label == "System ID")
                                        {
                                            if (header == null || header.responses == null || header.responses.text == null || header.responses.text == String.Empty)
                                                systemid = null;
                                            else
                                                systemid = header.responses.text;

                                            //update header systemID
                                            string headeritems = "UPDATE api_Header_Items_OTHER SET Text = @Text WHERE Audit_ID = @AuditID and label = 'System ID'";
                                            SqlCommand myHeaderItems = new SqlCommand(headeritems, connection);
                                            SqlParameter auditParam = myHeaderItems.Parameters.AddWithValue("@AuditID", audit_id);
                                            if (audit_id == null) auditParam.Value = DBNull.Value;
                                            SqlParameter textParam = myHeaderItems.Parameters.AddWithValue("@Text", systemid);
                                            if (systemid == null) textParam.Value = DBNull.Value;
                                            myHeaderItems.ExecuteNonQuery();
                                        }
                                    }
                                }
                                continue;
                            }

                            #region //audit
                            //insert into Audit Table
                            string audittable = "INSERT INTO api_Audit (Audit_id, Template_id, Created_at, Modified_at, Doc_Type)";
                            audittable += " VALUES (@Audit_id, @Template_id, @Created_at, @Modified_at, @Doc_Type)";

                            SqlCommand myCommand = new SqlCommand(audittable, connection);

                            myCommand.Parameters.AddWithValue("@Audit_id", auditno);
                            myCommand.Parameters.AddWithValue("@Template_id", template_id);
                            myCommand.Parameters.AddWithValue("@Created_at", creatat);
                            myCommand.Parameters.AddWithValue("@Modified_at", modat);
                            myCommand.Parameters.AddWithValue("@Doc_Type", doc_type);
                            myCommand.ExecuteNonQuery();
                            #endregion //audit
                            #region //auditdata
                            //insert into Audit Data Table
                            string auditdata = "INSERT INTO api_AuditData (Audit_ID, Score, Total_Score, Score_Percentage, Name, Duration, Device_ID, Owner, Author, Date_Completed, Date_Modified, Date_Started)";
                            auditdata += "VALUES (@Audit_ID, @Score, @Total_Score, @Score_Percentage, @Name, @Duration, @Device_ID, @Owner, @Author, @Date_Completed, @Date_Modified, @Date_Started)";

                            SqlCommand myAuditData = new SqlCommand(auditdata, connection);
                            SqlParameter auditidParam = myAuditData.Parameters.AddWithValue("@Audit_ID", auditno);
                            if (auditno == null) auditidParam.Value = DBNull.Value;
                            SqlParameter scoreParam = myAuditData.Parameters.AddWithValue("@Score", score);
                            if (score == 0.00) scoreParam.Value = DBNull.Value;
                            SqlParameter totalParam = myAuditData.Parameters.AddWithValue("@Total_Score", totalscore);
                            if (totalscore == 0.00) totalParam.Value = DBNull.Value;
                            SqlParameter percentParam = myAuditData.Parameters.AddWithValue("@Score_Percentage", scorepercentage);
                            if (scorepercentage == 0.00) percentParam.Value = DBNull.Value;
                            SqlParameter nameParam = myAuditData.Parameters.AddWithValue("@Name", name);
                            if (name == null) nameParam.Value = DBNull.Value;
                            SqlParameter durationParam = myAuditData.Parameters.AddWithValue("@Duration", duration);
                            if (duration == 0.00) durationParam.Value = DBNull.Value;
                            SqlParameter deviceidParam = myAuditData.Parameters.AddWithValue("@Device_ID", deviceid);
                            if (deviceid == null) deviceidParam.Value = DBNull.Value;
                            SqlParameter ownerParam = myAuditData.Parameters.AddWithValue("@Owner", owner);
                            if (owner == null) ownerParam.Value = DBNull.Value;
                            SqlParameter authorParam = myAuditData.Parameters.AddWithValue("@Author", author);
                            if (author == null) authorParam.Value = DBNull.Value;
                            SqlParameter datecParam = myAuditData.Parameters.AddWithValue("@Date_Completed", dateco);
                            if (datecompleted == null) datecParam.Value = DBNull.Value;
                            SqlParameter datemParam = myAuditData.Parameters.AddWithValue("@Date_Modified", datemo);
                            if (datemodified == null) datemParam.Value = DBNull.Value;
                            SqlParameter datesParam = myAuditData.Parameters.AddWithValue("@Date_Started", datest);
                            if (datestarted == null) datesParam.Value = DBNull.Value;
                            myAuditData.ExecuteNonQuery();
                            #endregion //auditdata
                            #region //templatedata
                            //insert into Template Data
                            string templatedata = "INSERT INTO api_Template_Data (Audit_ID, Device_ID, Owner, Author, Name, Description, Media_ID)";
                            templatedata += "VALUES (@Audit_ID, @Device_ID, @Owner, @Author, @Name, @Description, @Media_ID)";

                            SqlCommand myTemplateData = new SqlCommand(templatedata, connection);
                            SqlParameter auditnoParam = myTemplateData.Parameters.AddWithValue("@Audit_ID", auditno);
                            if (auditno == null) auditnoParam.Value = DBNull.Value;
                            SqlParameter deviceParam = myTemplateData.Parameters.AddWithValue("@Device_ID", deviceid);
                            if (deviceid == null) deviceParam.Value = DBNull.Value;
                            SqlParameter owner1Param = myTemplateData.Parameters.AddWithValue("@Owner", owner);
                            if (owner == null) owner1Param.Value = DBNull.Value;
                            SqlParameter author1Param = myTemplateData.Parameters.AddWithValue("@Author", author);
                            if (author == null) author1Param.Value = DBNull.Value;
                            SqlParameter name1Param = myTemplateData.Parameters.AddWithValue("@Name", metname);
                            if (metname == null) name1Param.Value = DBNull.Value;
                            SqlParameter descriptionParam = myTemplateData.Parameters.AddWithValue("@Description", metdesc);
                            if (metdesc == null) descriptionParam.Value = DBNull.Value;
                            SqlParameter mediaParam = myTemplateData.Parameters.AddWithValue("@Media_ID", metimage);
                            if (metimage == null) mediaParam.Value = DBNull.Value;
                            myTemplateData.ExecuteNonQuery();
                            #endregion //templatedata
                            #region //headeritems
                            //loop for header items
                            foreach (var header in auditresult.header_items)
                            {
                                var audit_no = auditresult.audit_id;

                                if (header.label == "System ID")
                                {
                                    #region initialize values
                                    //initialize values
                                    var parentid = String.Empty;
                                    var itemid = String.Empty;
                                    var label = String.Empty;
                                    var section = String.Empty;
                                    var systemid = String.Empty;
                                    //check if null
                                    if (header == null || header.parent_id == null || header.parent_id == String.Empty)
                                        parentid = null;
                                    else
                                        parentid = header.parent_id;
                                    if (header == null || header.item_id == null || header.item_id == String.Empty)
                                        itemid = null;
                                    else
                                        itemid = header.item_id;
                                    if (header == null || header.label == null || header.label == String.Empty)
                                        label = null;
                                    else
                                        label = header.label;
                                    if (header == null || header.section == null || header.section == String.Empty)
                                        section = null;
                                    else
                                        section = header.section;
                                    if (header == null || header.responses == null || header.responses.text == null || header.responses.text == String.Empty)
                                        systemid = null;
                                    else
                                        systemid = header.responses.text;
                                    #endregion
                                    #region insert into Header Items
                                    //insert into Header Items
                                    string headeritems = "INSERT INTO api_Header_Items (Audit_ID, Parent_ID, Item_ID, Label, Section, TexT )";
                                    headeritems += " VALUES (@Audit_ID, @Parent_ID, @Item_ID, @Label, @Section, @Text)";

                                    SqlCommand myHeaderItems = new SqlCommand(headeritems, connection);
                                    SqlParameter auditParam = myHeaderItems.Parameters.AddWithValue("@Audit_ID", audit_no);
                                    if (audit_no == null) auditParam.Value = DBNull.Value;
                                    SqlParameter parentParam = myHeaderItems.Parameters.AddWithValue("@Parent_ID", parentid);
                                    if (parentid == null) parentParam.Value = DBNull.Value;
                                    SqlParameter itemParam = myHeaderItems.Parameters.AddWithValue("@Item_ID", itemid);
                                    if (itemid == null) itemParam.Value = DBNull.Value;
                                    SqlParameter labelParam = myHeaderItems.Parameters.AddWithValue("@Label", label);
                                    if (label == null) labelParam.Value = DBNull.Value;
                                    SqlParameter sectionParam = myHeaderItems.Parameters.AddWithValue("@Section", section);
                                    if (section == null) sectionParam.Value = DBNull.Value;
                                    SqlParameter textParam = myHeaderItems.Parameters.AddWithValue("@Text", systemid);
                                    if (systemid == null) textParam.Value = DBNull.Value;
                                    //SqlParameter modifiedParam = myHeaderItems.Parameters.AddWithValue("@Modified_Date", modat);
                                    //if (modat == null) modifiedParam.Value = DBNull.Value;
                                    myHeaderItems.ExecuteNonQuery();
                                    #endregion
                                    //to get the region of SYSTEM ID and correct the dates
                                    SqlConnection connectIBASE = new SqlConnection(@"Server=pd1g9d7qth63qbk.c2ikttwxhgln.ap-northeast-1.rds.amazonaws.com;Database=Hccloudservices; User Id=admin; password=?hXfSXYco4J;max pool size=1;MultipleActiveResultSets=True;");
                                    connectIBASE.Open();

                                    var ibase = "SELECT Region FROM PRD.IBASES WHERE (FLNumber = @FLNumber)";
                                    var ibases = new SqlCommand(ibase, connectIBASE);
                                    var sysid = "800-" + systemid;
                                    var checkid = ibases.Parameters.AddWithValue("@FLNumber", sysid);
                                    if (sysid == null) checkid.Value = DBNull.Value;
                                    ibases.ExecuteNonQuery();
                                    var sIBASE = (String)ibases.ExecuteScalar();
                                    #region Region is not null
                                    if (sIBASE != null) // correct system id
                                    {
                                        ////for timezone settings
                                        DateTime date;
                                        TimeZoneInfo tst;
                                        TimeZoneInfo dst;
                                        bool isDaylight;

                                        createdat = DateTimeOffset.Parse(created_at, System.Globalization.CultureInfo.GetCultureInfo("en-AU"));
                                        modifiedat = DateTimeOffset.Parse(modified_at, System.Globalization.CultureInfo.GetCultureInfo("en-AU"));
                                        datec = DateTimeOffset.Parse(datecompleted, System.Globalization.CultureInfo.GetCultureInfo("en-AU"));
                                        datem = DateTimeOffset.Parse(datemodified, System.Globalization.CultureInfo.GetCultureInfo("en-AU"));
                                        dates = DateTimeOffset.Parse(datestarted, System.Globalization.CultureInfo.GetCultureInfo("en-AU"));

                                        #region ACT, VIC, NSW
                                        if (sIBASE == "ACT" || sIBASE == "VIC" || sIBASE == "NSW")
                                        {
                                            tst = TimeZoneInfo.FindSystemTimeZoneById("AUS Eastern Standard Time");
                                            creatat = TimeZoneInfo.ConvertTime(createdat, tst);
                                            isDaylight = tst.IsDaylightSavingTime(createdat);
                                            if (isDaylight == true)
                                            {
                                                //dst = TimeZoneInfo.FindSystemTimeZoneById("AUS Eastern Daylight Time");
                                                creatat = TimeZoneInfo.ConvertTime(createdat, tst);
                                                modat = TimeZoneInfo.ConvertTime(modifiedat, tst);
                                                dateco = TimeZoneInfo.ConvertTime(datec, tst);
                                                datemo = TimeZoneInfo.ConvertTime(datem, tst);
                                                datest = TimeZoneInfo.ConvertTime(dates, tst);
                                                //update audit table
                                                string auditupdate = "UPDATE api_Audit SET Created_at = @Created_at, Modified_at = @Modified_at Where Audit_id = @Audit_ID";

                                                SqlCommand cmd = new SqlCommand(auditupdate, connection);
                                                cmd.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                cmd.Parameters.AddWithValue("@Created_at", creatat);
                                                cmd.Parameters.AddWithValue("@Modified_at", modat);
                                                cmd.ExecuteNonQuery();

                                                string auditdataupdate = "UPDATE api_AuditData SET Date_Completed = @Date_Completed, Date_Modified = @Date_Modified, Date_Started = @Date_Started WHERE Audit_id = @Audit_ID";

                                                SqlCommand cmdauditdata = new SqlCommand(auditdataupdate, connection);
                                                cmdauditdata.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                cmdauditdata.Parameters.AddWithValue("@Date_Completed", dateco);
                                                cmdauditdata.Parameters.AddWithValue("@Date_Modified", datemo);
                                                cmdauditdata.Parameters.AddWithValue("@Date_Started", datest);
                                                cmdauditdata.ExecuteNonQuery();
                                            }
                                            else
                                            {
                                                modat = TimeZoneInfo.ConvertTime(modifiedat, tst);
                                                dateco = TimeZoneInfo.ConvertTime(datec, tst);
                                                datemo = TimeZoneInfo.ConvertTime(datem, tst);
                                                datest = TimeZoneInfo.ConvertTime(dates, tst);
                                                //update audit table
                                                string auditupdate = "UPDATE api_Audit SET Created_at = @Created_at, Modified_at = @Modified_at Where Audit_id = @Audit_ID";

                                                SqlCommand cmd = new SqlCommand(auditupdate, connection);
                                                cmd.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                cmd.Parameters.AddWithValue("@Created_at", creatat);
                                                cmd.Parameters.AddWithValue("@Modified_at", modat);
                                                cmd.ExecuteNonQuery();

                                                //update auditdata
                                                string auditdataupdate = "UPDATE api_AuditData SET Date_Completed = @Date_Completed, Date_Modified = @Date_Modified, Date_Started = @Date_Started WHERE Audit_id = @Audit_ID";
                                                SqlCommand cmdauditdata = new SqlCommand(auditdataupdate, connection);
                                                cmdauditdata.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                cmdauditdata.Parameters.AddWithValue("@Date_Completed", dateco);
                                                cmdauditdata.Parameters.AddWithValue("@Date_Modified", datemo);
                                                cmdauditdata.Parameters.AddWithValue("@Date_Started", datest);
                                                cmdauditdata.ExecuteNonQuery();
                                            }
                                        }
                                        #endregion
                                        #region QLD
                                        else if (sIBASE == "QLD")
                                        {
                                            tst = TimeZoneInfo.FindSystemTimeZoneById("E. Australia Standard Time");
                                            creatat = TimeZoneInfo.ConvertTime(createdat, tst);
                                            modat = TimeZoneInfo.ConvertTime(modifiedat, tst);
                                            dateco = TimeZoneInfo.ConvertTime(datec, tst);
                                            datemo = TimeZoneInfo.ConvertTime(datem, tst);
                                            datest = TimeZoneInfo.ConvertTime(dates, tst);
                                            //update audit table
                                            string auditupdate = "UPDATE api_Audit SET Created_at = @Created_at, Modified_at = @Modified_at Where Audit_id = @Audit_ID";

                                            SqlCommand cmd = new SqlCommand(auditupdate, connection);
                                            cmd.Parameters.AddWithValue("@Audit_ID", audit_no);
                                            cmd.Parameters.AddWithValue("@Created_at", creatat);
                                            cmd.Parameters.AddWithValue("@Modified_at", modat);
                                            cmd.ExecuteNonQuery();

                                            //update auditdata
                                            string auditdataupdate = "UPDATE api_AuditData SET Date_Completed = @Date_Completed, Date_Modified = @Date_Modified, Date_Started = @Date_Started WHERE Audit_id = @Audit_ID";
                                            SqlCommand cmdauditdata = new SqlCommand(auditdataupdate, connection);
                                            cmdauditdata.Parameters.AddWithValue("@Audit_ID", audit_no);
                                            cmdauditdata.Parameters.AddWithValue("@Date_Completed", dateco);
                                            cmdauditdata.Parameters.AddWithValue("@Date_Modified", datemo);
                                            cmdauditdata.Parameters.AddWithValue("@Date_Started", datest);
                                            cmdauditdata.ExecuteNonQuery();
                                        }
                                        #endregion
                                        #region SA
                                        else if (sIBASE == "SA")
                                        {
                                            tst = TimeZoneInfo.FindSystemTimeZoneById("AUS Central Standard Time");
                                            creatat = TimeZoneInfo.ConvertTime(createdat, tst);
                                            isDaylight = tst.IsDaylightSavingTime(createdat);
                                            if (isDaylight == true)
                                            {
                                                //dst = TimeZoneInfo.FindSystemTimeZoneById("AUS Central Daylight Time");
                                                creatat = TimeZoneInfo.ConvertTime(createdat, tst);
                                                modat = TimeZoneInfo.ConvertTime(modifiedat, tst);
                                                dateco = TimeZoneInfo.ConvertTime(datec, tst);
                                                datemo = TimeZoneInfo.ConvertTime(datem, tst);
                                                datest = TimeZoneInfo.ConvertTime(dates, tst);
                                                //update audit table
                                                string auditupdate = "UPDATE api_Audit SET Created_at = @Created_at, Modified_at = @Modified_at Where Audit_id = @Audit_ID";

                                                SqlCommand cmd = new SqlCommand(auditupdate, connection);
                                                cmd.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                cmd.Parameters.AddWithValue("@Created_at", creatat);
                                                cmd.Parameters.AddWithValue("@Modified_at", modat);
                                                cmd.ExecuteNonQuery();

                                                string auditdataupdate = "UPDATE api_AuditData SET Date_Completed = @Date_Completed, Date_Modified = @Date_Modified, Date_Started = @Date_Started WHERE Audit_id = @Audit_ID";

                                                SqlCommand cmdauditdata = new SqlCommand(auditdataupdate, connection);
                                                cmdauditdata.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                cmdauditdata.Parameters.AddWithValue("@Date_Completed", dateco);
                                                cmdauditdata.Parameters.AddWithValue("@Date_Modified", datemo);
                                                cmdauditdata.Parameters.AddWithValue("@Date_Started", datest);
                                                cmdauditdata.ExecuteNonQuery();
                                            }
                                            else
                                            {
                                                modat = TimeZoneInfo.ConvertTime(modifiedat, tst);
                                                dateco = TimeZoneInfo.ConvertTime(datec, tst);
                                                datemo = TimeZoneInfo.ConvertTime(datem, tst);
                                                datest = TimeZoneInfo.ConvertTime(dates, tst);
                                                //update audit table
                                                string auditupdate = "UPDATE api_Audit SET Created_at = @Created_at, Modified_at = @Modified_at Where Audit_id = @Audit_ID";

                                                SqlCommand cmd = new SqlCommand(auditupdate, connection);
                                                cmd.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                cmd.Parameters.AddWithValue("@Created_at", creatat);
                                                cmd.Parameters.AddWithValue("@Modified_at", modat);
                                                cmd.ExecuteNonQuery();

                                                //update auditdata
                                                string auditdataupdate = "UPDATE api_AuditData SET Date_Completed = @Date_Completed, Date_Modified = @Date_Modified, Date_Started = @Date_Started WHERE Audit_id = @Audit_ID";
                                                SqlCommand cmdauditdata = new SqlCommand(auditdataupdate, connection);
                                                cmdauditdata.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                cmdauditdata.Parameters.AddWithValue("@Date_Completed", dateco);
                                                cmdauditdata.Parameters.AddWithValue("@Date_Modified", datemo);
                                                cmdauditdata.Parameters.AddWithValue("@Date_Started", datest);
                                                cmdauditdata.ExecuteNonQuery();
                                            }
                                        }
                                        #endregion
                                        #region NT
                                        else if (sIBASE == "NT")
                                        {
                                            tst = TimeZoneInfo.FindSystemTimeZoneById("AUS Central Standard Time");
                                            creatat = TimeZoneInfo.ConvertTime(createdat, tst);
                                            modat = TimeZoneInfo.ConvertTime(modifiedat, tst);
                                            dateco = TimeZoneInfo.ConvertTime(datec, tst);
                                            datemo = TimeZoneInfo.ConvertTime(datem, tst);
                                            datest = TimeZoneInfo.ConvertTime(dates, tst);
                                            //update audit table
                                            string auditupdate = "UPDATE api_Audit SET Created_at = @Created_at, Modified_at = @Modified_at Where Audit_id = @Audit_ID";

                                            SqlCommand cmd = new SqlCommand(auditupdate, connection);
                                            cmd.Parameters.AddWithValue("@Audit_ID", audit_no);
                                            cmd.Parameters.AddWithValue("@Created_at", creatat);
                                            cmd.Parameters.AddWithValue("@Modified_at", modat);
                                            cmd.ExecuteNonQuery();

                                            //update auditdata
                                            string auditdataupdate = "UPDATE api_AuditData SET Date_Completed = @Date_Completed, Date_Modified = @Date_Modified, Date_Started = @Date_Started WHERE Audit_id = @Audit_ID";
                                            SqlCommand cmdauditdata = new SqlCommand(auditdataupdate, connection);
                                            cmdauditdata.Parameters.AddWithValue("@Audit_ID", audit_no);
                                            cmdauditdata.Parameters.AddWithValue("@Date_Completed", dateco);
                                            cmdauditdata.Parameters.AddWithValue("@Date_Modified", datemo);
                                            cmdauditdata.Parameters.AddWithValue("@Date_Started", datest);
                                            cmdauditdata.ExecuteNonQuery();
                                        }
                                        #endregion
                                        #region WA
                                        else if (sIBASE == "WA")
                                        {
                                            tst = TimeZoneInfo.FindSystemTimeZoneById("W. Australia Standard Time");
                                            creatat = TimeZoneInfo.ConvertTime(createdat, tst);
                                            modat = TimeZoneInfo.ConvertTime(modifiedat, tst);
                                            dateco = TimeZoneInfo.ConvertTime(datec, tst);
                                            datemo = TimeZoneInfo.ConvertTime(datem, tst);
                                            datest = TimeZoneInfo.ConvertTime(dates, tst);
                                            //update audit table
                                            string auditupdate = "UPDATE api_Audit SET Created_at = @Created_at, Modified_at = @Modified_at Where Audit_id = @Audit_ID";

                                            SqlCommand cmd = new SqlCommand(auditupdate, connection);
                                            cmd.Parameters.AddWithValue("@Audit_ID", audit_no);
                                            cmd.Parameters.AddWithValue("@Created_at", creatat);
                                            cmd.Parameters.AddWithValue("@Modified_at", modat);
                                            cmd.ExecuteNonQuery();

                                            //update auditdata
                                            string auditdataupdate = "UPDATE api_AuditData SET Date_Completed = @Date_Completed, Date_Modified = @Date_Modified, Date_Started = @Date_Started WHERE Audit_id = @Audit_ID";
                                            SqlCommand cmdauditdata = new SqlCommand(auditdataupdate, connection);
                                            cmdauditdata.Parameters.AddWithValue("@Audit_ID", audit_no);
                                            cmdauditdata.Parameters.AddWithValue("@Date_Completed", dateco);
                                            cmdauditdata.Parameters.AddWithValue("@Date_Modified", datemo);
                                            cmdauditdata.Parameters.AddWithValue("@Date_Started", datest);
                                            cmdauditdata.ExecuteNonQuery();
                                        }
                                        #endregion
                                        #region NZ
                                        else if (sIBASE == "AKL" || sIBASE == "AUK" || sIBASE == "BOP" || sIBASE == "CAN" || sIBASE == "GIS" || sIBASE == "HKB" || sIBASE == "MBH" || sIBASE == "MWT" || sIBASE == "NSN" || sIBASE == "NTL" || sIBASE == "OTA" || sIBASE == "STL" || sIBASE == "TKI" || sIBASE == "WGN" || sIBASE == "WKO" || sIBASE == "WTC")
                                        {
                                            createdat = DateTimeOffset.Parse(created_at, System.Globalization.CultureInfo.GetCultureInfo("en-NZ"));
                                            modifiedat = DateTimeOffset.Parse(modified_at, System.Globalization.CultureInfo.GetCultureInfo("en-NZ"));
                                            datec = DateTimeOffset.Parse(datecompleted, System.Globalization.CultureInfo.GetCultureInfo("en-NZ"));
                                            datem = DateTimeOffset.Parse(datemodified, System.Globalization.CultureInfo.GetCultureInfo("en-NZ"));
                                            dates = DateTimeOffset.Parse(datestarted, System.Globalization.CultureInfo.GetCultureInfo("en-NZ"));

                                            tst = TimeZoneInfo.FindSystemTimeZoneById("New Zealand Standard Time");
                                            creatat = TimeZoneInfo.ConvertTime(createdat, tst);
                                            isDaylight = tst.IsDaylightSavingTime(createdat);
                                            if (isDaylight == true)
                                            {
                                                //dst = TimeZoneInfo.FindSystemTimeZoneById("New Zealand Daylight Time");
                                                creatat = TimeZoneInfo.ConvertTime(createdat, tst);
                                                modat = TimeZoneInfo.ConvertTime(modifiedat, tst);
                                                dateco = TimeZoneInfo.ConvertTime(datec, tst);
                                                datemo = TimeZoneInfo.ConvertTime(datem, tst);
                                                datest = TimeZoneInfo.ConvertTime(dates, tst);
                                                //update audit table
                                                string auditupdate = "UPDATE api_Audit SET Created_at = @Created_at, Modified_at = @Modified_at Where Audit_id = @Audit_ID";

                                                SqlCommand cmd = new SqlCommand(auditupdate, connection);
                                                cmd.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                cmd.Parameters.AddWithValue("@Created_at", creatat);
                                                cmd.Parameters.AddWithValue("@Modified_at", modat);
                                                cmd.ExecuteNonQuery();

                                                string auditdataupdate = "UPDATE api_AuditData SET Date_Completed = @Date_Completed, Date_Modified = @Date_Modified, Date_Started = @Date_Started WHERE Audit_id = @Audit_ID";

                                                SqlCommand cmdauditdata = new SqlCommand(auditdataupdate, connection);
                                                cmdauditdata.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                cmdauditdata.Parameters.AddWithValue("@Date_Completed", dateco);
                                                cmdauditdata.Parameters.AddWithValue("@Date_Modified", datemo);
                                                cmdauditdata.Parameters.AddWithValue("@Date_Started", datest);
                                                cmdauditdata.ExecuteNonQuery();
                                            }
                                            else
                                            {
                                                modat = TimeZoneInfo.ConvertTime(modifiedat, tst);
                                                dateco = TimeZoneInfo.ConvertTime(datec, tst);
                                                datemo = TimeZoneInfo.ConvertTime(datem, tst);
                                                datest = TimeZoneInfo.ConvertTime(dates, tst);
                                                //update audit table
                                                string auditupdate = "UPDATE api_Audit SET Created_at = @Created_at, Modified_at = @Modified_at Where Audit_id = @Audit_ID";

                                                SqlCommand cmd = new SqlCommand(auditupdate, connection);
                                                cmd.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                cmd.Parameters.AddWithValue("@Created_at", creatat);
                                                cmd.Parameters.AddWithValue("@Modified_at", modat);
                                                cmd.ExecuteNonQuery();

                                                //update auditdata
                                                string auditdataupdate = "UPDATE api_AuditData SET Date_Completed = @Date_Completed, Date_Modified = @Date_Modified, Date_Started = @Date_Started WHERE Audit_id = @Audit_ID";
                                                SqlCommand cmdauditdata = new SqlCommand(auditdataupdate, connection);
                                                cmdauditdata.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                cmdauditdata.Parameters.AddWithValue("@Date_Completed", dateco);
                                                cmdauditdata.Parameters.AddWithValue("@Date_Modified", datemo);
                                                cmdauditdata.Parameters.AddWithValue("@Date_Started", datest);
                                                cmdauditdata.ExecuteNonQuery();
                                            }
                                        }
                                        #endregion
                                        #region TAS
                                        else if (sIBASE == "TAS")
                                        {
                                            //get the country
                                            var ibasecountry = "SELECT Country FROM PRD.IBASES WHERE (FLNumber = @FLNumber and Region = @Region)";
                                            var ibasecountries = new SqlCommand(ibasecountry, connectIBASE);
                                            var systid = "800-" + systemid;
                                            var checksid = ibasecountries.Parameters.AddWithValue("@FLNumber", sysid);
                                            if (systid == null) checksid.Value = DBNull.Value;
                                            var checksreg = ibasecountries.Parameters.AddWithValue("@Region", "TAS");
                                            ibasecountries.ExecuteNonQuery();
                                            var sIBASECountry = (String)ibasecountries.ExecuteScalar();
                                            if (sIBASECountry != null)
                                            {
                                                #region AU
                                                if (sIBASECountry == "AU")
                                                {
                                                    tst = TimeZoneInfo.FindSystemTimeZoneById("AUS Eastern Standard Time");
                                                    creatat = TimeZoneInfo.ConvertTime(createdat, tst);
                                                    isDaylight = tst.IsDaylightSavingTime(createdat);
                                                    if (isDaylight == true)
                                                    {
                                                        //dst = TimeZoneInfo.FindSystemTimeZoneById("AUS Eastern Daylight Time");
                                                        creatat = TimeZoneInfo.ConvertTime(createdat, tst);
                                                        modat = TimeZoneInfo.ConvertTime(modifiedat, tst);
                                                        dateco = TimeZoneInfo.ConvertTime(datec, tst);
                                                        datemo = TimeZoneInfo.ConvertTime(datem, tst);
                                                        datest = TimeZoneInfo.ConvertTime(dates, tst);
                                                        //update audit table
                                                        string auditupdate = "UPDATE api_Audit SET Created_at = @Created_at, Modified_at = @Modified_at Where Audit_id = @Audit_ID";

                                                        SqlCommand cmd = new SqlCommand(auditupdate, connection);
                                                        cmd.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                        cmd.Parameters.AddWithValue("@Created_at", creatat);
                                                        cmd.Parameters.AddWithValue("@Modified_at", modat);
                                                        cmd.ExecuteNonQuery();

                                                        string auditdataupdate = "UPDATE api_AuditData SET Date_Completed = @Date_Completed, Date_Modified = @Date_Modified, Date_Started = @Date_Started WHERE Audit_id = @Audit_ID";

                                                        SqlCommand cmdauditdata = new SqlCommand(auditdataupdate, connection);
                                                        cmdauditdata.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                        cmdauditdata.Parameters.AddWithValue("@Date_Completed", dateco);
                                                        cmdauditdata.Parameters.AddWithValue("@Date_Modified", datemo);
                                                        cmdauditdata.Parameters.AddWithValue("@Date_Started", datest);
                                                        cmdauditdata.ExecuteNonQuery();
                                                    }
                                                    else
                                                    {
                                                        modat = TimeZoneInfo.ConvertTime(modifiedat, tst);
                                                        dateco = TimeZoneInfo.ConvertTime(datec, tst);
                                                        datemo = TimeZoneInfo.ConvertTime(datem, tst);
                                                        datest = TimeZoneInfo.ConvertTime(dates, tst);
                                                        //update audit table
                                                        string auditupdate = "UPDATE api_Audit SET Created_at = @Created_at, Modified_at = @Modified_at Where Audit_id = @Audit_ID";

                                                        SqlCommand cmd = new SqlCommand(auditupdate, connection);
                                                        cmd.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                        cmd.Parameters.AddWithValue("@Created_at", creatat);
                                                        cmd.Parameters.AddWithValue("@Modified_at", modat);
                                                        cmd.ExecuteNonQuery();

                                                        //update auditdata
                                                        string auditdataupdate = "UPDATE api_AuditData SET Date_Completed = @Date_Completed, Date_Modified = @Date_Modified, Date_Started = @Date_Started WHERE Audit_id = @Audit_ID";
                                                        SqlCommand cmdauditdata = new SqlCommand(auditdataupdate, connection);
                                                        cmdauditdata.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                        cmdauditdata.Parameters.AddWithValue("@Date_Completed", dateco);
                                                        cmdauditdata.Parameters.AddWithValue("@Date_Modified", datemo);
                                                        cmdauditdata.Parameters.AddWithValue("@Date_Started", datest);
                                                        cmdauditdata.ExecuteNonQuery();
                                                    }
                                                }
                                                #endregion
                                                #region NZ
                                                else if (sIBASECountry == "NZ")
                                                {
                                                    createdat = DateTimeOffset.Parse(created_at, System.Globalization.CultureInfo.GetCultureInfo("en-NZ"));
                                                    modifiedat = DateTimeOffset.Parse(modified_at, System.Globalization.CultureInfo.GetCultureInfo("en-NZ"));
                                                    datec = DateTimeOffset.Parse(datecompleted, System.Globalization.CultureInfo.GetCultureInfo("en-NZ"));
                                                    datem = DateTimeOffset.Parse(datemodified, System.Globalization.CultureInfo.GetCultureInfo("en-NZ"));
                                                    dates = DateTimeOffset.Parse(datestarted, System.Globalization.CultureInfo.GetCultureInfo("en-NZ"));

                                                    tst = TimeZoneInfo.FindSystemTimeZoneById("New Zealand Standard Time");
                                                    creatat = TimeZoneInfo.ConvertTime(createdat, tst);
                                                    isDaylight = tst.IsDaylightSavingTime(createdat);
                                                    if (isDaylight == true)
                                                    {
                                                        //dst = TimeZoneInfo.FindSystemTimeZoneById("New Zealand Daylight Time");
                                                        creatat = TimeZoneInfo.ConvertTime(createdat, tst);
                                                        modat = TimeZoneInfo.ConvertTime(modifiedat, tst);
                                                        dateco = TimeZoneInfo.ConvertTime(datec, tst);
                                                        datemo = TimeZoneInfo.ConvertTime(datem, tst);
                                                        datest = TimeZoneInfo.ConvertTime(dates, tst);
                                                        //update audit table
                                                        string auditupdate = "UPDATE api_Audit SET Created_at = @Created_at, Modified_at = @Modified_at Where Audit_id = @Audit_ID";

                                                        SqlCommand cmd = new SqlCommand(auditupdate, connection);
                                                        cmd.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                        cmd.Parameters.AddWithValue("@Created_at", creatat);
                                                        cmd.Parameters.AddWithValue("@Modified_at", modat);
                                                        cmd.ExecuteNonQuery();

                                                        string auditdataupdate = "UPDATE api_AuditData SET Date_Completed = @Date_Completed, Date_Modified = @Date_Modified, Date_Started = @Date_Started WHERE Audit_id = @Audit_ID";

                                                        SqlCommand cmdauditdata = new SqlCommand(auditdataupdate, connection);
                                                        cmdauditdata.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                        cmdauditdata.Parameters.AddWithValue("@Date_Completed", dateco);
                                                        cmdauditdata.Parameters.AddWithValue("@Date_Modified", datemo);
                                                        cmdauditdata.Parameters.AddWithValue("@Date_Started", datest);
                                                        cmdauditdata.ExecuteNonQuery();
                                                    }
                                                    else
                                                    {
                                                        modat = TimeZoneInfo.ConvertTime(modifiedat, tst);
                                                        dateco = TimeZoneInfo.ConvertTime(datec, tst);
                                                        datemo = TimeZoneInfo.ConvertTime(datem, tst);
                                                        datest = TimeZoneInfo.ConvertTime(dates, tst);
                                                        //update audit table
                                                        string auditupdate = "UPDATE api_Audit SET Created_at = @Created_at, Modified_at = @Modified_at Where Audit_id = @Audit_ID";

                                                        SqlCommand cmd = new SqlCommand(auditupdate, connection);
                                                        cmd.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                        cmd.Parameters.AddWithValue("@Created_at", creatat);
                                                        cmd.Parameters.AddWithValue("@Modified_at", modat);
                                                        cmd.ExecuteNonQuery();

                                                        //update auditdata
                                                        string auditdataupdate = "UPDATE api_AuditData SET Date_Completed = @Date_Completed, Date_Modified = @Date_Modified, Date_Started = @Date_Started WHERE Audit_id = @Audit_ID";
                                                        SqlCommand cmdauditdata = new SqlCommand(auditdataupdate, connection);
                                                        cmdauditdata.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                        cmdauditdata.Parameters.AddWithValue("@Date_Completed", dateco);
                                                        cmdauditdata.Parameters.AddWithValue("@Date_Modified", datemo);
                                                        cmdauditdata.Parameters.AddWithValue("@Date_Started", datest);
                                                        cmdauditdata.ExecuteNonQuery();
                                                    }
                                                }
                                                #endregion
                                            }
                                        }
                                        #endregion

                                    }
                                    #endregion
                                    #region if System ID is not valid then get the Region of Employee
                                    else
                                    {
                                        string[] names = owner.ToString().Trim().Split(new char[] { ' ' }, 2);
                                        string fname = "";
                                        string lname = "";

                                        if (names.Length == 1)
                                        {
                                            fname = "";
                                            lname = names[0];
                                        }
                                        else
                                        {
                                            fname = names[0];
                                            lname = names[1];
                                        }

                                        var empReg = "SELECT Region FROM PRD.Employees WHERE (GivenName = @GivenName and LastName = @LastName and len(id) = 8 and (country <> '#N/A' and country <> ''))";
                                        var emps = new SqlCommand(empReg, connectIBASE);
                                        var checkf = emps.Parameters.AddWithValue("@GivenName", fname);
                                        if (fname == null) checkf.Value = DBNull.Value;
                                        var checkl = emps.Parameters.AddWithValue("@LastName", lname);
                                        if (lname == null) checkl.Value = DBNull.Value;
                                        emps.ExecuteNonQuery();
                                        var sEMP = (String)emps.ExecuteScalar();

                                        if (sEMP != null)
                                        {
                                            ////for timezone settings
                                            DateTime date;
                                            TimeZoneInfo tst;
                                            TimeZoneInfo dst;
                                            bool isDaylight;

                                            createdat = DateTimeOffset.Parse(created_at, System.Globalization.CultureInfo.GetCultureInfo("en-AU"));
                                            modifiedat = DateTimeOffset.Parse(modified_at, System.Globalization.CultureInfo.GetCultureInfo("en-AU"));
                                            datec = DateTimeOffset.Parse(datecompleted, System.Globalization.CultureInfo.GetCultureInfo("en-AU"));
                                            datem = DateTimeOffset.Parse(datemodified, System.Globalization.CultureInfo.GetCultureInfo("en-AU"));
                                            dates = DateTimeOffset.Parse(datestarted, System.Globalization.CultureInfo.GetCultureInfo("en-AU"));

                                            #region ACT, VIC, NSW
                                            if (sEMP == "ACT" || sEMP == "VIC" || sEMP == "NSW")
                                            {
                                                tst = TimeZoneInfo.FindSystemTimeZoneById("AUS Eastern Standard Time");
                                                creatat = TimeZoneInfo.ConvertTime(createdat, tst);
                                                isDaylight = tst.IsDaylightSavingTime(createdat);
                                                if (isDaylight == true)
                                                {
                                                    //dst = TimeZoneInfo.FindSystemTimeZoneById("AUS Eastern Daylight Time");
                                                    creatat = TimeZoneInfo.ConvertTime(createdat, tst);
                                                    modat = TimeZoneInfo.ConvertTime(modifiedat, tst);
                                                    dateco = TimeZoneInfo.ConvertTime(datec, tst);
                                                    datemo = TimeZoneInfo.ConvertTime(datem, tst);
                                                    datest = TimeZoneInfo.ConvertTime(dates, tst);
                                                    //update audit table
                                                    string auditupdate = "UPDATE api_Audit SET Created_at = @Created_at, Modified_at = @Modified_at Where Audit_id = @Audit_ID";

                                                    SqlCommand cmd = new SqlCommand(auditupdate, connection);
                                                    cmd.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                    cmd.Parameters.AddWithValue("@Created_at", creatat);
                                                    cmd.Parameters.AddWithValue("@Modified_at", modat);
                                                    cmd.ExecuteNonQuery();

                                                    string auditdataupdate = "UPDATE api_AuditData SET Date_Completed = @Date_Completed, Date_Modified = @Date_Modified, Date_Started = @Date_Started WHERE Audit_id = @Audit_ID";

                                                    SqlCommand cmdauditdata = new SqlCommand(auditdataupdate, connection);
                                                    cmdauditdata.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                    cmdauditdata.Parameters.AddWithValue("@Date_Completed", dateco);
                                                    cmdauditdata.Parameters.AddWithValue("@Date_Modified", datemo);
                                                    cmdauditdata.Parameters.AddWithValue("@Date_Started", datest);
                                                    cmdauditdata.ExecuteNonQuery();
                                                }
                                                else
                                                {
                                                    modat = TimeZoneInfo.ConvertTime(modifiedat, tst);
                                                    dateco = TimeZoneInfo.ConvertTime(datec, tst);
                                                    datemo = TimeZoneInfo.ConvertTime(datem, tst);
                                                    datest = TimeZoneInfo.ConvertTime(dates, tst);
                                                    //update audit table
                                                    string auditupdate = "UPDATE api_Audit SET Created_at = @Created_at, Modified_at = @Modified_at Where Audit_id = @Audit_ID";

                                                    SqlCommand cmd = new SqlCommand(auditupdate, connection);
                                                    cmd.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                    cmd.Parameters.AddWithValue("@Created_at", creatat);
                                                    cmd.Parameters.AddWithValue("@Modified_at", modat);
                                                    cmd.ExecuteNonQuery();

                                                    //update auditdata
                                                    string auditdataupdate = "UPDATE api_AuditData SET Date_Completed = @Date_Completed, Date_Modified = @Date_Modified, Date_Started = @Date_Started WHERE Audit_id = @Audit_ID";
                                                    SqlCommand cmdauditdata = new SqlCommand(auditdataupdate, connection);
                                                    cmdauditdata.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                    cmdauditdata.Parameters.AddWithValue("@Date_Completed", dateco);
                                                    cmdauditdata.Parameters.AddWithValue("@Date_Modified", datemo);
                                                    cmdauditdata.Parameters.AddWithValue("@Date_Started", datest);
                                                    cmdauditdata.ExecuteNonQuery();
                                                }
                                            }
                                            #endregion
                                            #region QLD
                                            else if (sEMP == "QLD")
                                            {
                                                tst = TimeZoneInfo.FindSystemTimeZoneById("E. Australia Standard Time");
                                                creatat = TimeZoneInfo.ConvertTime(createdat, tst);
                                                modat = TimeZoneInfo.ConvertTime(modifiedat, tst);
                                                dateco = TimeZoneInfo.ConvertTime(datec, tst);
                                                datemo = TimeZoneInfo.ConvertTime(datem, tst);
                                                datest = TimeZoneInfo.ConvertTime(dates, tst);
                                                //update audit table
                                                string auditupdate = "UPDATE api_Audit SET Created_at = @Created_at, Modified_at = @Modified_at Where Audit_id = @Audit_ID";

                                                SqlCommand cmd = new SqlCommand(auditupdate, connection);
                                                cmd.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                cmd.Parameters.AddWithValue("@Created_at", creatat);
                                                cmd.Parameters.AddWithValue("@Modified_at", modat);
                                                cmd.ExecuteNonQuery();

                                                //update auditdata
                                                string auditdataupdate = "UPDATE api_AuditData SET Date_Completed = @Date_Completed, Date_Modified = @Date_Modified, Date_Started = @Date_Started WHERE Audit_id = @Audit_ID";
                                                SqlCommand cmdauditdata = new SqlCommand(auditdataupdate, connection);
                                                cmdauditdata.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                cmdauditdata.Parameters.AddWithValue("@Date_Completed", dateco);
                                                cmdauditdata.Parameters.AddWithValue("@Date_Modified", datemo);
                                                cmdauditdata.Parameters.AddWithValue("@Date_Started", datest);
                                                cmdauditdata.ExecuteNonQuery();
                                            }
                                            #endregion
                                            #region SA
                                            else if (sEMP == "SA")
                                            {
                                                tst = TimeZoneInfo.FindSystemTimeZoneById("AUS Central Standard Time");
                                                creatat = TimeZoneInfo.ConvertTime(createdat, tst);
                                                isDaylight = tst.IsDaylightSavingTime(createdat);
                                                if (isDaylight == true)
                                                {
                                                    //dst = TimeZoneInfo.FindSystemTimeZoneById("AUS Central Daylight Time");
                                                    creatat = TimeZoneInfo.ConvertTime(createdat, tst);
                                                    modat = TimeZoneInfo.ConvertTime(modifiedat, tst);
                                                    dateco = TimeZoneInfo.ConvertTime(datec, tst);
                                                    datemo = TimeZoneInfo.ConvertTime(datem, tst);
                                                    datest = TimeZoneInfo.ConvertTime(dates, tst);
                                                    //update audit table
                                                    string auditupdate = "UPDATE api_Audit SET Created_at = @Created_at, Modified_at = @Modified_at Where Audit_id = @Audit_ID";

                                                    SqlCommand cmd = new SqlCommand(auditupdate, connection);
                                                    cmd.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                    cmd.Parameters.AddWithValue("@Created_at", creatat);
                                                    cmd.Parameters.AddWithValue("@Modified_at", modat);
                                                    cmd.ExecuteNonQuery();

                                                    string auditdataupdate = "UPDATE api_AuditData SET Date_Completed = @Date_Completed, Date_Modified = @Date_Modified, Date_Started = @Date_Started WHERE Audit_id = @Audit_ID";

                                                    SqlCommand cmdauditdata = new SqlCommand(auditdataupdate, connection);
                                                    cmdauditdata.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                    cmdauditdata.Parameters.AddWithValue("@Date_Completed", dateco);
                                                    cmdauditdata.Parameters.AddWithValue("@Date_Modified", datemo);
                                                    cmdauditdata.Parameters.AddWithValue("@Date_Started", datest);
                                                    cmdauditdata.ExecuteNonQuery();
                                                }
                                                else
                                                {
                                                    modat = TimeZoneInfo.ConvertTime(modifiedat, tst);
                                                    dateco = TimeZoneInfo.ConvertTime(datec, tst);
                                                    datemo = TimeZoneInfo.ConvertTime(datem, tst);
                                                    datest = TimeZoneInfo.ConvertTime(dates, tst);
                                                    //update audit table
                                                    string auditupdate = "UPDATE api_Audit SET Created_at = @Created_at, Modified_at = @Modified_at Where Audit_id = @Audit_ID";

                                                    SqlCommand cmd = new SqlCommand(auditupdate, connection);
                                                    cmd.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                    cmd.Parameters.AddWithValue("@Created_at", creatat);
                                                    cmd.Parameters.AddWithValue("@Modified_at", modat);
                                                    cmd.ExecuteNonQuery();

                                                    //update auditdata
                                                    string auditdataupdate = "UPDATE api_AuditData SET Date_Completed = @Date_Completed, Date_Modified = @Date_Modified, Date_Started = @Date_Started WHERE Audit_id = @Audit_ID";
                                                    SqlCommand cmdauditdata = new SqlCommand(auditdataupdate, connection);
                                                    cmdauditdata.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                    cmdauditdata.Parameters.AddWithValue("@Date_Completed", dateco);
                                                    cmdauditdata.Parameters.AddWithValue("@Date_Modified", datemo);
                                                    cmdauditdata.Parameters.AddWithValue("@Date_Started", datest);
                                                    cmdauditdata.ExecuteNonQuery();
                                                }
                                            }
                                            #endregion
                                            #region NT
                                            else if (sEMP == "NT")
                                            {
                                                tst = TimeZoneInfo.FindSystemTimeZoneById("AUS Central Standard Time");
                                                creatat = TimeZoneInfo.ConvertTime(createdat, tst);
                                                modat = TimeZoneInfo.ConvertTime(modifiedat, tst);
                                                dateco = TimeZoneInfo.ConvertTime(datec, tst);
                                                datemo = TimeZoneInfo.ConvertTime(datem, tst);
                                                datest = TimeZoneInfo.ConvertTime(dates, tst);
                                                //update audit table
                                                string auditupdate = "UPDATE api_Audit SET Created_at = @Created_at, Modified_at = @Modified_at Where Audit_id = @Audit_ID";

                                                SqlCommand cmd = new SqlCommand(auditupdate, connection);
                                                cmd.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                cmd.Parameters.AddWithValue("@Created_at", creatat);
                                                cmd.Parameters.AddWithValue("@Modified_at", modat);
                                                cmd.ExecuteNonQuery();

                                                //update auditdata
                                                string auditdataupdate = "UPDATE api_AuditData SET Date_Completed = @Date_Completed, Date_Modified = @Date_Modified, Date_Started = @Date_Started WHERE Audit_id = @Audit_ID";
                                                SqlCommand cmdauditdata = new SqlCommand(auditdataupdate, connection);
                                                cmdauditdata.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                cmdauditdata.Parameters.AddWithValue("@Date_Completed", dateco);
                                                cmdauditdata.Parameters.AddWithValue("@Date_Modified", datemo);
                                                cmdauditdata.Parameters.AddWithValue("@Date_Started", datest);
                                                cmdauditdata.ExecuteNonQuery();
                                            }
                                            #endregion
                                            #region WA
                                            else if (sEMP == "WA")
                                            {
                                                tst = TimeZoneInfo.FindSystemTimeZoneById("W. Australia Standard Time");
                                                creatat = TimeZoneInfo.ConvertTime(createdat, tst);
                                                modat = TimeZoneInfo.ConvertTime(modifiedat, tst);
                                                dateco = TimeZoneInfo.ConvertTime(datec, tst);
                                                datemo = TimeZoneInfo.ConvertTime(datem, tst);
                                                datest = TimeZoneInfo.ConvertTime(dates, tst);
                                                //update audit table
                                                string auditupdate = "UPDATE api_Audit SET Created_at = @Created_at, Modified_at = @Modified_at Where Audit_id = @Audit_ID";

                                                SqlCommand cmd = new SqlCommand(auditupdate, connection);
                                                cmd.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                cmd.Parameters.AddWithValue("@Created_at", creatat);
                                                cmd.Parameters.AddWithValue("@Modified_at", modat);
                                                cmd.ExecuteNonQuery();

                                                //update auditdata
                                                string auditdataupdate = "UPDATE api_AuditData SET Date_Completed = @Date_Completed, Date_Modified = @Date_Modified, Date_Started = @Date_Started WHERE Audit_id = @Audit_ID";
                                                SqlCommand cmdauditdata = new SqlCommand(auditdataupdate, connection);
                                                cmdauditdata.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                cmdauditdata.Parameters.AddWithValue("@Date_Completed", dateco);
                                                cmdauditdata.Parameters.AddWithValue("@Date_Modified", datemo);
                                                cmdauditdata.Parameters.AddWithValue("@Date_Started", datest);
                                                cmdauditdata.ExecuteNonQuery();
                                            }
                                            #endregion
                                            #region NZ
                                            else if (sEMP == "AKL" || sEMP == "AUK" || sEMP == "BOP" || sEMP == "CAN" || sEMP == "GIS" || sEMP == "HKB" || sEMP == "MBH" || sEMP == "MWT" || sEMP == "NSN" || sEMP == "NTL" || sEMP == "OTA" || sEMP == "STL" || sEMP == "TKI" || sEMP == "WGN" || sEMP == "WKO" || sEMP == "WTC")
                                            {
                                                createdat = DateTimeOffset.Parse(created_at, System.Globalization.CultureInfo.GetCultureInfo("en-NZ"));
                                                modifiedat = DateTimeOffset.Parse(modified_at, System.Globalization.CultureInfo.GetCultureInfo("en-NZ"));
                                                datec = DateTimeOffset.Parse(datecompleted, System.Globalization.CultureInfo.GetCultureInfo("en-NZ"));
                                                datem = DateTimeOffset.Parse(datemodified, System.Globalization.CultureInfo.GetCultureInfo("en-NZ"));
                                                dates = DateTimeOffset.Parse(datestarted, System.Globalization.CultureInfo.GetCultureInfo("en-NZ"));

                                                tst = TimeZoneInfo.FindSystemTimeZoneById("New Zealand Standard Time");
                                                creatat = TimeZoneInfo.ConvertTime(createdat, tst);
                                                isDaylight = tst.IsDaylightSavingTime(createdat);
                                                if (isDaylight == true)
                                                {
                                                    //dst = TimeZoneInfo.FindSystemTimeZoneById("New Zealand Daylight Time");
                                                    creatat = TimeZoneInfo.ConvertTime(createdat, tst);
                                                    modat = TimeZoneInfo.ConvertTime(modifiedat, tst);
                                                    dateco = TimeZoneInfo.ConvertTime(datec, tst);
                                                    datemo = TimeZoneInfo.ConvertTime(datem, tst);
                                                    datest = TimeZoneInfo.ConvertTime(dates, tst);
                                                    //update audit table
                                                    string auditupdate = "UPDATE api_Audit SET Created_at = @Created_at, Modified_at = @Modified_at Where Audit_id = @Audit_ID";

                                                    SqlCommand cmd = new SqlCommand(auditupdate, connection);
                                                    cmd.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                    cmd.Parameters.AddWithValue("@Created_at", creatat);
                                                    cmd.Parameters.AddWithValue("@Modified_at", modat);
                                                    cmd.ExecuteNonQuery();

                                                    string auditdataupdate = "UPDATE api_AuditData SET Date_Completed = @Date_Completed, Date_Modified = @Date_Modified, Date_Started = @Date_Started WHERE Audit_id = @Audit_ID";

                                                    SqlCommand cmdauditdata = new SqlCommand(auditdataupdate, connection);
                                                    cmdauditdata.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                    cmdauditdata.Parameters.AddWithValue("@Date_Completed", dateco);
                                                    cmdauditdata.Parameters.AddWithValue("@Date_Modified", datemo);
                                                    cmdauditdata.Parameters.AddWithValue("@Date_Started", datest);
                                                    cmdauditdata.ExecuteNonQuery();
                                                }
                                                else
                                                {
                                                    modat = TimeZoneInfo.ConvertTime(modifiedat, tst);
                                                    dateco = TimeZoneInfo.ConvertTime(datec, tst);
                                                    datemo = TimeZoneInfo.ConvertTime(datem, tst);
                                                    datest = TimeZoneInfo.ConvertTime(dates, tst);
                                                    //update audit table
                                                    string auditupdate = "UPDATE api_Audit SET Created_at = @Created_at, Modified_at = @Modified_at Where Audit_id = @Audit_ID";

                                                    SqlCommand cmd = new SqlCommand(auditupdate, connection);
                                                    cmd.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                    cmd.Parameters.AddWithValue("@Created_at", creatat);
                                                    cmd.Parameters.AddWithValue("@Modified_at", modat);
                                                    cmd.ExecuteNonQuery();

                                                    //update auditdata
                                                    string auditdataupdate = "UPDATE api_AuditData SET Date_Completed = @Date_Completed, Date_Modified = @Date_Modified, Date_Started = @Date_Started WHERE Audit_id = @Audit_ID";
                                                    SqlCommand cmdauditdata = new SqlCommand(auditdataupdate, connection);
                                                    cmdauditdata.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                    cmdauditdata.Parameters.AddWithValue("@Date_Completed", dateco);
                                                    cmdauditdata.Parameters.AddWithValue("@Date_Modified", datemo);
                                                    cmdauditdata.Parameters.AddWithValue("@Date_Started", datest);
                                                    cmdauditdata.ExecuteNonQuery();
                                                }
                                            }
                                            #endregion
                                            #region TAS
                                            else if (sEMP == "TAS")
                                            {
                                                //get the country
                                                var ibasecountry = "SELECT Country FROM PRD.IBASES WHERE (FLNumber = @FLNumber and Region = @Region)";
                                                var ibasecountries = new SqlCommand(ibasecountry, connectIBASE);
                                                var systid = "800-" + systemid;
                                                var checksid = ibasecountries.Parameters.AddWithValue("@FLNumber", sysid);
                                                if (systid == null) checksid.Value = DBNull.Value;
                                                var checksreg = ibasecountries.Parameters.AddWithValue("@Region", "TAS");
                                                ibasecountries.ExecuteNonQuery();
                                                var sIBASECountry = (String)ibasecountries.ExecuteScalar();
                                                if (sIBASECountry != null)
                                                {
                                                    #region AU
                                                    if (sIBASECountry == "AU")
                                                    {
                                                        tst = TimeZoneInfo.FindSystemTimeZoneById("AUS Eastern Standard Time");
                                                        creatat = TimeZoneInfo.ConvertTime(createdat, tst);
                                                        isDaylight = tst.IsDaylightSavingTime(createdat);
                                                        if (isDaylight == true)
                                                        {
                                                            //dst = TimeZoneInfo.FindSystemTimeZoneById("AUS Eastern Daylight Time");
                                                            creatat = TimeZoneInfo.ConvertTime(createdat, tst);
                                                            modat = TimeZoneInfo.ConvertTime(modifiedat, tst);
                                                            dateco = TimeZoneInfo.ConvertTime(datec, tst);
                                                            datemo = TimeZoneInfo.ConvertTime(datem, tst);
                                                            datest = TimeZoneInfo.ConvertTime(dates, tst);
                                                            //update audit table
                                                            string auditupdate = "UPDATE api_Audit SET Created_at = @Created_at, Modified_at = @Modified_at Where Audit_id = @Audit_ID";

                                                            SqlCommand cmd = new SqlCommand(auditupdate, connection);
                                                            cmd.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                            cmd.Parameters.AddWithValue("@Created_at", creatat);
                                                            cmd.Parameters.AddWithValue("@Modified_at", modat);
                                                            cmd.ExecuteNonQuery();

                                                            string auditdataupdate = "UPDATE api_AuditData SET Date_Completed = @Date_Completed, Date_Modified = @Date_Modified, Date_Started = @Date_Started WHERE Audit_id = @Audit_ID";

                                                            SqlCommand cmdauditdata = new SqlCommand(auditdataupdate, connection);
                                                            cmdauditdata.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                            cmdauditdata.Parameters.AddWithValue("@Date_Completed", dateco);
                                                            cmdauditdata.Parameters.AddWithValue("@Date_Modified", datemo);
                                                            cmdauditdata.Parameters.AddWithValue("@Date_Started", datest);
                                                            cmdauditdata.ExecuteNonQuery();
                                                        }
                                                        else
                                                        {
                                                            modat = TimeZoneInfo.ConvertTime(modifiedat, tst);
                                                            dateco = TimeZoneInfo.ConvertTime(datec, tst);
                                                            datemo = TimeZoneInfo.ConvertTime(datem, tst);
                                                            datest = TimeZoneInfo.ConvertTime(dates, tst);
                                                            //update audit table
                                                            string auditupdate = "UPDATE api_Audit SET Created_at = @Created_at, Modified_at = @Modified_at Where Audit_id = @Audit_ID";

                                                            SqlCommand cmd = new SqlCommand(auditupdate, connection);
                                                            cmd.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                            cmd.Parameters.AddWithValue("@Created_at", creatat);
                                                            cmd.Parameters.AddWithValue("@Modified_at", modat);
                                                            cmd.ExecuteNonQuery();

                                                            //update auditdata
                                                            string auditdataupdate = "UPDATE api_AuditData SET Date_Completed = @Date_Completed, Date_Modified = @Date_Modified, Date_Started = @Date_Started WHERE Audit_id = @Audit_ID";
                                                            SqlCommand cmdauditdata = new SqlCommand(auditdataupdate, connection);
                                                            cmdauditdata.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                            cmdauditdata.Parameters.AddWithValue("@Date_Completed", dateco);
                                                            cmdauditdata.Parameters.AddWithValue("@Date_Modified", datemo);
                                                            cmdauditdata.Parameters.AddWithValue("@Date_Started", datest);
                                                            cmdauditdata.ExecuteNonQuery();
                                                        }
                                                    }
                                                    #endregion
                                                    #region NZ
                                                    else if (sIBASECountry == "NZ")
                                                    {
                                                        createdat = DateTimeOffset.Parse(created_at, System.Globalization.CultureInfo.GetCultureInfo("en-NZ"));
                                                        modifiedat = DateTimeOffset.Parse(modified_at, System.Globalization.CultureInfo.GetCultureInfo("en-NZ"));
                                                        datec = DateTimeOffset.Parse(datecompleted, System.Globalization.CultureInfo.GetCultureInfo("en-NZ"));
                                                        datem = DateTimeOffset.Parse(datemodified, System.Globalization.CultureInfo.GetCultureInfo("en-NZ"));
                                                        dates = DateTimeOffset.Parse(datestarted, System.Globalization.CultureInfo.GetCultureInfo("en-NZ"));

                                                        tst = TimeZoneInfo.FindSystemTimeZoneById("New Zealand Standard Time");
                                                        creatat = TimeZoneInfo.ConvertTime(createdat, tst);
                                                        isDaylight = tst.IsDaylightSavingTime(createdat);
                                                        if (isDaylight == true)
                                                        {
                                                            //dst = TimeZoneInfo.FindSystemTimeZoneById("New Zealand Daylight Time");
                                                            creatat = TimeZoneInfo.ConvertTime(createdat, tst);
                                                            modat = TimeZoneInfo.ConvertTime(modifiedat, tst);
                                                            dateco = TimeZoneInfo.ConvertTime(datec, tst);
                                                            datemo = TimeZoneInfo.ConvertTime(datem, tst);
                                                            datest = TimeZoneInfo.ConvertTime(dates, tst);
                                                            //update audit table
                                                            string auditupdate = "UPDATE api_Audit SET Created_at = @Created_at, Modified_at = @Modified_at Where Audit_id = @Audit_ID";

                                                            SqlCommand cmd = new SqlCommand(auditupdate, connection);
                                                            cmd.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                            cmd.Parameters.AddWithValue("@Created_at", creatat);
                                                            cmd.Parameters.AddWithValue("@Modified_at", modat);
                                                            cmd.ExecuteNonQuery();

                                                            string auditdataupdate = "UPDATE api_AuditData SET Date_Completed = @Date_Completed, Date_Modified = @Date_Modified, Date_Started = @Date_Started WHERE Audit_id = @Audit_ID";

                                                            SqlCommand cmdauditdata = new SqlCommand(auditdataupdate, connection);
                                                            cmdauditdata.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                            cmdauditdata.Parameters.AddWithValue("@Date_Completed", dateco);
                                                            cmdauditdata.Parameters.AddWithValue("@Date_Modified", datemo);
                                                            cmdauditdata.Parameters.AddWithValue("@Date_Started", datest);
                                                            cmdauditdata.ExecuteNonQuery();
                                                        }
                                                        else
                                                        {
                                                            modat = TimeZoneInfo.ConvertTime(modifiedat, tst);
                                                            dateco = TimeZoneInfo.ConvertTime(datec, tst);
                                                            datemo = TimeZoneInfo.ConvertTime(datem, tst);
                                                            datest = TimeZoneInfo.ConvertTime(dates, tst);
                                                            //update audit table
                                                            string auditupdate = "UPDATE api_Audit SET Created_at = @Created_at, Modified_at = @Modified_at Where Audit_id = @Audit_ID";

                                                            SqlCommand cmd = new SqlCommand(auditupdate, connection);
                                                            cmd.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                            cmd.Parameters.AddWithValue("@Created_at", creatat);
                                                            cmd.Parameters.AddWithValue("@Modified_at", modat);
                                                            cmd.ExecuteNonQuery();

                                                            //update auditdata
                                                            string auditdataupdate = "UPDATE api_AuditData SET Date_Completed = @Date_Completed, Date_Modified = @Date_Modified, Date_Started = @Date_Started WHERE Audit_id = @Audit_ID";
                                                            SqlCommand cmdauditdata = new SqlCommand(auditdataupdate, connection);
                                                            cmdauditdata.Parameters.AddWithValue("@Audit_ID", audit_no);
                                                            cmdauditdata.Parameters.AddWithValue("@Date_Completed", dateco);
                                                            cmdauditdata.Parameters.AddWithValue("@Date_Modified", datemo);
                                                            cmdauditdata.Parameters.AddWithValue("@Date_Started", datest);
                                                            cmdauditdata.ExecuteNonQuery();
                                                        }
                                                    }
                                                    #endregion
                                                }
                                            }
                                            #endregion
                                        }
                                    }
                                    #endregion

                                    connectIBASE.Close();
                                }
                                else if (header.label == "Job #")
                                {
                                    #region initialize values
                                    //initialize values
                                    var parentid = String.Empty;
                                    var itemid = String.Empty;
                                    var label = String.Empty;
                                    var section = String.Empty;
                                    var jobno = String.Empty;
                                    //check if null
                                    if (header == null || header.parent_id == null || header.parent_id == String.Empty)
                                        parentid = null;
                                    else
                                        parentid = header.parent_id;
                                    if (header == null || header.item_id == null || header.item_id == String.Empty)
                                        itemid = null;
                                    else
                                        itemid = header.item_id;
                                    if (header == null || header.label == null || header.label == String.Empty)
                                        label = null;
                                    else
                                        label = header.label;
                                    if (header == null || header.section == null || header.section == String.Empty)
                                        section = null;
                                    else
                                        section = header.section;
                                    if (header == null || header.responses == null || header.responses.text == null || header.responses.text == String.Empty)
                                        jobno = null;
                                    else
                                        jobno = header.responses.text;
                                    #endregion
                                    #region insert into Header Items
                                    //insert into Header Items
                                    string headeritems = "INSERT INTO api_Header_Items (Audit_ID, Parent_ID, Item_ID, Label, Section,Text)";
                                    headeritems += "VALUES (@Audit_ID, @Parent_ID, @Item_ID, @Label, @Section, @Text)";

                                    SqlCommand myHeaderItems = new SqlCommand(headeritems, connection);
                                    SqlParameter auditParam = myHeaderItems.Parameters.AddWithValue("@Audit_ID", audit_no);
                                    if (audit_no == null) auditParam.Value = DBNull.Value;
                                    SqlParameter parentParam = myHeaderItems.Parameters.AddWithValue("@Parent_ID", parentid);
                                    if (parentid == null) parentParam.Value = DBNull.Value;
                                    SqlParameter itemParam = myHeaderItems.Parameters.AddWithValue("@Item_ID", itemid);
                                    if (itemid == null) itemParam.Value = DBNull.Value;
                                    SqlParameter labelParam = myHeaderItems.Parameters.AddWithValue("@Label", label);
                                    if (label == null) labelParam.Value = DBNull.Value;
                                    SqlParameter sectionParam = myHeaderItems.Parameters.AddWithValue("@Section", section);
                                    if (section == null) sectionParam.Value = DBNull.Value;
                                    SqlParameter textParam = myHeaderItems.Parameters.AddWithValue("@Text", jobno);
                                    if (jobno == null) textParam.Value = DBNull.Value;
                                    //SqlParameter modifiedParam = myHeaderItems.Parameters.AddWithValue("@Modified_Date", modat);
                                    //if (modat == null) modifiedParam.Value = DBNull.Value;
                                    myHeaderItems.ExecuteNonQuery();
                                    #endregion
                                }
                                else if (header.label == "Prepared by")
                                {
                                    //initialize values
                                    var parentid = String.Empty;
                                    var itemid = String.Empty;
                                    var label = String.Empty;
                                    var section = String.Empty;
                                    var preparedby = String.Empty;
                                    //check if null
                                    if (header == null || header.parent_id == null || header.parent_id == String.Empty)
                                        parentid = null;
                                    else
                                        parentid = header.parent_id;
                                    if (header == null || header.item_id == null || header.item_id == String.Empty)
                                        itemid = null;
                                    else
                                        itemid = header.item_id;
                                    if (header == null || header.label == null || header.label == String.Empty)
                                        label = null;
                                    else
                                        label = header.label;
                                    if (header == null || header.section == null || header.section == String.Empty)
                                        section = null;
                                    else
                                        section = header.section;
                                    if (header == null || header.responses == null || header.responses.text == null || header.responses.text == String.Empty)
                                        preparedby = null;
                                    else
                                        preparedby = header.responses.text;

                                    //insert into Header Items
                                    string headeritems = "INSERT INTO api_Header_Items (Audit_ID, Parent_ID, Item_ID, Label, Section,Text)";
                                    headeritems += "VALUES (@Audit_ID, @Parent_ID, @Item_ID, @Label, @Section, @Text)";

                                    SqlCommand myHeaderItems = new SqlCommand(headeritems, connection);
                                    SqlParameter auditParam = myHeaderItems.Parameters.AddWithValue("@Audit_ID", audit_no);
                                    if (audit_no == null) auditParam.Value = DBNull.Value;
                                    SqlParameter parentParam = myHeaderItems.Parameters.AddWithValue("@Parent_ID", parentid);
                                    if (parentid == null) parentParam.Value = DBNull.Value;
                                    SqlParameter itemParam = myHeaderItems.Parameters.AddWithValue("@Item_ID", itemid);
                                    if (itemid == null) itemParam.Value = DBNull.Value;
                                    SqlParameter labelParam = myHeaderItems.Parameters.AddWithValue("@Label", label);
                                    if (label == null) labelParam.Value = DBNull.Value;
                                    SqlParameter sectionParam = myHeaderItems.Parameters.AddWithValue("@Section", section);
                                    if (section == null) sectionParam.Value = DBNull.Value;
                                    SqlParameter textParam = myHeaderItems.Parameters.AddWithValue("@Text", preparedby);
                                    if (preparedby == null) textParam.Value = DBNull.Value;
                                    //SqlParameter modifiedParam = myHeaderItems.Parameters.AddWithValue("@Modified_Date", modat);
                                    //if (modat == null) modifiedParam.Value = DBNull.Value;
                                    myHeaderItems.ExecuteNonQuery();
                                }

                                else if (header.label == "Conducted on")
                                {
                                    //initialize values
                                    var parentid = String.Empty;
                                    var itemid = String.Empty;
                                    var label = String.Empty;
                                    var section = String.Empty;
                                    var weight = String.Empty;
                                    var conductedon = String.Empty;
                                    var enable_date = String.Empty;
                                    var enable_time = String.Empty;
                                    DateTimeOffset con = new DateTimeOffset();

                                    //check the values if null
                                    if (header == null || header.parent_id == null || header.parent_id == String.Empty)
                                        parentid = null;
                                    else
                                        parentid = header.parent_id;
                                    if (header == null || header.item_id == null || header.item_id == String.Empty)
                                        itemid = null;
                                    else
                                        itemid = header.item_id;
                                    if (header == null || header.label == null || header.label == String.Empty)
                                        label = null;
                                    else
                                        label = header.label;
                                    if (header == null || header.section == null || header.section == String.Empty)
                                        section = null;
                                    else
                                        section = header.section;
                                    if (header == null || header.options == null || header.options.weighting == null || header.options.weighting == String.Empty)
                                        weight = null;
                                    else
                                        weight = header.options.weighting;
                                    if (header == null || header.options == null || header.options.enable_date == null || header.options.enable_date == String.Empty)
                                        enable_date = null;
                                    else
                                        enable_date = header.options.enable_date;
                                    if (header == null || header.options == null || header.options.enable_time == null || header.options.enable_time == String.Empty)
                                        enable_time = null;
                                    else
                                        enable_time = header.options.enable_time;
                                    if (header == null || header.responses == null || header.responses.datetime == null || header.responses.datetime == String.Empty)
                                        conductedon = null;
                                    else
                                        conductedon = header.responses.datetime;
                                    if (conductedon == null)
                                        con = new DateTimeOffset();
                                    else
                                    {
                                        ////convert fomat of dates
                                        DateTimeOffset cdate = DateTimeOffset.Parse(conductedon, System.Globalization.CultureInfo.GetCultureInfo("en-AU"));
                                        DateTimeOffset codate = TimeZoneInfo.ConvertTime(cdate, TimeZoneInfo.Local);
                                        con = codate;
                                    }

                                    //insert into Header Items
                                    string headeritems = "INSERT INTO api_Header_Items (Audit_ID, Parent_ID, Item_ID, Label, Section, Weighting, Enable_Date, Enable_Time, Datetime)";
                                    headeritems += "VALUES (@Audit_ID, @Parent_ID, @Item_ID, @Label, @Section, @Weighting, @Enable_Date, @Enable_Time,  @Datetime)";

                                    SqlCommand myHeaderItems = new SqlCommand(headeritems, connection);
                                    SqlParameter auditParam = myHeaderItems.Parameters.AddWithValue("@Audit_ID", audit_no);
                                    if (audit_no == null) auditParam.Value = DBNull.Value;
                                    SqlParameter parentParam = myHeaderItems.Parameters.AddWithValue("@Parent_ID", parentid);
                                    if (parentid == null) parentParam.Value = DBNull.Value;
                                    SqlParameter itemParam = myHeaderItems.Parameters.AddWithValue("@Item_ID", itemid);
                                    if (itemid == null) itemParam.Value = DBNull.Value;
                                    SqlParameter labelParam = myHeaderItems.Parameters.AddWithValue("@Label", label);
                                    if (label == null) labelParam.Value = DBNull.Value;
                                    SqlParameter sectionParam = myHeaderItems.Parameters.AddWithValue("@Section", section);
                                    if (section == null) sectionParam.Value = DBNull.Value;
                                    SqlParameter weightParam = myHeaderItems.Parameters.AddWithValue("@Weighting", weight);
                                    if (weight == null) weightParam.Value = DBNull.Value;
                                    SqlParameter dateParam = myHeaderItems.Parameters.AddWithValue("@Enable_Date", enable_date);
                                    if (enable_date == null) dateParam.Value = DBNull.Value;
                                    SqlParameter timeParam = myHeaderItems.Parameters.AddWithValue("@Enable_Time", enable_time);
                                    if (enable_time == null) timeParam.Value = DBNull.Value;
                                    SqlParameter textParam = myHeaderItems.Parameters.AddWithValue("@DateTime", con);
                                    if (con == null) textParam.Value = DBNull.Value;
                                    //SqlParameter modifiedParam = myHeaderItems.Parameters.AddWithValue("@Modified_Date", modat);
                                    //if (modat == null) modifiedParam.Value = DBNull.Value;
                                    myHeaderItems.ExecuteNonQuery();
                                }
                            }
                            #endregion //headeritems
                            #region //items
                            int line_nos = 0;
                            foreach (var items in auditresult.items)
                            {
                                #region initialize values
                                //initialize values
                                var itemid = String.Empty;
                                var label = String.Empty;
                                var type_items = String.Empty;
                                var combined_score = 0.00;
                                var combined_max_score = 0.00;
                                var combined_score_percentage = 0.00;
                                var parentid = items.parent_id;
                                var weighting = String.Empty;
                                List<string> values = new List<string>();
                                var condition = String.Empty;
                                var type_options = String.Empty;
                                var visible_in_audit = false;
                                var visible_in_report = false;
                                var response_set = String.Empty;
                                var select = String.Empty;
                                var evaluation = false;
                                var inactive = false;

                                //check if null
                                if (items == null || items.item_id == null)
                                    itemid = null;
                                else
                                    itemid = items.item_id;
                                if (items == null || items.label == null)
                                    label = null;
                                else
                                    label = items.label;
                                if (items == null || items.type == null)
                                    type_items = null;
                                else
                                    type_items = items.type;
                                if (items == null || items.scoring == null || items.scoring.combined_score == 0.00)
                                    combined_score = 0.00;
                                else
                                    combined_score = items.scoring.combined_score;
                                if (items == null || items.scoring == null || items.scoring.combined_max_score == 0.00)
                                    combined_max_score = 0.00;
                                else
                                    combined_max_score = items.scoring.combined_max_score;
                                if (items == null || items.scoring == null || items.scoring.combined_score_percentage == 0.00)
                                    combined_score_percentage = 0.00;
                                else
                                    combined_score_percentage = items.scoring.combined_score_percentage;
                                if (items == null || items.options == null || items.options.weighting == null)
                                    weighting = null;
                                else
                                    weighting = items.options.weighting;
                                if (items == null || items.options == null || items.options.values == null)
                                    values = null;
                                else
                                    values = items.options.values;
                                if (items == null || items.options == null || items.options.condition == null || items.options.condition == String.Empty)
                                    condition = null;
                                else
                                    condition = items.options.condition;
                                if (items == null || items.options == null || items.options.type == null || items.options.type == String.Empty)
                                    type_options = null;
                                else
                                    type_options = items.options.type;
                                if (items == null || items.options == null || items.options.visible_in_audit == false)
                                    visible_in_audit = false;
                                else
                                    visible_in_audit = items.options.visible_in_audit;
                                if (items == null || items.options == null || items.options.visible_in_report == false)
                                    visible_in_report = false;
                                else
                                    visible_in_report = items.options.visible_in_report;
                                if (items == null || items.options == null || items.options.response_set == null || items.options.response_set == String.Empty)
                                    response_set = null;
                                else
                                    response_set = items.options.response_set;
                                if (items == null || items.evaluation == false)
                                    evaluation = false;
                                else
                                    evaluation = items.evaluation;
                                if (items == null || items.inactive == false)
                                    inactive = false;
                                else
                                    inactive = items.inactive;
                                #endregion
                                #region insert data to Items table
                                //insert data to Items_Data
                                line_nos++;                            
                            
                                string itemsdata = "INSERT INTO Items (Audit_ID, Item_ID, Parent_ID, Label, Type, Evaluation, Inactive, Weighting, Response_Set, Options_Values, Condition, Options_Type, Visible_in_Audit, Visible_in_Report,Combined_Score, Combined_Max_Score, Combined_Score_Percentage, Line_No )";
                                itemsdata += "VALUES (@Audit_ID, @Item_ID, @Parent_ID, @Label, @Type, @Evaluation, @Inactive, @Weighting, @Response_Set, @Option_Values, @Condition, @Options_Type, @Visible_in_Audit, @Visible_in_Report, @Combined_Score, @Combined_Max_Score, @Combined_Score_Percentage, @Line_Num)";

                                SqlCommand myItemData = new SqlCommand(itemsdata, connection);
                                SqlParameter auditno1Param = myItemData.Parameters.AddWithValue("@Audit_ID", audit_id);
                                if (audit_id == null) auditno1Param.Value = DBNull.Value;
                                SqlParameter itemParam = myItemData.Parameters.AddWithValue("@Item_ID", itemid);
                                if (itemid == null) itemParam.Value = DBNull.Value;
                                SqlParameter parentParam = myItemData.Parameters.AddWithValue("@Parent_ID", parentid);
                                if (parentid == null) parentParam.Value = DBNull.Value;
                                SqlParameter LabelParam = myItemData.Parameters.AddWithValue("@Label", label);
                                if (label == null) LabelParam.Value = DBNull.Value;
                                SqlParameter typeParam = myItemData.Parameters.AddWithValue("@Type", type_items);
                                if (type_items == null) typeParam.Value = DBNull.Value;
                                SqlParameter evalParam = myItemData.Parameters.AddWithValue("@Evaluation", evaluation);
                                if (evaluation == false) evalParam.Value = DBNull.Value;
                                SqlParameter inactiveParam = myItemData.Parameters.AddWithValue("@Inactive", inactive);
                                if (inactive == false) inactiveParam.Value = DBNull.Value;
                                SqlParameter weightParam = myItemData.Parameters.AddWithValue("@Weighting", weighting);
                                if (weighting == null) weightParam.Value = DBNull.Value;
                                SqlParameter setParam = myItemData.Parameters.AddWithValue("@Response_Set", response_set);
                                if (response_set == null) setParam.Value = DBNull.Value;
                                SqlParameter valuesParam = myItemData.Parameters.Add("@Option_Values", SqlDbType.VarChar);
                                if (values == null || values.Count == 0)
                                    myItemData.Parameters["@Option_Values"].Value = values;
                                else
                                {
                                    foreach (var val in values)
                                    {
                                        myItemData.Parameters["@Option_Values"].Value = val;
                                    }
                                }
                                if (values == null || values.Count == 0) valuesParam.Value = DBNull.Value;
                                SqlParameter conditionParam = myItemData.Parameters.AddWithValue("@Condition", condition);
                                if (condition == null) conditionParam.Value = DBNull.Value;
                                SqlParameter otypeParam = myItemData.Parameters.AddWithValue("@Options_Type", type_options);
                                if (type_options == null) otypeParam.Value = DBNull.Value;
                                SqlParameter visibleaParam = myItemData.Parameters.AddWithValue("@Visible_in_Audit", visible_in_audit);
                                if (visible_in_audit == false) visibleaParam.Value = DBNull.Value;
                                SqlParameter visiblerParam = myItemData.Parameters.AddWithValue("@Visible_in_Report", visible_in_report);
                                if (visible_in_report == false) visiblerParam.Value = DBNull.Value;
                                SqlParameter cscoreParam = myItemData.Parameters.AddWithValue("@Combined_Score", combined_score);
                                if (combined_score == 0.00) cscoreParam.Value = DBNull.Value;
                                SqlParameter cmscoreParam = myItemData.Parameters.AddWithValue("@Combined_Max_Score", combined_max_score);
                                if (combined_max_score == 0.00) cmscoreParam.Value = DBNull.Value;
                                SqlParameter cscorepParam = myItemData.Parameters.AddWithValue("@Combined_Score_Percentage", combined_score_percentage);
                                if (combined_score_percentage == 0.00) cscorepParam.Value = DBNull.Value;
                                SqlParameter line_nom = myItemData.Parameters.AddWithValue("@Line_Num", line_nos);
                                if (line_nos == 0) line_nom.Value = DBNull.Value;
                                myItemData.ExecuteNonQuery();
                                #endregion
                                #region update Items table
                                if (items == null || items.responses == null)
                                    select = null;
                                else
                                {
                                    if (items.responses.selected == null || items.responses.selected.Count == 0)
                                        continue;
                                    else
                                    {
                                        if (items.responses.selected[0] is string)
                                        {
                                            var stringValue = (string)items.responses.selected[0];
                                            if (stringValue == null)
                                                continue;
                                            else
                                            {
                                                var response_id = String.Empty;
                                                response_id = stringValue;

                                                String strResult = String.Empty;
                                                string data = String.Empty;
                                                string itemdata = String.Empty;
                                                SqlCommand itemData = new SqlCommand();
                                                SqlCommand Data = new SqlCommand();
                                                SqlParameter auditrParam = new SqlParameter();
                                                SqlParameter parentrParam = new SqlParameter();

                                                //check data if already inserted to Items
                                                data = "SELECT Audit_ID, Parent_ID FROM Items WHERE (Audit_ID = @AuditID and Parent_ID = @ParentID)";
                                                Data = new SqlCommand(data, connection);
                                                auditrParam = Data.Parameters.AddWithValue("@AuditID", audit_id);
                                                if (audit_id == null) auditrParam.Value = DBNull.Value;
                                                parentrParam = Data.Parameters.AddWithValue("@ParentID", parentid);
                                                if (parentid == null) parentrParam.Value = DBNull.Value;
                                                Data.ExecuteNonQuery();
                                                strResult = (String)Data.ExecuteScalar();
                                                if (strResult == null)
                                                {
                                                    itemdata = "UPDATE Items SET ID = @ID WHERE (Audit_ID = @Audit_ID and Parent_ID = @ParentID)";
                                                    itemData = new SqlCommand(itemdata, connection);

                                                    auditrParam = itemData.Parameters.AddWithValue("@Audit_ID", audit_id);
                                                    if (audit_id == null) auditrParam.Value = DBNull.Value;
                                                    parentrParam = itemData.Parameters.AddWithValue("@Item_ID", itemid);
                                                    if (itemid == null) parentrParam.Value = DBNull.Value;
                                                    SqlParameter idParam = itemData.Parameters.AddWithValue("@ID", response_id);
                                                    if (response_id == null) idParam.Value = DBNull.Value;
                                                    itemData.ExecuteNonQuery();
                                                }
                                            }
                                        }
                                        else
                                        {
                                            var sele = (items.responses.selected[0].GetType().GetGenericTypeDefinition().IsAssignableFrom(typeof(Dictionary<string, object>)));

                                            var dictionaryValue = items.responses.selected[0] as Dictionary<string, object>;

                                            if (dictionaryValue == null)
                                                return;
                                            else
                                            {
                                                foreach (var sel in dictionaryValue)
                                                {
                                                    String strResult = String.Empty;
                                                    string data = String.Empty;
                                                    string itemdata = String.Empty;
                                                    SqlCommand itemData = new SqlCommand();
                                                    SqlCommand Data = new SqlCommand();
                                                    SqlParameter auditrParam = new SqlParameter();
                                                    SqlParameter itemrParam = new SqlParameter();

                                                    //initialize values
                                                    object response_id = String.Empty;
                                                    object colour = String.Empty;
                                                    object label_response = String.Empty;
                                                    object label_score = 0.00;
                                                    object short_label = String.Empty;
                                                    object response_type = String.Empty;
                                                    object enable_score = false;
                                                    object image = String.Empty;

                                                    //get the values
                                                    if (sel.Key == ("id"))
                                                    {
                                                        response_id = sel.Value;

                                                        //Check data if already exists in Items
                                                        data = "SELECT Audit_ID, Item_ID FROM Items WHERE (Audit_ID = @AuditID and Item_ID = @ItemID)";
                                                        Data = new SqlCommand(data, connection);
                                                        auditrParam = Data.Parameters.AddWithValue("@AuditID", audit_id);
                                                        if (audit_id == null) auditrParam.Value = DBNull.Value;

                                                        itemrParam = Data.Parameters.AddWithValue("@ItemID", itemid);
                                                        if (itemid == null) itemrParam.Value = DBNull.Value;

                                                        Data.ExecuteNonQuery();
                                                        strResult = (String)Data.ExecuteScalar();

                                                        if (strResult != null)
                                                        {
                                                            itemdata = "UPDATE Items SET ID = @ID WHERE (Audit_ID = @Audit_ID and Item_ID = @Item_ID)";
                                                            itemData = new SqlCommand(itemdata, connection);

                                                            auditrParam = itemData.Parameters.AddWithValue("@Audit_ID", audit_id);
                                                            if (audit_id == null) auditrParam.Value = DBNull.Value;

                                                            itemrParam = itemData.Parameters.AddWithValue("@Item_ID", itemid);
                                                            if (itemid == null) itemrParam.Value = DBNull.Value;

                                                            SqlParameter idParam = itemData.Parameters.AddWithValue("@ID", response_id);
                                                            if (response_id == null) idParam.Value = DBNull.Value;

                                                            itemData.ExecuteNonQuery();
                                                        }
                                                    }
                                                    else if (sel.Key == ("colour"))
                                                    {
                                                        colour = sel.Value;

                                                        //update data to Items_Responses
                                                        data = "SELECT Audit_ID, Item_ID FROM Items WHERE (Audit_ID = @AuditID and Item_ID = @ItemID)";
                                                        Data = new SqlCommand(data, connection);
                                                        auditrParam = Data.Parameters.AddWithValue("@AuditID", audit_id);
                                                        if (audit_id == null) auditrParam.Value = DBNull.Value;

                                                        itemrParam = Data.Parameters.AddWithValue("@ItemID", itemid);
                                                        if (itemid == null) itemrParam.Value = DBNull.Value;

                                                        Data.ExecuteNonQuery();
                                                        strResult = (String)Data.ExecuteScalar();

                                                        if (strResult != null)
                                                        {
                                                            itemdata = "UPDATE Items SET Colour = @Colour WHERE (Audit_ID = @Audit_ID and Item_ID = @Item_ID)";
                                                            itemData = new SqlCommand(itemdata, connection);
                                                            auditrParam = itemData.Parameters.AddWithValue("@Audit_ID", audit_id);
                                                            if (audit_id == null) auditrParam.Value = DBNull.Value;

                                                            itemrParam = itemData.Parameters.AddWithValue("@Item_ID", itemid);
                                                            if (itemid == null) itemrParam.Value = DBNull.Value;

                                                            SqlParameter colourParam = itemData.Parameters.AddWithValue("@Colour", colour);
                                                            if (colour == null) colourParam.Value = DBNull.Value;

                                                            itemData.ExecuteNonQuery();
                                                        }
                                                    }
                                                    else if (sel.Key == ("label"))
                                                    {
                                                        label_response = sel.Value;

                                                        //update data to Items_Responses
                                                        data = "SELECT Audit_ID, Item_ID FROM Items WHERE (Audit_ID = @AuditID and Item_ID = @ItemID)";
                                                        Data = new SqlCommand(data, connection);
                                                        auditrParam = Data.Parameters.AddWithValue("@AuditID", audit_id);
                                                        if (audit_id == null) auditrParam.Value = DBNull.Value;

                                                        itemrParam = Data.Parameters.AddWithValue("@ItemID", itemid);
                                                        if (itemid == null) itemrParam.Value = DBNull.Value;

                                                        Data.ExecuteNonQuery();
                                                        strResult = (String)Data.ExecuteScalar();

                                                        if (strResult != null)
                                                        {
                                                            itemdata = "UPDATE Items SET Response_Label = @Label WHERE (Audit_ID = @Audit_ID and Item_ID = @Item_ID)";
                                                            itemData = new SqlCommand(itemdata, connection);
                                                            auditrParam = itemData.Parameters.AddWithValue("@Audit_ID", audit_id);
                                                            if (audit_id == null) auditrParam.Value = DBNull.Value;

                                                            itemrParam = itemData.Parameters.AddWithValue("@Item_ID", itemid);
                                                            if (itemid == null) itemrParam.Value = DBNull.Value;

                                                            SqlParameter labelParam = itemData.Parameters.AddWithValue("@Label", label_response);
                                                            if (label_response == null) labelParam.Value = DBNull.Value;

                                                            itemData.ExecuteNonQuery();
                                                        }
                                                        #region checktheresponse = "NO"
                                                        if (label_response.ToString() == "No")
                                                        {
                                                            #region check if it is unsafe to continue
                                                            ////to check if the answer in Is it safe to continue is NO, if no send email to manager
                                                            var dataresponse = "SELECT Response_Label  FROM Items WHERE (Audit_ID = @AuditID and Item_ID = 'fd183420-0b18-11e4-9e0d-571854d42979' and Label = 'Is it safe to continue?\r\n')";
                                                            Data = new SqlCommand(dataresponse, connection);
                                                            auditrParam = Data.Parameters.AddWithValue("@AuditID", audit_id);
                                                            if (audit_id == null) auditrParam.Value = DBNull.Value;
                                                            Data.ExecuteNonQuery();
                                                            var strResult2 = (String)Data.ExecuteScalar();

                                                            if (strResult2 != null)
                                                            {
                                                                if (strResult2 == "No" || strResult2 == "NO" || strResult2 == "N")
                                                                {
                                                                    #region getAuditItems
                                                                    ////get all the audit items and responses
                                                                    var getItems = "SELECT Label, Response_Label FROM ITEMS WHERE Audit_ID = @AuditID";
                                                                    Data = new SqlCommand(getItems, connection);
                                                                    auditrParam = Data.Parameters.AddWithValue("@AuditID", audit_id);
                                                                    if (audit_id == null) auditrParam.Value = DBNull.Value;
                                                                    using (SqlDataReader readerItems = Data.ExecuteReader())
                                                                    {
                                                                        List<ItemsSaved> setItems = new List<ItemsSaved>();
                                                                        while (readerItems.Read())
                                                                        {
                                                                            ItemsSaved i = new ItemsSaved();
                                                                            i.Label = (string)readerItems["Label"];
                                                                            if (readerItems["Response_Label"] is System.DBNull)
                                                                                i.Response_Label = String.Empty;
                                                                            else
                                                                                i.Response_Label = (string)readerItems["Response_Label"];
                                                                            setItems.Add(i);
                                                                        }
                                                                        #region getAuthor
                                                                        ////get system id, date completed, author, text
                                                                        var getAuthor = "Select a.Label, a.Text, b.Author, c.Date_Completed from iAuditor.dbo.api_Header_Items a,iAuditor.dbo.api_Template_Data b, iAuditor.dbo.api_AuditData c where a.Audit_ID = b.Audit_ID and b.Audit_ID = c.Audit_ID and a.Label = 'System ID'and a.Audit_ID = @AuditID";
                                                                        Data = new SqlCommand(getAuthor, connection);
                                                                        auditrParam = Data.Parameters.AddWithValue("@AuditID", audit_id);
                                                                        if (audit_id == null) auditrParam.Value = DBNull.Value;
                                                                        #endregion getAuthor
                                                                        using (SqlDataReader readerData = Data.ExecuteReader())
                                                                        {

                                                                            while (readerData.Read())
                                                                            {
                                                                                #region readerData
                                                                                var setLabel = readerData["Label"];
                                                                                var setText = readerData["Text"];
                                                                                var setAuthor = readerData["Author"];
                                                                                var setDate = readerData["Date_Completed"];

                                                                                #region getTeamLeader
                                                                                var getEmployeeTL = "SELECT Email,TeamLeadID, CSMID FROM Employee WHERE Name = @setAuthor";
                                                                                Data = new SqlCommand(getEmployeeTL, connection);
                                                                                var authorrParam = Data.Parameters.AddWithValue("@setAuthor", setAuthor);
                                                                                if (setAuthor == null) authorrParam.Value = DBNull.Value;
                                                                                #endregion getTeamLeader
                                                                                using (SqlDataReader reader = Data.ExecuteReader())
                                                                                {
                                                                                    #region reader
                                                                                    while (reader.Read())
                                                                                    {
                                                                                        var setEmployeeEmail = reader["Email"];
                                                                                        var setEmployeeTL = reader["TeamLeadID"];
                                                                                        var setEmployeeCSM = reader["CSMID"];

                                                                                        var getTeamLead = "SELECT a.Name as TLName, a.Email as TLEmail, b.Name as CSMName, b.Email as CSMEmail FROM TeamLead a, CSM_Region b WHERE a.Employee_ID = @setEmployeeTL and a.CSMID = b.Employee_ID";
                                                                                        Data = new SqlCommand(getTeamLead, connection);
                                                                                        var employeeTL = Data.Parameters.AddWithValue("@setEmployeeTL", setEmployeeTL);
                                                                                        if (setEmployeeTL == null) employeeTL.Value = DBNull.Value;
                                                                                        using (SqlDataReader readerTL = Data.ExecuteReader())
                                                                                        {
                                                                                            #region readerTL
                                                                                            while (readerTL.Read())
                                                                                            {
                                                                                                var setTLName = readerTL[0];
                                                                                                var setTLEmail = readerTL[1];
                                                                                                var setCSMName = readerTL[2];
                                                                                                var setCSMEmail = readerTL[3];
                                                                                                //var testemail = "kathrine.pagsisihan@siemens.com";
                                                                                                try
                                                                                                {
                                                                                                    #region send email
                                                                                                    var emailFrom = "";
                                                                                                    const String SMTP_USERNAME = "AKIAJALM5KQA6PDKLANQ";
                                                                                                    const String SMTP_PASSWORD = "AmNy0k1zP5phX7WFKdoC8QGLpAGfeW9xOw1AbI/0xV4q";
                                                                                                    if (ConfigurationManager.AppSettings["EmailFrom"] != null)
                                                                                                        emailFrom = ConfigurationManager.AppSettings["EmailFrom"].ToString();

                                                                                                    //determine settings for email server
                                                                                                    var emailServer = ConfigurationManager.AppSettings["EmailServer"];
                                                                                                    if (emailServer == null)
                                                                                                        return;

                                                                                                    SmtpClient client = new SmtpClient();
                                                                                                    client.Port = 587;
                                                                                                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                                                                                                    client.Credentials = new System.Net.NetworkCredential(SMTP_USERNAME, SMTP_PASSWORD);
                                                                                                    client.Host = emailServer;
                                                                                                    client.EnableSsl = true;
                                                                                                    //var testemailcc = "mark.mcnamara@siemens.com";
                                                                                                    MailMessage mailItem = new MailMessage(emailFrom, setTLEmail.ToString());
                                                                                                    mailItem.To.Add(setEmployeeEmail.ToString());
                                                                                                    mailItem.Subject = "Attention: An unsafe Take 5 inspection has occurred.";
                                                                                                    mailItem.CC.Add(setCSMEmail.ToString());
                                                                                                    mailItem.CC.Add("hccloudservices.au@siemens.com");
                                                                                                    mailItem.IsBodyHtml = true;
                                                                                                    mailItem.Body = "System ID: " + setText + "<br/> Date Completed: " + (setDate) + "<br/>" + "Completed By: " + setAuthor + "<br/><br/>";
                                                                                                    foreach (var item in setItems)
                                                                                                    {
                                                                                                        mailItem.Body += "<tr>";
                                                                                                        mailItem.Body += "<td stlye='color:blue;'>" + item.Label + "</td>" + "<td stlye='color:blue;'>" + item.Response_Label + "</td>";
                                                                                                        mailItem.Body += "</tr><br>";
                                                                                                    }
                                                                                                    mailItem.Body += "</table>";
                                                                                                    client.Send(mailItem);
                                                                                                }
                                                                                                    #endregion send email

                                                                                                catch (Exception ex)
                                                                                                {
                                                                                                    MessageBox.Show(ex.Message);
                                                                                                }
                                                                                                finally
                                                                                                {

                                                                                                }

                                                                                            }

                                                                                            #endregion readerTL

                                                                                        }

                                                                                    }
                                                                                    //connection.Close();
                                                                                    #endregion reader
                                                                                }

                                                                                #endregion readerData
                                                                            }
                                                                            //connection.Close();
                                                                        }
                                                                    }
                                                                    #endregion getAuditItems
                                                                }
                                                            }
                                                            #endregion check if it unsafe to continue
                                                        }
                                                        #endregion
                                                    }
                                                    else if (sel.Key == "score")
                                                    {
                                                        label_score = sel.Value;

                                                        //update data to Items_Responses
                                                        data = "SELECT Audit_ID, Item_ID FROM Items WHERE (Audit_ID = @AuditID and Item_ID = @ItemID)";
                                                        Data = new SqlCommand(data, connection);
                                                        auditrParam = Data.Parameters.AddWithValue("@AuditID", audit_id);
                                                        if (audit_id == null) auditrParam.Value = DBNull.Value;

                                                        itemrParam = Data.Parameters.AddWithValue("@ItemID", itemid);
                                                        if (itemid == null) itemrParam.Value = DBNull.Value;

                                                        Data.ExecuteNonQuery();
                                                        strResult = (String)Data.ExecuteScalar();

                                                        if (strResult != null)
                                                        {
                                                            itemdata = "UPDATE Items SET Score = @Score WHERE (Audit_ID = @Audit_ID and Item_ID = @Item_ID)";
                                                            itemData = new SqlCommand(itemdata, connection);
                                                            auditrParam = itemData.Parameters.AddWithValue("@Audit_ID", audit_id);
                                                            if (audit_id == null) auditrParam.Value = DBNull.Value;

                                                            itemrParam = itemData.Parameters.AddWithValue("@Item_ID", itemid);
                                                            if (itemid == null) itemrParam.Value = DBNull.Value;

                                                            SqlParameter score1Param = itemData.Parameters.AddWithValue("@Score", label_score);
                                                            if (label_score == null) score1Param.Value = DBNull.Value;

                                                            itemData.ExecuteNonQuery();
                                                        }
                                                    }
                                                    else if (sel.Key == ("short_label"))
                                                    {
                                                        short_label = sel.Value;

                                                        //update data to Items_Responses
                                                        data = "SELECT Audit_ID, Item_ID FROM Items WHERE (Audit_ID = @AuditID and Item_ID = @ItemID)";
                                                        Data = new SqlCommand(data, connection);
                                                        auditrParam = Data.Parameters.AddWithValue("@AuditID", audit_id);
                                                        if (audit_id == null) auditrParam.Value = DBNull.Value;

                                                        itemrParam = Data.Parameters.AddWithValue("@ItemID", itemid);
                                                        if (itemid == null) itemrParam.Value = DBNull.Value;

                                                        Data.ExecuteNonQuery();
                                                        strResult = (String)Data.ExecuteScalar();

                                                        if (strResult != null)
                                                        {
                                                            itemdata = "UPDATE Items SET Short_Label = @Short_Label WHERE (Audit_ID = @Audit_ID and Item_ID = @Item_ID)";
                                                            itemData = new SqlCommand(itemdata, connection);
                                                            auditrParam = itemData.Parameters.AddWithValue("@Audit_ID", audit_id);
                                                            if (audit_id == null) auditrParam.Value = DBNull.Value;

                                                            itemrParam = itemData.Parameters.AddWithValue("@Item_ID", itemid);
                                                            if (itemid == null) itemrParam.Value = DBNull.Value;

                                                            SqlParameter shortParam = itemData.Parameters.AddWithValue("@Short_Label", short_label);
                                                            if (short_label == null) shortParam.Value = DBNull.Value;

                                                            itemData.ExecuteNonQuery();
                                                        }
                                                    }
                                                    else if (sel.Key == ("type"))
                                                    {
                                                        response_type = sel.Value;

                                                        //update data to Items_Responses
                                                        data = "SELECT Audit_ID, Item_ID FROM Items WHERE (Audit_ID = @AuditID and Item_ID = @ItemID)";
                                                        Data = new SqlCommand(data, connection);
                                                        auditrParam = Data.Parameters.AddWithValue("@AuditID", audit_id);
                                                        if (audit_id == null) auditrParam.Value = DBNull.Value;

                                                        itemrParam = Data.Parameters.AddWithValue("@ItemID", itemid);
                                                        if (itemid == null) itemrParam.Value = DBNull.Value;

                                                        Data.ExecuteNonQuery();
                                                        strResult = (String)Data.ExecuteScalar();

                                                        if (strResult != null)
                                                        {

                                                            itemdata = "UPDATE Items SET Responses_Type = @Response_Type WHERE (Audit_ID = @Audit_ID and Item_ID = @Item_ID)";
                                                            itemData = new SqlCommand(itemdata, connection);
                                                            auditrParam = itemData.Parameters.AddWithValue("@Audit_ID", audit_id);
                                                            if (audit_id == null) auditrParam.Value = DBNull.Value;

                                                            itemrParam = itemData.Parameters.AddWithValue("@Item_ID", itemid);
                                                            if (itemid == null) itemrParam.Value = DBNull.Value;

                                                            SqlParameter rtypeParam = itemData.Parameters.AddWithValue("@Response_Type", response_type);
                                                            if (response_type == null) rtypeParam.Value = DBNull.Value;

                                                            itemData.ExecuteNonQuery();
                                                        }
                                                    }

                                                    else if (sel.Key == ("enable_score"))
                                                    {
                                                        enable_score = sel.Value;

                                                        //update data to Items_Responses
                                                        data = "SELECT Audit_ID, Item_ID FROM Items WHERE (Audit_ID = @AuditID and Item_ID = @ItemID)";
                                                        Data = new SqlCommand(data, connection);
                                                        auditrParam = Data.Parameters.AddWithValue("@AuditID", audit_id);
                                                        if (audit_id == null) auditrParam.Value = DBNull.Value;

                                                        itemrParam = Data.Parameters.AddWithValue("@ItemID", itemid);
                                                        if (itemid == null) itemrParam.Value = DBNull.Value;

                                                        Data.ExecuteNonQuery();
                                                        strResult = (String)Data.ExecuteScalar();

                                                        if (strResult != null)
                                                        {
                                                            itemdata = "UPDATE Items SET Enable_Score = @Enable_Score WHERE (Audit_ID = @Audit_ID and Item_ID = @Item_ID)";
                                                            itemData = new SqlCommand(itemdata, connection);
                                                            auditrParam = itemData.Parameters.AddWithValue("@Audit_ID", audit_id);
                                                            if (audit_id == null) auditrParam.Value = DBNull.Value;

                                                            itemrParam = itemData.Parameters.AddWithValue("@Item_ID", itemid);
                                                            if (itemid == null) itemrParam.Value = DBNull.Value;

                                                            SqlParameter escoreParam = itemData.Parameters.AddWithValue("@Enable_Score", enable_score);
                                                            if (enable_score == null) escoreParam.Value = DBNull.Value;

                                                            itemData.ExecuteNonQuery();
                                                        }
                                                    }
                                                    else if (sel.Key == ("image"))
                                                    {
                                                        image = sel.Value;

                                                        //update data to Items_Responses
                                                        data = "SELECT Audit_ID, Item_ID FROM Items WHERE (Audit_ID = @AuditID and Item_ID = @ItemID)";
                                                        Data = new SqlCommand(data, connection);
                                                        auditrParam = Data.Parameters.AddWithValue("@AuditID", audit_id);
                                                        if (audit_id == null) auditrParam.Value = DBNull.Value;

                                                        itemrParam = Data.Parameters.AddWithValue("@ItemID", itemid);
                                                        if (itemid == null) itemrParam.Value = DBNull.Value;

                                                        Data.ExecuteNonQuery();
                                                        strResult = (String)Data.ExecuteScalar();

                                                        if (strResult != null)
                                                        {
                                                            itemdata = "UPDATE Items SET Image = @Image WHERE (Audit_ID = @Audit_ID and Item_ID = @Item_ID)";
                                                            itemData = new SqlCommand(itemdata, connection);
                                                            auditrParam = itemData.Parameters.AddWithValue("@Audit_ID", audit_id);
                                                            if (audit_id == null) auditrParam.Value = DBNull.Value;

                                                            itemrParam = itemData.Parameters.AddWithValue("@Item_ID", itemid);
                                                            if (itemid == null) itemrParam.Value = DBNull.Value;

                                                            SqlParameter imagerParam = itemData.Parameters.AddWithValue("@Image", image);
                                                            if (image == null) imagerParam.Value = DBNull.Value;

                                                            itemData.ExecuteNonQuery();
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                #endregion
                            }
                            #endregion //items

                            dataGridView1.DataSource = new List<JsonResult> { auditresult };
                            connection.Close();
                        }
                    }

                    MessageBox.Show("Successfully Inserted", "iAuditor", MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk);

                }
                #endregion //all
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Error");
            }
        }
        private void Clear_Click(object sender, EventArgs e)
        {
            audit_text.Text = string.Empty;
            listBox_template.Text = string.Empty;
            dateTimePicker_from.Text = string.Empty;
            dateTimePicker_to.Text = string.Empty;
        }
    }
}
