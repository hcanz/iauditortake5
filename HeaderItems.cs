﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_iAuditor
{
    public class HeaderItems
    {
        
        public string parent_id { get; set; }
        public string item_id { get; set; }
        public string label { get; set; }
        public string section { get; set; }
        public string[] children { get; set; }
        public Option options { get; set; }
        public Response responses { get; set; }
       
    }
}
