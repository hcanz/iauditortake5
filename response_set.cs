﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace API_iAuditor
{
    public class response_set
    {
        public string id { get; set; }
        public List<ID> content { get; set; }
        public string type { get; set; }
    }
}
