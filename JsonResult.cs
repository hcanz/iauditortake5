﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace API_iAuditor
{
    public class JsonResult
    {
        public JsonResult ()
        {

        }

        public string template_id { get; set; }
        public string audit_id { get; set; }
        public string created_at { get; set; }
        public string modified_at { get; set; }
        public Audit_Data audit_data { get; set; }
        public TemplateData template_data { get; set; }
        public List<HeaderItems> header_items { get; set; }
        public List<Item> items { get; set; }
       // public Assets assets { get; set; }
    }
}
