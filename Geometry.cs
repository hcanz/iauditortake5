﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_iAuditor
{
    public class Geometry
    {
        public List<Coordinates> location { get; set; }
        public string location_type { get; set; }
        public List<ViewPort> viewport { get; set; }
    }
}
