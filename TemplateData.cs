﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_iAuditor
{
    public class TemplateData
    {
        public Authorship authorship { get; set; }
        public Metadata metadata { get; set; }
        public Response_Sets response_sets { get; set; }
        //public List<Response_Sets> response_sets { get; set; }
    }
}
