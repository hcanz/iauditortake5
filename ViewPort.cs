﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_iAuditor
{
    public class ViewPort
    {
        public Coordinates northeast { get; set; }
        public Coordinates southwest { get; set; }
    }
}
