﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_iAuditor
{
    public class Options
    {
        public string weighting { get; set; }
        public string response_set { get; set; }
        //public string[] values { get; set; }
        public List<string> values { get; set; }
        public string condition { get; set; }
        public string type { get; set; }
        public bool visible_in_audit { get; set; }
        public bool visible_in_report { get; set; }

        //public List<Assets> assets { get; set; }
        //public string computed_field { get; set; }        
        //public string element { get; set; }
        //public bool enable_date { get; set; }
        //public bool enable_signature_timestamp { get; set; }
        //public bool enable_time { get; set; }
        //public bool hide_barcode { get; set; }
        //public string increment { get; set; }
        //public bool is_mandatory { get; set; }
        //public string label { get; set; }
        //public string link { get; set; }
        //public bool locked { get; set; }
        //public string max { get; set; }
        //public string media { get; set; }
        //public bool media_visible_in_report { get; set; }
        //public string min { get; set; }
        //public bool multiple_selection { get; set; }       
        //public bool secure { get; set; }
        //public string url { get; set; }
        
       
        
    }
}
